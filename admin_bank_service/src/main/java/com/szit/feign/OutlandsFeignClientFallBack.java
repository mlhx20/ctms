package com.szit.feign;

import com.szit.model.bean.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

/**
 * @author ：小木
 * @version :1.0
 * @description:
 * @date ：create in 2020/4/28 0028 18:29
 */
@Component
public class OutlandsFeignClientFallBack implements OutlandsFeignClient {

    @Override
    public boolean reg(User user, String pwd2) {
        return false;
    }
}
