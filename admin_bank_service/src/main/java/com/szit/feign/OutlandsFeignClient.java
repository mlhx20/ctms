package com.szit.feign;

import com.szit.model.bean.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@FeignClient(name = "outlands-bank-service",fallback = OutlandsFeignClientFallBack.class)
public interface OutlandsFeignClient {
    @RequestMapping(value = "/user/reg",method = RequestMethod.POST)
    @ResponseBody
    public boolean reg(@RequestBody User user, @RequestParam("pwd2")String pwd2);

}
