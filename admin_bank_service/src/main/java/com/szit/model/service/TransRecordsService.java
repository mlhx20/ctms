package com.szit.model.service;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;

import java.util.List;

public interface TransRecordsService {
    /**
     * 分页获取交易记录信息
     * @param tr 条件查询
     * @param page 分页实体对象
     * @return 返回实体对象泛型集合
     */
    List<TransRecords> listByPage(TransRecords tr, PageIndexer page);
    int count();
    /**
     * 根据用户id查询交易信息
     * @param id 交易id
     * @return 返回实体对象
     */
    TransRecords searchById(Integer id);
}
