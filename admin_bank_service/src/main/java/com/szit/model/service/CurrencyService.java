package com.szit.model.service;

import com.szit.model.bean.Currency;

import java.util.List;

/**
 * 货币类型服务接口
 *
 * @author 李浩
 * @since 2020-04-18 15:01:30
 */
public interface CurrencyService {


    /**
     * 返回全部currency信息
     *
     * @return
     */
    List<Currency> selectCurrencyType();
}