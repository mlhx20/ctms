package com.szit.model.service;

import com.szit.model.bean.Card;
import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.User;

import java.util.List;

/**
 * 银行卡业务逻辑层
 * @author 李浩
 * @version 1.0 2020-04-05
 */
public interface CardService  {


    /**
     * 修改银行卡信息
     *
     * @param card 银行卡实体对象
     * @return
     */
    Integer update(Card card);


    /**
     * 添加银行卡信息
     * @param card 银行卡实体对象
     * @return
     */
    Integer insert(Card card);

    /**
     * 查询所有用户信息
     * @return
     */
    List<Card> findAll();

    /**
     * 根据银行卡号查找id
     * @param no
     * @return
     */
    int findById(String no);

    /**
     * 查找银行卡是否重复
     * @param no
     * @return
     */
    Card searchByBankNo(String no);

    /**
     * 分页获取银行卡信息
     * @param card 条件查询
     * @param page 分页实体对象
     * @return 返回实体对象泛型集合
     */
    List<Card> listByPage(Card card, PageIndexer page);
}
