package com.szit.model.service;

import com.szit.model.bean.UserCard;

/**
 * 用户银行卡服务接口
 *
 * @author 李浩
 * @since 2020-04-06 15:43:57
 */
public interface UserCardService {

    /**
     * 新增数据
     * @param userCard 实例对象
     * @return 实例对象
     */
    UserCard insert(UserCard userCard);


}