package com.szit.model.service;

import com.szit.model.bean.*;

import java.util.List;

/**
 * 异常业务逻辑层接口
 *
 * @author Derek
 * @version 1.0 2020.04.03
 */
public interface AbnormalService {

    /**
     * 分页模糊查询异常信息
     *
     * @param abnormal     异常
     * @param page         页面
     * @return 异常集合
     */
    List<Abnormal> listAbnormalsByPage(Abnormal abnormal, PageIndexer page);

    /**
     * 修改异常状态
     *
     * @param id
     * @param state
     * @return
     */
    boolean update(int id, int state);


}
