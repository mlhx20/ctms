package com.szit.model.service;

import com.szit.model.bean.Administrator;

/**
 * 管理员业务逻辑层
 *
 * @author 李浩
 * @version 1.0 2020-04-05
 */
public interface AdministratorService  {
    /**
     * 管理员登陆
     * @param name
     * @param pwd
     * @return
     */
    Administrator login (String name , String pwd);

}
