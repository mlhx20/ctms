package com.szit.model.service;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.User;

import java.util.List;

/**
 * 用户业务逻辑层
 *
 * @author 李浩
 * @version 1.0 2020-04-05
 */
public interface UserService {

    /**
     * 查询所有用户信息
     * @return
     */
    List<User> findAll();

    /**
     * 验证用户名存不存在
     * @param name
     * @return
     */
    User isExistUser(String name);

    /**
     * 根据身份证号查找用户id
     * @param idCard
     * @return
     */
    User searchUserById(String idCard);

    /**
     * 分页获取用户列表
     * @param user 条件查询
     * @param page 分页实体对象
     * @return 返回实体对象泛型集合
     */
    List<User> listByPage(User user, PageIndexer page);

    /**
     * 新增数据
     *
     * @param User 实例对象
     * @return 实例对象
     */
    boolean insert(User User);
}
