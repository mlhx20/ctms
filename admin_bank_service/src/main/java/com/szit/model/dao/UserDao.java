package com.szit.model.dao;


import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户数据访问层
 *
 * @author 李浩
 * @since 2020-04-05
 */
@Repository
@Mapper
public interface UserDao {

    /**
     * 查询用户返回所有用户信息
     *
     * @return
     */
    List<User> queryAll();

    /**
     * 通过名查询单条数据
     *
     * @param name 用户名
     * @return 实例对象
     */
    User searchByName(String name);

    /**
     * 通过名查询单条数据
     *
     * @param idCard 身份证号码
     * @return 实例对象
     */
    User searchById(String idCard);


    /**
     * 分页查询用户信息
     *
     * @return 返回用户实体对象
     */
    List<User> getUsersByPage(
            @Param("id") Integer id,
            @Param("name") String name,
            @Param("state") Integer state,
            @Param("page") Integer page,
            @Param("limit") Integer limit);

    /**
     * 查询分页记录总数,以便求总页数
     *
     * @return 返回记录总数
     */
    int count(@Param("id") Integer id);

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    boolean addUser(User user);

}
