package com.szit.model.dao;


import com.szit.model.bean.Administrator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 管理员数据库访问层
 * @author 李浩
 * @since 2020-04-03
 */
@Transactional
@Mapper
public interface AdministratorDao   {

    /**
     * 登陆获取用户名密码
     * @param name
     * @param pwd
     * @return
     */
    Administrator login (@Param("name")String name , @Param("pwd") String pwd);
}