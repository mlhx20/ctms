package com.szit.model.dao;

import com.szit.model.bean.Currency;

import java.util.List;

/**
 * 货币类型数据库访问层
 * @author 李浩
 * @since 2020-04-18 15:01:29
 */
public interface CurrencyDao {

    /**
     * 查询所有货币信息
     * @return
     */
    List<Currency> queryAll();
}