package com.szit.model.dao;


import com.szit.model.bean.Card;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 银行卡表数据库访问层
 *
 * @author 李浩
 * @since 2020-04-03 16:19:44
 */
@Mapper
public interface CardDao {

    /**
     * 新增数据
     *
     * @param card 实例对象
     * @return 影响行数
     */
    int insert(Card card);

    /**
     * 修改数据
     *
     * @param card 实例对象
     * @return 影响行数
     */
    int update(Card card);

    /**
     * 查询用户返回所有用户信息
     *
     * @return
     */
    List<Card> queryAll();

    /**
     * 根据银行卡号查找id
     *
     * @param no
     * @return
     */
    int findById(String no);

    /**
     * 查找银行卡是否重复
     *
     * @param no
     * @return
     */
    Card searchByBankNo(String no);

    /**
     * 查询分页记录总数,以便求总页数
     *
     * @return 返回记录总数
     */
    int getCountsByPage();

    /**
     * 分页查询交易记录信息
     *
     * @param no 银行卡号
     * @return 返回银行卡实体对象
     */
    List<Card> getCardByPage(
            @Param("no") String no,
            @Param("state") Integer state,
            @Param("bankid") Integer bankid,
            @Param("page") Integer page,
            @Param("limit") Integer limit);
}
