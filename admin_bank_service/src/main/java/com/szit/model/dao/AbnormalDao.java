package com.szit.model.dao;

import com.szit.model.bean.Abnormal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 异常数据访问层接口
 *
 * @author Derek
 * @version 1.0 2020.04.03
 */
@Mapper
@Repository
public interface AbnormalDao {

    /**
     * 分页模糊查询异常
     *
     * @param code      异常编号
     * @param type      异常类型
     * @param state     异常状态
     * @param from      起始行
     * @param pageSize  页容量
     * @return 异常集合
     */
    List<Abnormal> getAbnormalsByPage(@Param("abnCode") Integer code,
                                      @Param("abnType") Integer type,
                                      @Param("abnState") Integer state,
                                      @Param("from") Integer from,
                                      @Param("pageSize") Integer pageSize);

    /**
     * 查询总记录数,以便求总页数
     *
     * @param code
     * @param type
     * @param state
     * @return
     */
    int getCountsByPage(@Param("abnCode") Integer code,
                        @Param("abnType") Integer type,
                        @Param("abnState") Integer state);

    /**
     * 修改异常状态
     *
     * @param id
     * @param state
     * @return
     */
    boolean updateAbnormal(@Param("abnId") Integer id,
                           @Param("abnState") Integer state);


    /**
     * 添加异常
     *
     * @param abnormal 异常对象
     * @return 添加状态
     */
    boolean insertAbnormal(Abnormal abnormal);
}
