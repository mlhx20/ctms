package com.szit.model.dao;

import com.szit.model.bean.UserCard;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户银行卡数据库访问层
 * @author 李浩
 * @since 2020-04-06 15:43:57
 */
@Mapper
public interface UserCardDao {

    /**
     * 新增数据
     *
     * @param userCard 实例对象
     * @return 影响行数
     */
    int insert(UserCard userCard);
}