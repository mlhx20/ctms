package com.szit.model.dao;

import com.szit.model.bean.TransRecords;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface TransRecordsDao {
    /**
     * 分页查询交易记录信息
     * @param code 交易编号
     * @return 返回交易记录实体对象
     */
    List<TransRecords> getTransRecordsByPage(
            @Param("code") String code,
            @Param("userId") Integer userId,
            @Param("payeeId") Integer payeeId,
            @Param("state") Integer state,
            @Param("page") Integer page,
            @Param("limit") Integer limit);
    /**
     * 查询分页记录总数,以便求总页数
     *
     * @return 返回记录总数
     */
    int getCountsByPage();
    /**
     * 根据id查看交易记录信息
     * @param id 交易记录id
     * @return 返回交易记录实体对象
     */
    TransRecords getTransRecordsById(Integer id);
}
