package com.szit.model.bean;

import java.io.Serializable;

/**
 * 异常实体类
 */
public class Abnormal implements Serializable {
    private Integer id;
    private Integer userId; //用户id
    private Integer code;
    private Integer transId; //交易记录id
    private Integer type;
    private Integer state;
    private User user; //用户
    private TransRecords transRecords; //交易记录
    private Payee payee;    //收款人
    private Card card;  //银行卡
    private TypeTrade typeTrade;    //转账类型
    private Currency currency;  //货币
    private Bank bank;

    public Abnormal() {
        this.user = new User();
        this.transRecords = new TransRecords();
        this.payee = new Payee();
        this.card = new Card();
        this.typeTrade = new TypeTrade();
        this.currency = new Currency();
        this.bank = new Bank();
    }

    public Abnormal(Integer id, Integer userId, Integer code, Integer transId, Integer type, Integer state, User user, TransRecords transRecords, Payee payee, Card card, TypeTrade typeTrade, Currency currency, Bank bank) {
        this.id = id;
        this.userId = userId;
        this.code = code;
        this.transId = transId;
        this.type = type;
        this.state = state;
        this.user = user;
        this.transRecords = transRecords;
        this.payee = payee;
        this.card = card;
        this.typeTrade = typeTrade;
        this.currency = currency;
        this.bank = bank;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TransRecords getTransRecords() {
        return transRecords;
    }

    public void setTransRecords(TransRecords transRecords) {
        this.transRecords = transRecords;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public TypeTrade getTypeTrade() {
        return typeTrade;
    }

    public void setTypeTrade(TypeTrade typeTrade) {
        this.typeTrade = typeTrade;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}
