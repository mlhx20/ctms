package com.szit.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 货币实体类
 */
public class Currency implements Serializable {
    private Integer id;
    private String name;
    private String logo; //简写
    private String sign; //货币符号
    private Integer sort;
    private BigDecimal rate; //汇率 表示为100外币兑换多少人民币
    private Date update; //汇率更新时间

    public Currency() {
    }

    public Currency(Integer id, String name, String logo, String sign, Integer sort, BigDecimal rate, Date update) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.sign = sign;
        this.sort = sort;
        this.rate = rate;
        this.update = update;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getUpdate() {
        return update;
    }

    public void setUpdate(Date update) {
        this.update = update;
    }
}
