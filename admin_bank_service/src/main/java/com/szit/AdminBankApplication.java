package com.szit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EntityScan("com.szit.model.bean")
@Configuration
@MapperScan(basePackages = {"com.szit.model.dao"})
public class AdminBankApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminBankApplication.class,args);
        System.out.println("admin 服务启动");
    }
}
