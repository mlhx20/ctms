package com.szit.service.impl;

import com.szit.model.bean.Card;
import com.szit.model.bean.Currency;
import com.szit.model.dao.CurrencyDao;
import com.szit.model.service.CurrencyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 货币类型服务实现类
 * @author 李浩
 * @since 2020-04-18 15:01:30
 */
@Service("CurrencyService")
public class CurrencyServiceImpl implements CurrencyService {
    @Resource
    private CurrencyDao currencyDao;

    /**
     * 返回所有Currency信息（货币类型）
     * @return
     */
    @Override
    public List<Currency> selectCurrencyType() {
        return currencyDao.queryAll();
    }
}