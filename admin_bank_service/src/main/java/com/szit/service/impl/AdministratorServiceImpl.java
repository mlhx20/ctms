package com.szit.service.impl;


import com.szit.model.bean.Administrator;
import com.szit.model.dao.AdministratorDao;
import com.szit.model.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 管理员业务逻辑实现层
 * @author 李浩
 * @version 1.0 2020-04-05
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private AdministratorDao administratorDao;

    /**
     * 管理员登陆
     * @param name
     * @param pwd
     * @return
     */
    @Override
    public Administrator login(String name, String pwd) {
        return administratorDao.login(name, pwd);
    }
}
