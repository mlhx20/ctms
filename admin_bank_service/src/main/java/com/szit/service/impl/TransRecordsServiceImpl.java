package com.szit.service.impl;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;
import com.szit.model.dao.TransRecordsDao;
import com.szit.model.service.TransRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service("TransRecordsService")
@Transactional
public class TransRecordsServiceImpl implements TransRecordsService {
    @Autowired
    private TransRecordsDao trDao;
    public List<TransRecords> listByPage(TransRecords tr, PageIndexer page) {
        List<TransRecords> lists=trDao.getTransRecordsByPage(tr.getCode(),tr.getUserId(),
                tr.getPayeeId(),tr.getState(),
                (page.getPageIndex() - 1) * page.getPageSize(),
                page.getPageSize());
        // 计算总页数
        int count = trDao.getCountsByPage();

        page.setPageCount((int)Math.ceil(count*1.0/page.getPageSize()));
        page.setCount(count);

        return lists;
    }


    public int count() {
        return trDao.getCountsByPage();
    }


    public TransRecords searchById(Integer id) {
        return trDao.getTransRecordsById(id);
    }
}
