package com.szit.service.impl;


import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.User;
import com.szit.model.dao.UserDao;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ${author}
 * @since 2020-01-31
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    /**
     * 返回所有的银行卡信息
     * @return
     */
    @Override
    public List<User> findAll() {
        return null;
    }

    /**
     * 查找用户名是否存在
     * @param name
     * @return
     */
    @Override
    public User isExistUser(String name) {
        return userDao.searchByName(name);
    }

    /**
     * 根据身份证号码查找用户id
     * @param idCard
     * @return
     */
    @Override
    public User searchUserById(String idCard) {
        return userDao.searchById(idCard);
    }

    public List<User> listByPage(User user, PageIndexer page) {
        List<User> lists = userDao.getUsersByPage(user.getId(),user.getName(),user.getState(),
                (page.getPageIndex() - 1) * page.getPageSize(),
                page.getPageSize());
        // 计算总页数

        int count = userDao.count(user.getId());

        page.setPageCount((int) Math.ceil(count * 1.0 / page.getPageSize()));
        page.setCount(count);
        return lists;
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(User user) {
        return userDao.addUser(user);
    }
}
