package com.szit.service.impl;

import com.szit.model.bean.*;
import com.szit.model.dao.AbnormalDao;
import com.szit.model.service.AbnormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 异常业务逻辑层接口实现类
 *
 * @author Derek
 * @version 1.0 2020.04.03
 */
@Service("abnormalService")
@Transactional
public class AbnormalServiceImpl implements AbnormalService {

    @Autowired
    private AbnormalDao abnormalDao;

    public AbnormalDao getAbnormalDao() {

        return abnormalDao;
    }

    public void setAbnormalDao(AbnormalDao abnormalDao) {

        this.abnormalDao = abnormalDao;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public List<Abnormal> listAbnormalsByPage(Abnormal abnormal, PageIndexer page) {
        List<Abnormal> lists = abnormalDao.getAbnormalsByPage(abnormal.getCode(), abnormal.getType(), abnormal.getState(), (page.getPageIndex() - 1) * page.getPageSize(), page.getPageSize());

        // 计算总页数
        int count = abnormalDao.getCountsByPage(abnormal.getCode(),
                abnormal.getType(), abnormal.getState());

        page.setPageCount((int) Math.ceil(count * 1.0 / /*page.getPageSize()*/10));
        page.setCount(count);

        return lists;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(int id, int state) {
        boolean flag = true;

        try {
            abnormalDao.updateAbnormal(id, state);
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
        }
        return flag;
    }

}
