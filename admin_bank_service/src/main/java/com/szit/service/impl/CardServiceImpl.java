package com.szit.service.impl;


import com.szit.model.bean.Card;
import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.User;
import com.szit.model.dao.CardDao;
import com.szit.model.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 银行卡业务逻辑实现层
 *
 * @author 李浩
 * @version 1.0 2020-04-05
 */
@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private CardDao cardDao;

    /**
     * 返回银行卡所有信息（test）
     * @return
     */
    @Override
    public List<Card> findAll() {
        return cardDao.queryAll();
    }

    /**
     * 根据银行卡号码查找银行卡id
     * @param no
     * @return
     */
    @Override
    public int findById(String no) {
        return cardDao.findById(no);
    }

    /**
     * 查找银行卡号码是否重复
     * @param no
     * @return
     */
    @Override
    public Card searchByBankNo(String no) {
        return cardDao.searchByBankNo(no);
    }

    /**
     * 添加银行卡
     * @param card 银行卡实体对象
     * @return
     */
    @Override
    public Integer insert(Card card) {
        return cardDao.insert(card);
    }

    /**
     * 修改银行卡
     * @param card 银行卡实体对象
     * @return
     */
    @Override
    public Integer update(Card card) {
        return cardDao.update(card);
    }

    /**
     * 显示银行卡
     * @param card 银行卡实体对象
     * @return
     */
    public List<Card> listByPage(Card card, PageIndexer page) {
        List<Card> lists=cardDao.getCardByPage(card.getNo(),card.getState(),card.getBankid(),
                (page.getPageIndex() - 1) * page.getPageSize(),
                page.getPageSize());
        // 计算总页数
        int count = cardDao.getCountsByPage();

        page.setPageCount((int)Math.ceil(count*1.0/page.getPageSize()));
        page.setCount(count);
        return lists;
    }
}
