package com.szit.service.impl;


import com.szit.model.bean.UserCard;
import com.szit.model.dao.UserCardDao;
import com.szit.model.service.UserCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户银行卡服务实现类
 *
 * @author 李浩
 * @since 2020-04-06 15:43:57
 */
@Service
public class UserCardServiceImpl implements UserCardService {
    @Autowired
    private UserCardDao ctmsUserCardDao;

    /**
     * 新增数据
     * @param userCard 实例对象
     * @return 实例对象
     */
    @Override
    public UserCard insert(UserCard userCard) {
        this.ctmsUserCardDao.insert(userCard);
        return userCard;
    }


}