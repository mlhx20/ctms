package com.szit.controller;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;
import com.szit.model.service.TransRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/record")
@Controller
public class TransRecordsController {
    @Autowired
    private TransRecordsService trs;
    @RequestMapping("/list")
    public String TransRecords()
    {

        return "transrecordsManagement";
    }
    @RequestMapping("/gettransrecords")
    @ResponseBody
    public List<TransRecords> getTransRecords(@RequestParam(required = false) String code,
                                              @RequestParam(required = false) Integer userId,
                                              @RequestParam(required = false) Integer payeeId,
                                              @RequestParam(required = false) String page,
                                              @RequestParam(required = false) String limit,
                                              HttpSession session)
    {
        TransRecords t = new TransRecords();
        PageIndexer paging = new PageIndexer();
        paging.setPageIndex(Integer.parseInt(page));
        paging.setPageSize(Integer.parseInt(limit));
       // PageIndexer page = new PageIndexer(1, 5, 0, 0);
        if (code != null && code != "") {
            t.setCode(code);
        }
        if(userId!=null)
            t.setUserId(userId);
        if(payeeId!=null)
            t.setPayeeId(payeeId);
        List<TransRecords> lists = trs.listByPage(t,paging);

        return lists;
    }

}
