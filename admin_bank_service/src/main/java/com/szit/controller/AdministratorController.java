package com.szit.controller;


import com.szit.common.ResultObj;
import com.szit.model.bean.Administrator;
import com.szit.model.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;


/**
 * 管理员控制器
 *
 * @author 李浩
 * @version 1.0 2020-04-05
 */
@Controller
@RequestMapping("sys")
public class AdministratorController {

    @Value("${expire}")
    private Long expire;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 跳转到登陆页面
     */
    @RequestMapping("toLogin")
    public String toLogin() {
        return "login";
    }

    /**
     * 登录
     * @param name
     * @param pwd
     * @param session
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public ResultObj login(String name, String pwd, HttpSession session) {

        // 登录时首先查询Redis缓存
        Administrator administrator = (Administrator) this.redisTemplate.opsForValue().get(name);
        // 若缓存中不存在，则查询数据库
        if (administrator == null) {
            administrator = this.administratorService.login(name, pwd);
            // 将登录信息保存到Redis缓存中,有效时间为expire=60000毫秒
            if (administrator != null) {
                // 设置字符串序列化器，使 key 在 Redis 中以字符串形式显示，否则会出现十六进制代码，不方便查看
                this.redisTemplate.setKeySerializer(new StringRedisSerializer());
                this.redisTemplate.opsForValue().set(name, pwd, expire, TimeUnit.MILLISECONDS);
            }
        }
        if (administrator == null) {
            return new ResultObj(-2, "用户名或密码不正确");
        }
        if (administrator != null && administrator.getPwd().equals(pwd)) {
            session.setAttribute("admin", administrator);
            return new ResultObj(200, "登陆成功");
        }
        return new ResultObj(-1, "用户名或密码不正确!!!");
    }

    /**
     * 跳转到首页
     *
     * @return
     */
    @RequestMapping("index")
    public String cardlist() {
        return "main";
    }


    /**
     * 注销登录
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();//使Session变成无效，及用户退出
        return "login";
    }
}
