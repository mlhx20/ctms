package com.szit.controller;


import com.szit.feign.OutlandsFeignClient;
import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.User;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 用户控制器
 *
 * @author 李浩
 * @since 2020-04-06 15:43:58
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
//    @Autowired
//    private OutlandsFeignClient outlandsFeignClient;

    @RequestMapping("/list")
    public String user() {
        return "UserManagement";
    }

    @RequestMapping("/getusers")
    @ResponseBody
    public List<User> getUser(@RequestParam(required = false) Integer id,
                              @RequestParam(required = false) String name,
                              @RequestParam(required = false) Integer state,
                              @RequestParam(required = false) String page,
                              @RequestParam(required = false) String limit,
                              HttpSession session) {
        User user = new User();
        PageIndexer paging = new PageIndexer();
        paging.setPageIndex(Integer.parseInt(page));
        paging.setPageSize(Integer.parseInt(limit));
        if (id != null) {
            user.setId(id);
        }
        if (name != null && name != "") {
            user.setName(name);
        }
        if (state != null) {
            user.setState(state);
        }
        session.setAttribute("id", id);
        session.setAttribute("name", name);
        session.setAttribute("state", state);
        List<User> lists = userService.listByPage(user, paging);
        return lists;
    }

//    @ResponseBody
//    @RequestMapping(value = "/reg",method = RequestMethod.POST)
//    public boolean register(User user, String pwd2){
//        System.out.println("user:"+user.toString());
//        System.out.println("pwd2"+pwd2);
//        return outlandsFeignClient.reg(user,pwd2);
//    }


    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    @ResponseBody
    public boolean register(User user){
        boolean flag = true;
        try {
            System.out.println("user:"+user.toString());
            userService.insert(user);
        }catch(Exception e){
            flag = false;
        }
        return flag;
    }
}
