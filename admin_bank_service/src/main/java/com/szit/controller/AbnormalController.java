package com.szit.controller;

import com.szit.model.bean.*;
import com.szit.model.service.AbnormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/abnormal")
public class AbnormalController {
    @Autowired
    private AbnormalService abnormalService;

@RequestMapping("/show")
public String show() {
    return "abnormalManagement";

}
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<Abnormal> listAbnormalsByPage(@RequestParam(required = false) String abnCode,
                                              @RequestParam(required = false) String abnType,
                                              @RequestParam(required = false) String abnState,
                                              @RequestParam(required = false) String page,
                                              @RequestParam(required = false) String limit,
                                              HttpSession session) {

        List<Abnormal> list = new ArrayList<Abnormal>();

        try {
            Abnormal abnormal = new Abnormal();
            if (abnCode != null && abnCode != "") {
                abnormal.setCode(Integer.parseInt(abnCode));
            }
            if (abnType != null && abnType != "") {
                abnormal.setType(Integer.parseInt(abnType));
            }
            if (abnState != null && abnState != "") {
                abnormal.setState(Integer.parseInt(abnState));
            }

            PageIndexer paging = new PageIndexer();
            if (page != null && page != "") {
                paging.setPageIndex(Integer.parseInt(page));
            } else {
                paging.setPageIndex(1);
            }
            if (limit != null && limit != "") {
                paging.setPageSize(Integer.parseInt(limit));
            } else {
                paging.setPageSize(10);
            }

            session.setAttribute("abnCode", abnCode);
            session.setAttribute("abnType", abnType);
            session.setAttribute("abnState", abnState);
            session.setAttribute("paging", paging);

            list = abnormalService.listAbnormalsByPage(abnormal, paging);
            System.out.println("ctrl list: " + list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Boolean update(@RequestBody Map map) {
        System.out.println("ctrl map: " + map.get("abnState").toString());
        boolean flag = abnormalService.update(Integer.parseInt(map.get("abnId").toString()),
                Integer.parseInt(map.get("abnState").toString()));
        return flag;
    }
}

