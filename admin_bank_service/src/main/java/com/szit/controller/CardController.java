package com.szit.controller;


import com.szit.common.BankNumber;
import com.szit.model.bean.*;
import com.szit.model.bean.Currency;
import com.szit.model.service.CardService;
import com.szit.model.service.CurrencyService;
import com.szit.model.service.UserCardService;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

/**
 * 银行卡控制器
 *
 * @author 李浩
 * @version 1.0 2020-04-05
 */
@Controller
@RequestMapping("/card")
public class CardController {

    @Autowired
    private CardService cardService;
    @Autowired
    private UserCardService userCardService;
    @Autowired
    private UserService userService;


    @Autowired
    private CurrencyService currencyService;


    /**
     * 跳转到cardcardManagement管理页面
     *
     * @return
     */
    @RequestMapping("/list")
    public String list() {
        return "cardManagement";
    }

    /**
     * 添加银行卡
     */
    @RequestMapping("addCard")
    @ResponseBody
    private Map<String, Object> add(@RequestBody Map jsonData) {
        Map<String, Object> map = new HashMap<>();
        int result = 0;//添加银行卡返回的结果
        Integer currencyType = 0;//货币类型
        String idCard = "";//身份证
        Integer cardSign = 0;//银行卡签约
        Double bankcardGarde = 0.00;//银行卡等级
        String bankCardNo = "";//银行卡号码
        int isFindUserById = 0;//shi否存在用户id
        String regex = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
                "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        String pwd = "";
        String password = "";
        //System.out.println("shuju"+jsonData);
        User user = new User();
        Card card = new Card();
        Card cardBank = new Card();
        UserCard userCard = new UserCard();

        if (jsonData.containsKey("idCard")) {
            idCard = jsonData.get("idCard").toString();
        }
        if (jsonData.containsKey("pwd")) {
            pwd = jsonData.get("pwd").toString();
        }
        if (jsonData.containsKey("password")) {
            password = jsonData.get("password").toString();
        }
        if (jsonData.containsKey("openbank")) {
            card.setOpenbank(jsonData.get("openbank").toString());
        }
        if (jsonData.containsKey("balance")) {
            card.setBalance(new BigDecimal(jsonData.get("balance").toString()));
        }
        if (jsonData.containsKey("currencyType")) {
            currencyType = Integer.parseInt(jsonData.get("currencyType").toString());
        }
        if (jsonData.containsKey("type")) {
            card.setType(Integer.parseInt(jsonData.get("type").toString()));
        }
        if (jsonData.containsKey("cardSign")) {
            cardSign = Integer.parseInt(jsonData.get("cardSign").toString());
        }

        bankcardGarde = Double.parseDouble(jsonData.get("balance").toString());
        if (bankcardGarde >= 0.01 && bankcardGarde <= 100000.00) {
            card.setSort(1);
        } else if (bankcardGarde > 100000.00 && bankcardGarde <= 1000000000.00) {
            card.setSort(2);
        } else {
            card.setSort(3);
        }

        bankCardNo = BankNumber.getRandom(15);

        do {
            cardBank = cardService.searchByBankNo(bankCardNo);
        } while (cardBank != null);

        card.setNo(bankCardNo);
        card.setState(1);
        card.setOpendate(new Date());
        card.setBankid(1);
        card.setCurrencyid(currencyType);

        if (idCard.matches(regex)) {
            user = userService.searchUserById(idCard);
        }
        if (Integer.parseInt(card.getBalance().toString()) > 0 && pwd.equals(password) && ((pwd.length() > 5) && (pwd.length() < 13))) {
            card.setPwd(pwd);
            if (user.getId() > 0) {
                result = cardService.insert(card);
            }
        }

        isFindUserById = cardService.findById(card.getNo());

        if (result > 0) {
            userCard.setUserId(user.getId());
            userCard.setCardId(isFindUserById);
            userCard.setCardSign(cardSign);
            userCardService.insert(userCard);
            map.put("status", 1);
        } else {
            map.put("status", 0);
        }
        return map;
    }

    /**
     * 修改银行卡
     *
     * @param jsonData
     * @return
     */
    @RequestMapping("updateCard")
    @ResponseBody
    private Map<String, Object> update(@RequestBody Map jsonData) {
        Map<String, Object> map = new HashMap<String, Object>();
        Card card = new Card();
        String no = null;
        String pass = null;
        String turePwd = null;
        int result = 0;
        if (jsonData.containsKey("no")) {
            no = jsonData.get("no").toString();
        }
        if (jsonData.containsKey("pass")) {
            pass = jsonData.get("pass").toString();
        }
        if (jsonData.containsKey("turePwd")) {
            turePwd = jsonData.get("turePwd").toString();
        }
        //System.out.println(card.toString());
        if (turePwd.equals(pass)) {
            if ((pass.length() > 5) && (pass.length() < 13)) {
                card.setNo(no);
                card.setPwd(pass);
                result = cardService.update(card);
            }
        }
        if (result > 0) {
            map.put("status", 1);
        } else {
            map.put("status", 0);
        }
        return map;
    }

//    /**
//     * 加载card所有数据
//     */
//    @RequestMapping("getAllLisr")
//    @ResponseBody
//    public List<Card> getAllLisr() {
//        return cardService.findAll();
//    }

    /**
     * 返回货币类型
     *
     * @return
     */
    @RequestMapping("currencyTpey")
    @ResponseBody
    public List<Currency> getCurrencyName() {
        return currencyService.selectCurrencyType();
    }

    /**
     * 银行卡分页查询
     */
    @RequestMapping("/getAllLisr")
    @ResponseBody
    public List<Card> getCard(@RequestParam(required = false) String no,
                              @RequestParam(required = false) Integer state,
                              @RequestParam(required = false) Integer bankid,
                              @RequestParam(required = false) String page,
                              @RequestParam(required = false) String limit,
                              HttpSession session) {
        Card card = new Card();
        PageIndexer paging = new PageIndexer();

        if (no != null) {
            card.setNo(no);
        }
        if (state != null) {
            card.setState(state);
        }
        if (bankid != null) {
            card.setBankid(bankid);
        }
        session.setAttribute("no", no);
        session.setAttribute("state", state);
        session.setAttribute("bankid", bankid);
        paging.setPageIndex(Integer.parseInt(page));
        paging.setPageSize(Integer.parseInt(limit));
        //PageIndexer paging = new PageIndexer(1, 5, 0, 0);
        List<Card> lists = cardService.listByPage(card, paging);

        return lists;
    }
}



