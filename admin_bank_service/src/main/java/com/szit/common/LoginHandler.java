package com.szit.common;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author ：李浩
 * @version :1.0
 * @description:登陆拦截器
 * @date ：create in 2020/4/17 0017 15:52
 */
public class LoginHandler implements HandlerInterceptor {

    /**
     * @Author: Pan
     * @Description: preHandle 顾名思义 请求完成之前
     * @Param: [request, response, handler]
     * @Return: false 就是终止操作,true就是请求继续
     * @Date: 2020\4\7 0007 21:19
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        //如果session为空  重定向 到 overTime 页面
        if(session.getAttribute("admin") == null){
            response.sendRedirect("toLogin");
            return false;
        }
        return true;
    }
}
