package com.szit.common;

import java.util.Random;

/**
 * @author ：李浩
 * @version :1.0
 * @description: 银行卡号码
 * @date ：create in 2020/4/14 0014 16:43
 */
public class BankNumber {
    /**
     * 生成指定位数的随机数
     * @param length
     * @return
     */
    public static String getRandom(int length) {
        String val = "6621";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            val += String.valueOf(random.nextInt(10));
        }
        return val;
    }

    public static void main(String[] args) {
        System.out.println(getRandom(15));
    }
}
