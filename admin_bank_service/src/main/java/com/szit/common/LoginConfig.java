package com.szit.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：小木
 * @version :1.0
 * @description: 登陆拦截器
 * @date ：create in 2020/4/17 0017 16:15
 */
@Configuration
public class LoginConfig implements WebMvcConfigurer {

    /**
     * @Author: Pan
     * @Description: 拦截 未登录的请求
     * @Param: [registry]
     * @Return: void
     * @Date: 2020\4\7 0007 21:17
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //通过registry.addInterceptor()放法，添加一个自定的拦截适配器
        //也就是上一步已经创建好的 LoginHandler  类
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(new LoginHandler());
        //addPathPatterns()方法 -> 添加需要拦截的 url
        interceptorRegistration.addPathPatterns("/card/**");
        interceptorRegistration.addPathPatterns("/sys/index");
        //excludePathPatterns() -> 排除不要拦截的 url
        interceptorRegistration.excludePathPatterns("/redirect:login.html")
                .excludePathPatterns("/cardManagement.html")
                .excludePathPatterns("/main.html")
                .excludePathPatterns("/**/**.js")
                .excludePathPatterns("/**/**.css")
                .excludePathPatterns("/**/**.png")
                .excludePathPatterns("/**/**.otf")
                .excludePathPatterns("/**/**.svg")
                .excludePathPatterns("/**/**.ttf")
                .excludePathPatterns("/**/**.woff")
                .excludePathPatterns("/**/**.woff2")
                .excludePathPatterns("/**/**.eot")
                .excludePathPatterns("/**/**.woff")
                .excludePathPatterns("/**/**.jpg");
    }
}
