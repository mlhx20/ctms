layui.use(['form', 'jquery', 'layer', 'laydate', 'table'], function () {
    var form = layui.form;
    var $ = layui.$;
    var layer = layui.layer;
    var laydate = layui.laydate;
    var table = layui.table;

    //监听提交
    form.on('submit(update-user-form-submit)', function (data) {
        // TODO 校验
        //formSubmit(data);
        return false;
    });

    form.verify({
        pwd: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],

        confirmPass: function (value) {
            if ($('input[name=pwd]').val() !== value)
                return '两次密码输入不一致！';
        },
        idCard: [
            /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, '请输入正确的身份证号'
        ],
        balance: [
            /(^[1-9](\d+)?(\.\d{1,2})?$)|(^\d\.\d{1,2}$)/, '您输入的金额不正确'
        ],
        balance: [
            /^[0-9]*$/, '您输入的字符非法'
        ],
        pass: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],
        turePwd: function (value) {
            if ($('input[name=pass]').val() !== value)
                return '两次密码输入不一致！';
        },
    });


    $.ajax({
        url: '/card/currencyTpey',
        dataType: 'json',
        type: 'post',
        success: function (data) {
            if (data !== null) {
                $("#currencyType").empty();
                $("#currencyType").append(new Option("人民币(默认选项)",1));
                $.each(data, function (index, item) {
                    $('#currencyType').append(new Option(item.sign, item.id));
                });
            } else {
                $("#currencyType").append(new Option("暂无数据", ""));
            }
            //重新渲染
            form.render("select");
        }
    });


    /*显示所有银行卡信息*/
    var tableIns = table.render({
        elem: '#cardList'
        , url: '/card/getAllLisr'
        , toolbar: true //开启头部工具栏，并为其绑定左侧模板
        , title: '银行卡数据表'
        , page: true
        , parseData: function (res) {
            console.log(res);
            return {
                "code": 0,
                "msg": "",
                "count": 1000,
                data: res
            }
        }
        , cols:
            [ [
                {field: 'id', title: '银行卡id', align: 'center'}
                , {field: 'no', title: '银行卡号', align: 'center', sort: true}
                , {field: 'pwd', title: '银行卡密码', align: 'center'}
                , {field: 'balance', title: '余额', align: 'center'}
                , {field: 'opendate', title: '开卡日期', align: 'center'}
                , {field: 'openbank', title: '开户行', align: 'center'}
                , {field: 'bankid', title: '开户行ID', align: 'center'}
                , {field: 'state', title: '卡状态', align: 'center', templet: function (data) {
                        if (data.state === 1) {
                            return '正常';
                        } else if (data.state === 2) {
                            return '可疑';
                        }
                    }}
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', align: 'center'}
            ] ]
    });
    //监听搜索框
    form.on('submit(searchSubmit)', function (data) {
        // alert("data: " + data);
        //console.log("data2: " + data)
        load(data);

        function load(obj) {
            //重新加载table
            tableIns.reload({
                where: obj.field
            });
        }

        return false;//false：阻止表单跳转  true：表单跳转
    });

    $('#view-card-btn').click(function () {

        layer.open({
            type: 1,
            title: '查看银行卡',
            skin: 'layui-layer-molv',
            area: ['700px'],
            content: $('#view-card-layer')
        });
    });

    //显示添加用户弹出层
    $('#add-user-btn').click(function () {
        // 每次显示前重置表单
        $('#add-user-form')[0].reset();
        layer.open({
            type: 1,
            title: '添加银行卡',
            skin: 'layui-layer-molv',
            area: ['700px'],
            content: $('#add-user-layer')
        });
    });

    // 添加用户表单提交
    form.on('submit(add-user-form-submit)', function (data) {
        // ajax方式添加用户
        $.ajax({
            url: "/card/addCard",
            type: "POST",
            data: JSON.stringify(data.field),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    layer.close(layer.index);
                    layer.msg('添加成功');
                    table.reload('user-tbl');
                    window.location.reload()  //刷新页面
                }
                else {
                    layer.msg('添加失败 该用户未开户 请先开户！');
                }
            },
            error: function () {
                layer.msg('添加失败 该用户未开户 请先开户 ');
                console.log("ajax error");
            }

        });

        //检查项目添加到下拉框中
        $.ajax({
            url: 'card/currencyTpey',
            dataType: 'json',
            type: 'get',
            success: function (data) {
                $.each(data, function (index, item) {
                    $('#currencyType').append(new Option(item.name, item.id));// 下拉菜单里添加元素
                });
                layui.form.render("select");
                //重新渲染 固定写法
            }
        })
        // 阻止表单跳转
        return false;
    });

    //监听行工具事件
    /*编辑*/
    table.on('tool(cardList)', function (obj) {
        // 获取当前行数据和lay-event的值
        var data = obj.data;
        var event = obj.event;
        // 更新用户事件
        if (event === 'updateUser') {

            // 每次显示更新用户的表单前自动为表单填写该行的数据
            form.val('update-user-form', {
                "no": data.no,
                "pwd": data.pwd,
                //"openbank": data.openbank,
                //"balance": data.balance,
                "currencyid": data.currencyid
            });
            // 显示更新用户表单的弹出层
            layer.open({
                type: 1,
                title: '更新',
                skin: 'layui-layer-molv',
                area: ['500px'],
                content: $('#update-user-layer')
            });
            // 更新用户表单提交
            form.on('submit(update-user-form-submit)', function (data) {
                // ajax方式更新用户
                $.ajax({
                    url: "/card/updateCard",
                    type: "PUT",
                    data: JSON.stringify(data.field),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 1) {
                            layer.close(layer.index);
                            layer.msg('更新成功');
                            table.reload('user-tbl');
                            window.location.reload()  //刷新页面
                        } else {
                            layer.msg('更新失败');
                        }
                    },
                    error: function () {
                        console.log("ajax error");
                    }
                });
                // 阻止表单跳转
                return false;
            });
        }

        if (event === 'viewCard') {
            form.val('view-card-form', {
                "id": data.id,
                "no": data.no,
                "pwd": data.pwd,
                "type": data.type,
                "balance": data.balance,
                "opendate": data.opendate,
                "openbank": data.openbank,
                "bankid": data.bankid,
                "state": data.state,
                "currency":data.currency.name
            });

            layer.open({
                type: 1,
                title: '查看',
                skin: 'layui-layer-molv',
                area: ['500px'],
                content: $('#view-card-layer')
            });
        }
    });
});

//下拉框绑定
function addoption() {
    $.post("/card/selectCurrencyTyp",
        function (obj) {
            console.log(obj);
            $("#typeID").empty();
            $("#typeID").append("<option value=" + 0 + ">" + "----请选择----" + "</option>");
            for (var i = 0; i < obj.data.length; i++) {
                $("#hobbyID").append("<option value=" + obj.data[i].typeID + ">" + obj.data[i].name + "</option>");
                //每次动态添加下拉框都要重新手动渲染一次
                form.render();
            }
        }, "json");
}

