/**
 * 异常管理
 */
var pageCurr;
var form;
$(function() {

    layui.use(['form', 'jquery', 'layer', 'laydate', 'table', 'laypage'], function () {
        var form = layui.form;
        var $ = layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;
        var table = layui.table;

        /*显示所有异常信息*/
        var tableIns = table.render({
            elem: '#abnormalList'
            , url: '/abnormal/list'
            , method: 'post'
            , toolbar: true //开启头部工具栏，并为其绑定左侧模板
            , title: '异常数据表'
            , page: true
            , parseData: function (res) {
                console.log(res);
                return {
                    "code": 0,
                    "msg": "",
                    "count": 10000000,
                    data: res
                }
            }

            , cols:
                [ [
                    {field: 'id', title: '异常ID', align: 'center', sort: true}
                    , {field: 'code', title: '异常编号', align: 'center', sort: true}
                    , {
                        field: 'transRecords', title: '交易编号', align: 'center'
                        , templet: function (data) {
                            return data.transRecords.code;
                        }
                    }
                    , {
                        field: 'user', title: '用户姓名', align: 'center'
                        , templet: function (data) {
                            return data.user.name;
                        }
                    }
                    , {
                        field: 'transRecords', title: '交易时间', align: 'center', sort: true
                        , templet: function (data) {
                            return data.transRecords.time;
                        }
                    }
                    , {
                        field: 'type', title: '异常类型', align: 'center', templet: function (data) {
                            if (data.type === 1) {
                                return '反洗黑钱异常';
                            } else if (data.type === 2) {
                                return '交易异常';
                            }
                        }
                    }
                    , {
                        field: 'state', title: '异常状态', align: 'center', templet: function (data) {
                            if (data.state === 0) {
                                return '未处理';
                            } else if (data.state === 1) {
                                return '已处理';
                            } else if (data.state === 2) {
                                return '正在处理';
                            }
                        }
                    }
                    , {fixed: 'right', title: '详情', toolbar: '#barDemo', align: 'center'}

                ] ]
        });

        //监听搜索框
        form.on('submit(searchSubmit)', function (data) {
            load(data);
            function load(obj){
                //重新加载table
                tableIns.reload({
                    where: obj.field

                });

            }
            return false;//false：阻止表单跳转  true：表单跳转

        });

        //监听工具条
        table.on('tool(abnormalList)', function (obj) {
            var data = obj.data;
            var event = obj.event;
            console.log("data:"+data)
            if (event === 'view') {
                // 每次显示更新用户的表单前自动为表单填写该行的数据
                form.val('update-abnormal-form', {
                    "abnId": data.id,
                    "abnCode": data.code,
                    "abnType": data.type,
                    "transCode": data.transRecords.code,
                    "transAmount": data.transRecords.amount,
                    "typeName": data.typeTrade.name,
                    "transTime": data.transRecords.time,
                    "transPurpose": data.transRecords.purpose,
                    "userCode": data.user.code,
                    "userRealName": data.user.realName,
                    "remitCardNo": data.card.no,
                    "remitCardBank": data.bank.name,
                    "remitCardBalance": data.card.balance,
                    "userIdcard": data.user.idCard,
                    "userPhone": data.user.phone,
                    "userAddress": data.user.address,
                    "payeeCode": data.payee.code,
                    "payeeName": data.payee.name,
                    // "recvCardNo": data.card.no,
                    // "recvCardBank": data.bank.name,
                    // "recvCardBalance": data.card.balance,
                    "payeePhone": data.payee.phone,
                    "payeeAddress": data.payee.address,
                    "curName": data.currency.name,
                    "abnState": data.state
                });
                // 显示更新用户表单的弹出层
                layer.open({
                    type: 1,
                    title: '更新',
                    skin: 'layui-layer-molv',
                    area: ['700px', '650px'],
                    content: $('#update-abnormal-layer')
                });
                // 更新用户表单提交
                form.on('submit(updateSubmit)', function (data) {
                    // ajax方式更新用户
                    $.ajax({
                        url: "/abnormal/update",
                        type: "POST",
                        data: JSON.stringify(data.field),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                layer.close(layer.index);
                                layer.msg('更新成功');
                                // table.reload('user-tbl');
                                window.location.reload()  //刷新页面
                            } else {
                                layer.msg('更新失败');
                            }
                        },
                        error: function () {
                            console.log("ajax error");
                        }
                    });
                    // 阻止表单跳转
                    return false;
                });

            }
        });

    });

});


