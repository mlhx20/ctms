layui.use(['form', 'jquery', 'layer', 'laydate', 'table'], function () {
    var form = layui.form;
    var $ = layui.$;
    var layer = layui.layer;
    var laydate = layui.laydate;
    var table = layui.table;

    var tableIns = table.render({
        elem: '#recordList'
        , url: '/record/gettransrecords'
        , toolbar: true //开启头部工具栏，并为其绑定左侧模板
        , title: '交易记录数据表'
        , page: true
        , parseData: function (res) {
            console.log(res);
            return {
                "code": 0,
                "msg": "",
                "count": 1000,
                data: res
            }
        }
        , cols:
            [ [
                {field: 'id', title: '交易记录ID', align: 'center'},
                {field: 'code', title: '交易编码', align: 'center'},
                {field: 'userId', title: '汇款用户ID', align: 'center'},
                {field: 'payeeId', title: '收款人ID', align: 'center'},
                {field: 'typeId', title: '交易类型ID', align: 'center'},
                {field: 'curId', title: '货币ID', align: 'center'},
                {field: 'state', title: '交易状态', align: 'center', templet: function (data) {
                        if (data.state === 1) {
                            return '正常';
                        } else if (data.state === 2) {
                            return '可疑';
                        }
                    }},
                {fixed: 'right', title: '操作', toolbar: '#barDemo', align: 'center'}

            ] ]
    });
//监听搜索框
    form.on('submit(searchSubmit)', function (data) {
        load(data);
        function load(obj) {
            //重新加载table
            tableIns.reload({
                where: obj.field
            });
        }
        return false;//false：阻止表单跳转  true：表单跳转
    });
    //监听行工具事件
    table.on('tool(recordList)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data //获得当前行数据
            ,layEvent = obj.event; //获得 lay-event 对应的值
        if(layEvent === 'vtr') {
            form.val('view-record-form', {
                "code": data.code,
                "userRealName": data.user.realName,
                "payeeName": data.payee.name,
                "remitCard": data.remitCard.no,
                "receivCard": data.receivCard.no,
                "curName": data.currency.name,
                "typeName":data.typeTrade.name,
                "time":data.time,
                "state":data.state,
                "purpose":data.purpose,
                "amount":data.amount
            });

            layer.open({
                type: 1,
                title: '查看',
                skin: 'layui-layer-molv',
                area: ['500px'],
                content: $('#view-record-layer')
            });
        }
    });
})
