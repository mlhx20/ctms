$(function () {
    layui.use(['form', 'jquery', 'layer', 'laydate', 'table'], function () {
        var form = layui.form;
        var $ = layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;
        var table = layui.table;

        form.verify({
            pwd: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],

            confirmPass: function (value) {
                if ($('input[name=pwd]').val() !== value)
                    return '两次密码输入不一致！';
            },
            idCard: [
                /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, '请输入正确的身份证号'
            ],
        });

        /*显示所有银行卡信息*/
        var tableIns = table.render({
            elem: '#userlist'
            , url: '/user/getusers'
            , toolbar: true //开启头部工具栏，并为其绑定左侧模板
            , title: '用户数据表'
            , page: true
            , parseData: function (res) {
                console.log("res "+res);
                return {
                    "code": 0,
                    "msg": "",
                    "count": 10000,
                    data: res
                }
            }
            , cols:
                [[
                    {field: 'id', title: '用户id', align: 'center'},
                    {field: 'name', title: '姓名', align: 'center'},
                    {field: 'type', title: '用户类型', align: 'center'},
                    {field: 'state', title: '用户状态', align: 'center'
                        , templet: function (data) {
                            if (data.state === 1) {
                                return '正常';
                            } else if (data.state === 2) {
                                return '可疑';
                            }
                        }
                        },
                    {fixed: 'right', title: '操作', toolbar: '#barDemo', align: 'center'}
                ]]
        });

        //显示添加用户弹出层
        $('#add-user-btn').click(function () {
            // 每次显示前重置表单
            $('#add-user')[0].reset();
            layer.open({
                type: 1,
                title: '添加用户',
                skin: 'layui-layer-molv',
                area: ['700px'],
                content: $('#add-layer')
            });
        });

        // 添加用户表单提交
        form.on('submit(add-user-submit)', function (data) {

            // ajax方式添加用户
            $.ajax({
                url: "/user/reg",
                type: "POST",
                data: JSON.stringify(data.field),
                contentType: 'application/json',
                dataType: 'json',

                success: function (data) {
                    if (data) {
                        layer.close(layer.index);
                        layer.msg('添加成功');
                        //table.reload('user-tbl');
                        //window.location.reload()  //刷新页面
                    }
                    else {
                        layer.msg('添加失败');
                    }
                },
                error: function () {
                    layer.msg('添加失败');
                    console.log("ajax error");
                }

            });
        });

        //监听搜索框
        form.on('submit(searchSubmit)', function (data) {
            load(data);
            function load(obj) {
                //重新加载table
                tableIns.reload({
                    where: obj.field
                });
            }
            return false;//false：阻止表单跳转  true：表单跳转
        });

        //监听行工具事件
        table.on('tool(userlist)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'viewUser') {
                form.val('view-user-form', {
                    "id": data.id,
                    "code": data.code,
                    "name": data.name,
                    "pwd": data.pwd,
                    "realName": data.realName,
                    "idCard": data.idCard,
                    "state": data.state,
                    "type": data.type,
                    "address": data.address,
                });

                layer.open({
                    type: 1,
                    title: '查看',
                    skin: 'layui-layer-molv',
                    area: ['500px'],
                    content: $('#view-user-layer')
                });
            }
        });

    });
});
