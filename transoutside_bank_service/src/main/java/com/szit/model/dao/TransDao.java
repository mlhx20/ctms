package com.szit.model.dao;

import com.szit.model.bean.Transbank;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Mapper
public interface TransDao {

    //  通过，用户名和用户卡号查询用户信息
    public Transbank getCardAndName(Transbank transbank);

    //根据卡号查询用户余额
    public double queryBalanceByCardNo(@Param("usercard") String usercard);

    //通过卡号修改用户余额
    public int updateBalanceByCardNo(Transbank transbank);

}
