package com.szit.model.service;

import com.szit.model.bean.Transbank;

import java.util.Map;

public interface TransService {

    //  通过，用户名和用户卡号查询用户信息
    public int isCardAndName(Transbank transbank);

    //通过卡号修改用户余额
    public int searchBalanceByCardNo(Transbank transbank);

    //通过卡号修改用户余额 1:修改成功 0：修改失败
    public int searchFundBalanceByCardNo(String usercard,double gruanMoney);

}
