package com.szit.service.impl;

import com.szit.model.bean.Transbank;
import com.szit.model.dao.TransDao;
import com.szit.model.service.TransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TransServiceImpl implements TransService {

    @Autowired
    private TransDao transDao;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public int isCardAndName(Transbank transbank) {

        if(transDao.getCardAndName(transbank)!=null){
            return 1;
        }

        return 0;
    }

    //通过卡号修改用户余额 1:修改成功 0：修改失败
    @Override
    public int searchBalanceByCardNo(Transbank transbank) {

        double balance = transDao.queryBalanceByCardNo(transbank.getUsercard());

        double finaBalance = balance+transbank.getUserbalance();

        Transbank trans = new Transbank();
        trans.setUsercard(transbank.getUsercard());
        trans.setUserbalance(finaBalance);

        return transDao.updateBalanceByCardNo(trans);
    }

    @Override
    public int searchFundBalanceByCardNo(String usercard, double gruanMoney) {
        return 0;
    }


}
