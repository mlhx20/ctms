package com.szit.service.impl;

import com.szit.model.bean.Payee;
import com.szit.model.dao.PayeeDao;
import com.szit.model.service.PayeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PayeeServiceImpl  implements PayeeService {

    @Autowired
    private PayeeDao payeeDao;
    @Override
    public List<Payee> getPayeeByUserId(Integer userId) {
        return payeeDao.queryByUserId(userId);
    }

}
