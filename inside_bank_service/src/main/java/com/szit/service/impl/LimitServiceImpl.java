package com.szit.service.impl;

import com.szit.model.bean.Limit;
import com.szit.model.dao.LimitDao;
import com.szit.model.service.LimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class LimitServiceImpl implements LimitService {
    @Autowired
    private LimitDao limitDao;
    @Override
    public Limit getLimitByUserId(Integer userId) {
        return limitDao.queryLimitByUserId(userId);
    }

}
