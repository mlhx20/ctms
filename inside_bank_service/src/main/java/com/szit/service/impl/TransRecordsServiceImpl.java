package com.szit.service.impl;

import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import com.szit.model.dao.CardDao;
import com.szit.model.dao.TransRecordsDao;
import com.szit.model.service.TransRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TransRecordsServiceImpl implements TransRecordsService {
    @Autowired
    private TransRecordsDao transRecordsDao;
    @Autowired
    private CardDao cardDao;

    @Override
    public List<TransRecords> getTransRecordsByUserId(Integer userId) {
        List<TransRecords> lists=transRecordsDao.queryByUserId(userId);
        for(TransRecords transRecords:lists){
            transRecords.setRemitCard(cardDao.queryByCardId(transRecords.getRemitCardId()));
            transRecords.setReceivCard(cardDao.queryByCardId(transRecords.getReceivCardId()));
        }
        return lists;
    }

    @Override
    public  List<TransRecords>  queryTransRecordsByUserIdAndState(Integer userId, Integer state) {
        List<TransRecords> transRecords=transRecordsDao.queryTransRecordsByUserIdAndState(userId,state);
        if(transRecords!=null){
            for(TransRecords transRecords1:transRecords){
                transRecords1.setReceivCard(cardDao.queryByCardId(transRecords1.getReceivCardId()));
            }
        }

        return transRecords;
    }

    @Override
    public TransRecords queryByTransId(Integer transId) {
        TransRecords transRecords=transRecordsDao.queryByTransId(transId);
        transRecords.setReceivCard(cardDao.queryByCardId(transRecords.getReceivCardId()));
        return transRecords;
    }

    @Override
    public BigDecimal getDayAmount(Integer userId) {
        BigDecimal amount=new BigDecimal("0");
        List<TransRecords> lists=transRecordsDao.queryNow(userId);
        if(lists.size()!=0) {
            for (TransRecords transRecords : lists) {
                if (transRecords.getState() == 1) {
                    if(transRecords.getAmount()!=null){
                        amount.add(transRecords.getAmount());
                    }
                }
            }
        }
        return amount;
    }

    @Override
    public boolean updateTransRecordsState(Integer state, Integer id) {
        boolean flag= false;
        if(transRecordsDao.updateTransRecordsState(state,id)>0){
            flag=true;
        }
        return flag;
    }

    @Override
    public boolean insertTransRecords(TransRecords transRecords) {
        int flag=transRecordsDao.insertTransRecords(transRecords);
        if(flag>=1){
            return true;
        }
        return false;
    }
}
