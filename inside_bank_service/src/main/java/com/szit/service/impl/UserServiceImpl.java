package com.szit.service.impl;

import com.szit.model.bean.Card;
import com.szit.model.bean.User;
import com.szit.model.dao.UserDao;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public List<User> findAll() {
        return userDao.queryAll();
    }

    @Override
    public List<Integer> findAllUserId() {
        return userDao.queryUserId();
    }

    @Override
    public User queryUserByUserName(String userName) {
        return userDao.queryUserByUserName(userName);
    }

    @Override
    public User queryUserByCardId(Integer cardId) {
        return userDao.queryUserByCardId(cardId);
    }

}
