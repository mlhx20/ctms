package com.szit.service.impl;

import com.szit.model.bean.Card;
import com.szit.model.dao.CardDao;
import com.szit.model.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private CardDao cardDao;

    @Override
    public boolean updateCardSign(Integer cardSign, Integer userId, Integer cardId) {
        int flag=cardDao.updateCardSign(cardSign,userId,cardId);
        if(flag>=1){
            return true;
        }
        return false;
    }

    @Override
    public Card findCardByCardNo(String no) {
        return cardDao.getCardByCardNo(no);
    }

    @Override
    public List<Card> findCardByUserIdAndCardSign(Integer userId, Integer cardSign) {
        return cardDao.queryByUserIdAndCardSign(userId,cardSign);
    }

    @Override
    public Card findCardByCardId(Integer cardId) {
        return cardDao.getCardByCardId(cardId);
    }

    @Override
    public List<Card> findCardByPayId(Integer payId) {
        return cardDao.getCardByCardPayId(payId);
    }

    @Override
    public List<Card> findCardByUserId(Integer userId) {
        return cardDao.queryByUserId(userId);
    }

    @Override
    public boolean insedeTran(Card remitCard, Card receivCard, BigDecimal money) {
        boolean sucess=true;
        try {
            remitCard.setBalance(remitCard.getBalance().subtract(money));
            receivCard.setBalance(receivCard.getBalance().add(money));
            System.out.println(receivCard.getBalance()+"----"+remitCard.getBalance()+"---"+money);
            int flag1 =cardDao.updateCard(remitCard);
            int flag2 =cardDao.updateCard(receivCard);
            if(flag1>=1 && flag2>=1){
                sucess=true;
            }
        }catch (Exception e){
            e.printStackTrace();
            sucess=false;
        }
        return sucess;
    }

    @Override
    public boolean deleteCardById(Integer cardId) {
        boolean sucess = false;
        int flag = cardDao.deleteById(cardId);
        if (flag >= 1) {
            sucess = true;
        }
        return sucess;
    }

    @Override
    public int isInsideCard(String no) {
        int flag=-1;
        Card returnCard=cardDao.getCardByCardNo(no);
        if(returnCard!=null){
            if(returnCard.getBankid()==1){
                flag=1;
            }else{
                flag=0;
            }
        }
        return flag;
    }
}
