package com.szit.feign;

import com.szit.model.bean.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PayeeFeignClientFallBack implements PayeeFeignClient {
    /**
     * 熔断降级的方法
     * @return
     */
    @Override
    public List<User> getUsersByPayee() {
        //此处返回结果根据需要显示数据，注意设置后不能让页面操作数据库，只用于页面显示
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setName("张三丰");
        user.setPhone("18756478222");
        users.add(user);
        return users;
    }
}
