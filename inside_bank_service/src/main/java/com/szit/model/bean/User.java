package com.szit.model.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户实体类
 */
public class User implements Serializable {
    private Integer id;
    private String code;
    private String name; //用户名
    private String pwd;
    private String realName; //真实姓名
    private String idCard; //身份证号
    private Date birthday;
    private String address;
    private Integer postalcod; //邮政编码
    private String phone;
    private Date opendate;
    private Integer state;
    private Integer type;
    private Integer sort;
    private String privateKey;
    private String publicKey;

    public User() {
    }

    public User(Integer id, String code, String name, String pwd, String realName, String idCard, Date birthday, String address, Integer postalcod, String phone, Date opendate, Integer state, Integer type, Integer sort, String privateKey, String publicKey) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.pwd = pwd;
        this.realName = realName;
        this.idCard = idCard;
        this.birthday = birthday;
        this.address = address;
        this.postalcod = postalcod;
        this.phone = phone;
        this.opendate = opendate;
        this.state = state;
        this.type = type;
        this.sort = sort;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPostalcod() {
        return postalcod;
    }

    public void setPostalcod(Integer postalcod) {
        this.postalcod = postalcod;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getOpendate() {
        return opendate;
    }

    public void setOpendate(Date opendate) {
        this.opendate = opendate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
