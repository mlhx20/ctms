package com.szit.model.bean;

import java.io.Serializable;

/**
 * 银行实体类
 */
public class Bank implements Serializable {
    private Integer id;
    private String name;
    private String country;
    private String address;
    private Integer state;

    public Bank() {
    }

    public Bank(Integer id, String name, String country, String address, Integer state) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.address = address;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
