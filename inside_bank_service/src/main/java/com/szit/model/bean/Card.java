package com.szit.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 银行卡实体类
 */
public class Card implements Serializable {
    private Integer id;
    private String no; //银行卡号
    private String pwd;
    private Integer type;
    private Date opendate;
    private BigDecimal balance; //卡余额
    private String openbank; //开户行
    private Integer state; //卡状态
    private Integer bankid; //银行id
    private Bank bank; //银行实体类
    private Integer sort; //排序
    private Currency currency; //货币种类实体类
    private Integer currencyid; //货币id
    public Card() {
        this.bank = new Bank();
        this.currency = new Currency();
    }

    public Card(Integer id, String no, String pwd, Integer type, BigDecimal balance, Date opendate, String openbank, Integer state, Integer bankid, Bank bank, Integer sort, Currency currency, Integer currencyid) {
        this.id = id;
        this.no = no;
        this.pwd = pwd;
        this.type = type;
        this.balance = balance;
        this.opendate = opendate;
        this.openbank = openbank;
        this.state = state;
        this.bankid = bankid;
        this.bank = bank;
        this.sort = sort;
        this.currency = currency;
        this.currencyid = currencyid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getOpendate() {
        return opendate;
    }

    public void setOpendate(Date opendate) {
        this.opendate = opendate;
    }

    public String getOpenbank() {
        return openbank;
    }

    public void setOpenbank(String openbank) {
        this.openbank = openbank;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getBankid() {
        return bankid;
    }

    public void setBankid(Integer bankid) {
        this.bankid = bankid;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Integer getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(Integer currencyid) {
        this.currencyid = currencyid;
    }
}
