package com.szit.model.service;

import com.szit.model.bean.Payee;

import java.util.List;

public interface PayeeService {
    /**
     * 通过用户Id搜索收款人
     * @param userId
     * @return
     */
    List<Payee> getPayeeByUserId(Integer userId);

}
