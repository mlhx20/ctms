package com.szit.model.service;

import com.szit.model.bean.Card;
import com.szit.model.bean.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    /**
     * 查询有卡的用户id
     * @return
     */
    List<Integer> findAllUserId();
    User queryUserByUserName(String userName);
    User  queryUserByCardId(Integer cardId);
}
