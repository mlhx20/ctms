package com.szit.model.service;

import com.szit.model.bean.TransRecords;

import java.math.BigDecimal;
import java.util.List;

public interface TransRecordsService {
   /**
    * 通过用户id差交易记录
    * @param userId
    * @return
    */
   List<TransRecords> getTransRecordsByUserId(Integer userId);

   /**
    * 查询未完成的交易记录
    * @param userId
    * @param state
    * @return
    */
   List<TransRecords>  queryTransRecordsByUserIdAndState(Integer userId,Integer state);

   TransRecords queryByTransId(Integer transId);
   /**
    * 查当天的交易金额
    * @param userId
    * @return
    */
   BigDecimal getDayAmount(Integer userId);

   /**
    * 修改交易记录状态
    * @param state
    * @param id
    * @return
    */
   boolean updateTransRecordsState(Integer state,Integer id);

   /**
    * 插入交易记录
    * @param transRecords
    * @return
    */
   boolean insertTransRecords(TransRecords transRecords);
}
