package com.szit.model.service;

import com.szit.model.bean.Card;
import org.springframework.cache.annotation.Cacheable;

import java.math.BigDecimal;
import java.util.List;

public interface CardService {
    /**
     * 修改卡标记
     * @param cardSign
     * @param userId
     * @param cardId
     * @return
     */
    boolean updateCardSign(Integer cardSign,Integer userId,Integer cardId);
    /**
     * 获取银行卡
     * @param no
     * @return
     */
    Card findCardByCardNo(String no);

    /**
     * 用户id与卡标记查卡信息
     * @param userId
     * @param cardSign
     * @return
     */
    List<Card> findCardByUserIdAndCardSign(Integer userId,Integer cardSign);
    /**
     * 获取银行卡ById
     * @param cardId
     * @return
     */
    Card findCardByCardId(Integer cardId);
    /**
     * 查询收款人银行卡
     * @param payId
     * @return
     */
    List<Card> findCardByPayId(Integer payId);

    /**
     * 获取用户银行卡
     * @param userId
     * @return
     */
    List<Card> findCardByUserId(Integer userId);

    /**
     * 银行卡转账
     * @param remitCard
     * @param receivCard
     * @param money
     * @return 1 转账成功；-1 卡1不存在；-2 卡2不存在；0 卡1密码错误；-3卡1余额不足 -4超过限额
     */
    boolean insedeTran(Card remitCard, Card receivCard, BigDecimal money);

    /**
     * 删除银行卡
     * @param cardId
     * @return
     */
    boolean deleteCardById(Integer cardId);

    /**
     * 根据卡号判断是否是行内银行卡
     * @param no
     * @return 1 行内卡 0 行外卡 -1 不存在卡
     */
    int isInsideCard(String no);
}
