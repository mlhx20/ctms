package com.szit.model.service;

import com.szit.model.bean.Limit;

import java.math.BigDecimal;

public interface LimitService {
    /**
     * 通过用户id查找额度
     * @param userId
     * @return
     */
    Limit getLimitByUserId( Integer userId);

}
