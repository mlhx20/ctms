package com.szit.model.dao;

import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao {
    List<User> queryAll();

    /**
     * 查询所有有卡的用户id
     * @return
     */
    List<Integer> queryUserId();

    /**
     * 通过用户名查询用户id和真实姓名
     * @param userName
     * @return
     */
    User queryUserByUserName(String userName);

    /**
     * 银行卡id查用户
     * @param cardId
     * @return
     */
    User queryUserByCardId(Integer cardId);
}
