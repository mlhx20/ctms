package com.szit.model.dao;

import com.szit.model.bean.Card;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CardDao {
    /**
     * 修改卡标记
     * @param cardSign
     * @param userId
     * @param cardId
     * @return
     */
    int updateCardSign(Integer cardSign,Integer userId,Integer cardId);
    /**
     * 通过卡号获取卡
     * @param no
     * @return
     */
    Card getCardByCardNo(String no);

    /**
     * 通过卡id获取卡
     * @param cardId
     * @return
     */
    Card getCardByCardId(Integer cardId);

    /**
     * 用户id与卡标记查卡信息
     * @param userId
     * @param cardSign
     * @return
     */
    List<Card> queryByUserIdAndCardSign(Integer userId,Integer cardSign);
    /**
     * 通过收款人查询银行卡
     * @param payId
     * @return
     */
    List<Card> getCardByCardPayId(Integer payId);

    /**
     * 查询用户的银行卡
     * @param userId
     * @return
     */
    List<Card> queryByUserId(@Param("userId") Integer userId);

    /**
     * 通过银行卡id查卡号
     * @param id
     * @return
     */
    Card queryByCardId(Integer id);

    /**
     *修改卡信息
     * @param card
     * @return
     */
    int updateCard(Card card);

    /**
     * 通过id删除卡号
     * @param cardId
     * @return
     */
    int deleteById(Integer cardId);

}
