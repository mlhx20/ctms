package com.szit.model.dao;

import com.szit.model.bean.Payee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PayeeDao {
    List<Payee> queryByUserId(Integer userId);
}
