package com.szit.model.dao;

import com.szit.model.bean.TransRecords;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TransRecordsDao {
    /**
     * 查询未完成的交易记录
     * @param userId
     * @param state
     * @return
     */
    List<TransRecords> queryTransRecordsByUserIdAndState(@Param("userId") Integer userId,@Param("state") Integer state);
    /**
     * 用户所有记录
     * @param userId
     * @return
     */
    List<TransRecords> queryByUserId(Integer userId);
    TransRecords queryByTransId(Integer transId);
    /**
     * 用户当天交易记录
     * @param userId
     * @return
     */
    List<TransRecords> queryNow(Integer userId);

    /**
     * 修改交易记录状态
     * @param state
     * @return
     */
    int updateTransRecordsState(@Param("state") Integer state,@Param("id") Integer id);

    /**
     * 插入交易记录
     * @param transRecords
     * @return
     */
    int insertTransRecords(TransRecords transRecords);
}
