package com.szit.util;

import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import com.szit.model.service.CardService;
import com.szit.model.service.TransRecordsService;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

//@Component注解用于对那些比较中立的类进行注释；
//相对与在持久层、业务层和控制层分别采用 @Repository、@Service 和 @Controller 对分层中的类进行注释
@Component
@EnableScheduling   // 1.开启定时任务
@EnableAsync        // 2.开启多线程
public class MultithreadScheduleTask {

    @Autowired
    private CardService cardService;
    @Autowired
    private UserService userService;
    @Autowired
    private TransRecordsService transRecordsService;
    @Async
   // @Scheduled(fixedDelay = 1000)  //间隔1秒
    //Cron表达式参数分别表示：
    //
    //秒（0~59） 例如0/5表示每5秒
    //分（0~59）
    //时（0~23）
    //日（0~31）的某天，需计算
    //月（0~11）
    //周几（ 可填1-7 或 SUN/MON/TUE/WED/THU/FRI/SAT）
    //@Scheduled：除了支持灵活的参数表达式cron之外，还支持简单的延时操作，例如 fixedDelay ，fixedRate 填写相应的毫秒数即可
   //"0 15 10 L * ?" 每月最后一日的上午10:15触发
    @Scheduled(cron = "0 */1 * * * ?")//每分钟
    public void first() throws InterruptedException {
        //查询有卡用户
        List<Integer> userIds=userService.findAllUserId();
        //归集最低资金
        BigDecimal lowMoney=new BigDecimal("1000");
        for(Integer userId:userIds){
            //查询用户主卡
            List<Card> cardList1=cardService.findCardByUserIdAndCardSign(userId,1);
            if(cardList1.size()!=0){
                //查询用户分卡
                List<Card> cardList2=cardService.findCardByUserIdAndCardSign(userId,2);
                if(cardList2!=null){
                    //进行资金归集
                    for(Card card:cardList2){
                        //判断是否行内
                        if(card.getBankid()==cardList1.get(0).getBankid()){
                            //compareTo -1 小于； 0 等于； 1 大于；>-1 大于等于；<1 小于等于
                            if(lowMoney.compareTo(card.getBalance())<1) {
                                //计算归集金额
                                BigDecimal amount = card.getBalance().subtract(lowMoney);
                                if (amount.compareTo(new BigDecimal("0")) ==1) {
                                    //归集一张卡的资金
                                    int state = 3;//失败
                                    if (cardService.insedeTran(card, cardList1.get(0), amount)) {
                                        state = 1;//成功
                                    }
                                    //添加交易记录
                                    TransRecords transRecords = new TransRecords();
                                    transRecords.setUserId(userId);
                                    transRecords.setRemitCardId(card.getId());
                                    transRecords.setReceivCardId(cardList1.get(0).getId());
                                    transRecords.setAmount(amount);
                                    transRecords.setState(state);
                                    transRecordsService.insertTransRecords(transRecords);
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println("定时任务 : " + LocalDateTime.now().toLocalTime() + "\r\n线程 : " + Thread.currentThread().getName());
       // Thread.sleep(1000 * 10);
    }

//    @Async
//    @Scheduled(fixedDelay = 2000)
//    public void second() {
//        System.out.println("第二个定时任务开始 : " + LocalDateTime.now().toLocalTime() + "\r\n线程 : " + Thread.currentThread().getName());
//        System.out.println();
//    }
}
