package com.szit.controller;

import com.szit.feign.PayeeFeignClient;
import com.szit.model.bean.Card;
import com.szit.model.bean.User;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private PayeeFeignClient payeeFeignClient;

    @RequestMapping(value = "/getusers",method = RequestMethod.GET)
    public List<User> getAllUser(){
        return userService.findAll();
    }
    @RequestMapping(value = "/getusersby",method = RequestMethod.GET)
    public List<User> getAllUserByPayee(){
        List<User> users = payeeFeignClient.getUsersByPayee();
        return users;
    }
     @GetMapping("/queryUserByUserName")
      public User queryUserByUserName(@RequestParam String userName){
        return userService.queryUserByUserName(userName);
    }

    @GetMapping("/queryUserByCardId")
    public User queryUserByCardId(@RequestParam Integer cardId){
        return userService.queryUserByCardId(cardId);
    }
}
