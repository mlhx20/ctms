package com.szit.controller;

import com.szit.model.bean.Limit;
import com.szit.model.bean.TransRecords;
import com.szit.model.service.CardService;
import com.szit.model.service.LimitService;
import com.szit.model.service.TransRecordsService;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/transRecords")
public class TransRecordsController {
    @Autowired
    private  TransRecordsService transRecordsService;
    @Autowired
    private LimitService limitService;
    @Autowired
    private CardService cardService;

    @RequestMapping(value = "/getTransRecords",method = RequestMethod.POST)
    public List<TransRecords> getTransRecords(@RequestParam Integer id){
        return transRecordsService.getTransRecordsByUserId(id);
    }
    @RequestMapping(value = "/setTransRecords",method = RequestMethod.POST)
    public boolean setTransRecords(@RequestBody  TransRecords transRecords){
        //插入交易记录状态
        return transRecordsService.insertTransRecords(transRecords);
    }

    @RequestMapping(value = "/selectTransRecords",method = RequestMethod.GET)
    public List<TransRecords> selectTransRecords(@RequestParam  Integer userId,@RequestParam Integer state){
        //插入交易记录状态
        return transRecordsService.queryTransRecordsByUserIdAndState(userId,state);
    }

    @RequestMapping(value = "/getTransRecordsByTransId",method = RequestMethod.GET)
    public TransRecords getTransRecordsByTransId(@RequestParam Integer transId){
        return transRecordsService.queryByTransId(transId);
    }
    @RequestMapping(value = "/updateTransRecordsState",method = RequestMethod.GET)
    public boolean updateTransRecordsState(@RequestParam Integer transId,@RequestParam Integer state){
        return transRecordsService.updateTransRecordsState(state,transId);
    }

    @RequestMapping(value = "/insertrecords",method = RequestMethod.POST)
    public boolean insertTransRecords(@RequestBody TransRecords transRecords){
        boolean res = transRecordsService.insertTransRecords(transRecords);
        return res;
    }

    @RequestMapping(value = "/insideTrans",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public int insideTrans(@RequestBody  TransRecords transRecords) {
//        ResultInfo resultInfo = new ResultInfo();
//        resultInfo.setMessage("交易失败！");
//        resultInfo.setIndex(0);
        int flag=0;
        //用户限额
        Limit limit = limitService.getLimitByUserId(transRecords.getUserId());
        //当天已交易金额
        BigDecimal amount = transRecordsService.getDayAmount(transRecords.getUserId());
        try {
            //判断是否行内
            if(transRecords.getRemitCard().getBankid()==transRecords.getReceivCard().getBankid()) {
                //判断当天限额
                if (limit.getDaylmt().compareTo(amount.add(transRecords.getAmount())) > -1) { //compareTo -1 小于； 0 等于； 1 大于；>-1 大于等于；<1 小于等于
                    //判断单笔限额
                    if (limit.getTranslimit().compareTo(transRecords.getAmount()) > -1) {
                        //判断余额
                        if (transRecords.getRemitCard().getBalance().compareTo(transRecords.getAmount().add(new BigDecimal("10"))) > -1) {
                            //转账
                            if (cardService.insedeTran(transRecords.getRemitCard(), transRecords.getReceivCard(), transRecords.getAmount())) {
                             //  resultInfo.setMessage("转账成功！");
                                flag=1;
                                //插入交易记录状态
                                if(transRecords.getState()==1) {
                                    transRecordsService.insertTransRecords(transRecords);
                                }else {
                                    transRecordsService.updateTransRecordsState(1,transRecords.getId());
                                }
                            } else {
                                flag=2;
                               // resultInfo.setMessage("系统异常，转账失败！！");
                            }
                        } else {
                            flag=3;
                          //  resultInfo.setMessage("余额不足！");
                        }
                    } else {
                        flag=4;
                       // resultInfo.setMessage("单笔转账金额超过当前限额！");
                    }

                } else {
                    flag=5;
                   // resultInfo.setMessage("当天转账金额超过当前限额！");
                }
            }else {
                flag=6;
               // resultInfo.setMessage("非行内转账！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return flag;
      //  return ResultUtil.createSubmitResult(resultInfo);
    }

}
