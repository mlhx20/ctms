package com.szit.controller;

import com.szit.model.bean.Card;
import com.szit.model.bean.Payee;
import com.szit.model.bean.TransRecords;
import com.szit.model.service.CardService;
import com.szit.model.service.PayeeService;
import com.szit.model.service.TransRecordsService;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.SubmitResultInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/payee")
public class PayeeController {

    @Autowired
    private PayeeService payeeService;
    @Autowired
    private TransRecordsService transRecordsService;
    @Autowired
    private CardService cardService;
    @RequestMapping(value = "/getpayees/{userId}",method = RequestMethod.GET)
    public List<Payee> getPayees(@PathVariable("userId") Integer userId){
        return payeeService.getPayeeByUserId(userId);

    }

    @RequestMapping(value = "/getpayee",method = RequestMethod.GET)
    public List<Payee> getPayee(@RequestParam Integer userId){
        List<Payee> payees=payeeService.getPayeeByUserId(userId);
        return  payees;
    }
}
