package com.szit.controller;

import com.szit.feign.PayeeFeignClient;
import com.szit.model.bean.Card;
import com.szit.model.bean.Limit;
import com.szit.model.bean.ResultObj;
import com.szit.model.bean.TransRecords;
import com.szit.model.service.CardService;
import com.szit.model.service.LimitService;
import com.szit.model.service.TransRecordsService;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/card")
public class CardController {
    @Autowired
    private CardService cardService;
    @Autowired
    private LimitService limitService;
    @Autowired
    private TransRecordsService transRecordsService;
    @Autowired
    private PayeeFeignClient payeeFeignClient;

    /**
     * 银行卡号查银行卡
     * @param cardNo
     * @return
     */
    @GetMapping("/getCardByCardNo")
    public  Card getCardByCardNo(@RequestParam String cardNo){
        return cardService.findCardByCardNo(cardNo);
    }

    @GetMapping("/updateCardSign")
    public  boolean updateCardSign(@RequestParam Integer cardSign,@RequestParam Integer userId,Integer cardId){
        return cardService.updateCardSign(cardSign,userId,cardId);
    }

    @RequestMapping(value = "/getCardByUserIdAndCardSign",method = RequestMethod.GET)
    public  List<Card> getCardByUserIdAndCardSign(@RequestParam Integer userId,@RequestParam Integer cardSign){
        return cardService.findCardByUserIdAndCardSign(userId,cardSign);
    }
    /**
     * 收款人id查银行卡
     * @param payeeId
     * @return
     */
    @RequestMapping(value = "/getCardByPayId",method = RequestMethod.GET)
    public  List<Card> getCardByPayId(@RequestParam Integer payeeId){
        List<Card> cards=cardService.findCardByPayId(payeeId);
        return cards;
    }
    /**
     * 用户id查银行卡
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getCardsByUserId",method = RequestMethod.GET)
    public  List<Card> getCardsByUserId(@RequestParam Integer userId){
        return cardService.findCardByUserId(userId);
    }

    @GetMapping("/getCardByUserId/{userId}")
    public DataTableJson getCardByUserId(@PathVariable Integer userId){
        DataTableJson dataTableJson=new DataTableJson();
        String msg = "获取Card信息！";
        List<Card> cards=cardService.findCardByUserId(userId);
        dataTableJson.setData(cards);
        dataTableJson.setMsg(msg);
        dataTableJson.setCount(cards.size());
        return dataTableJson;
    }
    @GetMapping("/getCard/{userId}")
    public List<String> getCard(@PathVariable Integer userId){
        DataTableJson dataTableJson=new DataTableJson();
        List<Card> cards=cardService.findCardByUserId(userId);
        List<String> list=new ArrayList<String>();
        for (Card c:cards) {
            list.add(c.getNo());
        }
        return list;
    }

    @RequestMapping(value = "/sweep",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public SubmitResultInfo sweep(@RequestBody Card reciveCard,@RequestBody List<Card> remitCards,@RequestBody BigDecimal money){
        ResultInfo resultInfo = new ResultInfo();
        String msg = "交易失败！";
        try {
            for(Card card:remitCards) {
                //判断余额
                if (card.getBalance().compareTo(money.add(new BigDecimal("10"))) > -1) {
                    //转账
                    if (cardService.insedeTran(card,reciveCard, money)) {
                        msg = "归集成功！";
                    }
                } else {
                    msg = "有卡余额不足";
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        resultInfo.setMessage(msg);
        return ResultUtil.createSubmitResult(resultInfo);
    }

}
