package com.szit.tool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charles.
 */
public class PageIndexer implements Serializable {

    private static final long serialVersionUID = 153137774455553139L;

    // 总记录数
    private long totalCount;
    // 当前页号
    private int pageNo;
    // 总页数
    private int totalPageCount;
    // 页大小
    private int pageSize;
    // 显示列表记录
    private List items;

    public PageIndexer(long totalCount, int pageNo, List items, int pageSize) {
        this.totalCount = totalCount;
        this.items = items == null ? new ArrayList() : items;
        this.pageSize = pageSize;
        if (totalCount != 0) {
            int temp = (int) (totalCount / pageSize);
            this.totalPageCount = (totalCount % pageSize == 0) ? temp : (temp + 1);// 显示到下一页
            this.pageNo = pageNo;
        } else {
            this.pageNo = 0;
        }
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public int getPageSize() {
        if (pageSize < 1)
            pageSize = 20;
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

}
