package com.szit.tool;

/**
 * 系统参数配置类，配置了系统用到参数
 */
public class Config {

	/**
	 * 超级管理员名称
	 */
	public static final String SYSTEM_NAME = "中国转账管理系统";
	/**
	 * 超级管理员名称
	 */
	public static final String ADMIN_NAME = "超级管理员";
	/**
	 * 系统语言环境，默认为中文zh
	 */
	public static final String LANGUAGE = "zh";

	/**
	 * 系统国家环境，默认为中国CN
	 */
	public static final String COUNTRY = "CN";

	/**
	 * servletContext
	 */
	public static final String SERVLETCONTEXT_KEY = "servletContext";

	/**
	 * 提示信息配置文件名
	 */
	public static final String MESSAGEFILE = "message_CH_zh";

	/**
	 * 系统版本文件名称
	 */
	public static final String VERSION = "version";

	/**
	 * 公共权限，需要给用户分配权限即可使用
	 */
	public static final String COMMON_ACTIONS = "commonActions";

	/**
	 * 公开权限，用户无需登录即可使用
	 */
	public static final String ANONYMOUS_ACTIONS = "anonymousActions";

	/**
	 * 系统参数配置文件名称
	 */
	public static final String SYSCONFIG = "sysParams";

	/**
	 * session中存放登录用户的key名称
	 */
	public static final String ACTIVEUSER_KEY = "activeUser";

	/**
	 * 配置文件中系统基础url的key名称
	 */
	public static final String BASEURL_KEY = "ctx";

	/**
	 * 配置文件中加密密钥的key名称
	 */
	public static final String DESKEY_KEY = "deskey";

	/**
	 * 配置文件中系统版本名称的key名称
	 */
	public static final String VERSION_NAME_KEY = "version_name";

	/**
	 * 配置文件中系统版本号的key名称
	 */
	public static final String VERSION_NUMBER_KEY = "version_number";

	/**
	 * 配置文件中系统版本发布时间的key名称
	 */
	public static final String VERSION_DATE_KEY = "version_date";

	/**
	 * 系统提示信息ResultInfo对象key
	 */
	public static final String RESULTINFO_KEY = "resultInfo";

	/**
	 * 一般错误提示页面,该路径需要匹配页面前后缀
	 */
	public static final String ERROR_PAGE = "/home/err";
	/**
	 * 登录页面地址,该路径需要匹配页面前后缀
	 */
	public static final String LOGIN_PAGE = "/home/login";

	/**
	 * 无权提示页面地址,该路径需要匹配页面前后缀
	 */
	public static final String REFUSE_PAGE = "/home/refuse";

	/**
	 * 系统公开action，无需登录即可使用
	 */
	// private static List<Operation> anonymousActions;

	/**
	 * 获取公开访问地址
	 * 
	 * @return
	 */
	/*
	 * public static List<Operation> getAnonymousActions() { if(anonymousActions
	 * !=null){ return anonymousActions; } anonymousActions = new
	 * ArrayList<Operation>();
	 * 
	 * List<String> baseActions_list =
	 * ResourcesUtil.gekeyList(Config.ANONYMOUS_ACTIONS); for (String
	 * common_actionUrl:baseActions_list) { Operation o_i = new
	 * Operation(common_actionUrl); anonymousActions.add(o_i); } return
	 * anonymousActions; }
	 */

}
