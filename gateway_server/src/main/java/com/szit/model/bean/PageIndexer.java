package com.szit.model.bean;

import java.io.Serializable;

/**
 * 分页实体类
 *
 * @author Derek
 * @version 1.0
 */
public class PageIndexer implements Serializable {
    private Integer pageIndex;  //页码
    private Integer pageSize;   //每页行数
    private Integer pageCount;  //总页数
    private Integer count;  //分页记录总数


    public PageIndexer() {

    }

    public PageIndexer(Integer pageIndex, Integer pageSize, Integer pageCount, Integer count) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
        this.count = count;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
