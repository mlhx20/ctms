package com.szit.model.dao;

import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao {
    List<User> queryAll();
}
