package com.szit.controller;

import com.alibaba.fastjson.JSONObject;
import com.szit.utils.JwtConstant;
import com.szit.utils.JwtFactory;
import com.szit.utils.TokenCacheUtil;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

@Configuration
public class IsLoginGateway implements GlobalFilter,Ordered {

    @Autowired
    private TokenCacheUtil tokenCacheUtil;

    @Override
    public int getOrder() {
        return 1;
    }

    //拉取token过期时间
    @Value("${token.expire:43200000}")
    @RefreshScope
    public void setExpire(Long expire) {
        if (expire != null) {
            JwtConstant.JWT_REFRESH_TTL = Long.valueOf(expire);
        }
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        String userId = getLoginUserId(exchange);
        if (request.getMethod().name().equalsIgnoreCase("OPTIONS")) {
            JSONObject message = new JSONObject();
            message.put("statusCode", -1);
            message.put("msg", "请求类型错误!!!");
            byte[] bits = message.toJSONString().getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = response.bufferFactory().wrap(bits);
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            //指定编码，否则在浏览器中会中文乱码
            response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
            return response.writeWith(Mono.just(buffer));
        }
        if (StringUtils.isNotBlank(userId)) {
            exchange.getResponse().getHeaders().set("isLogin", userId);
        } else {
            exchange.getResponse().getHeaders().set("isLogin", "-1");
            JSONObject message = new JSONObject();
            message.put("statusCode", -1);
            message.put("msg", "鉴权失败");
            byte[] bits = message.toJSONString().getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = response.bufferFactory().wrap(bits);
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            //指定编码，否则在浏览器中会中文乱码
            response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
            return response.writeWith(Mono.just(buffer));
        }
        return chain.filter(exchange);
    }

    private String getLoginUserId(ServerWebExchange exchange) {
        String userId = null;
        String auth =exchange.getRequest().getHeaders().getFirst("Authorization");

        if ((auth != null) && (auth.length() > 7)) {
            Claims claims = JwtFactory.parseJWT(auth);
            if (claims != null) {
                userId = claims.get("userid") == null ? "" : claims.get("userid").toString();
                if (!tokenCacheUtil.checkTokenExists(auth, Integer.valueOf(userId))) {
                    return null;
                }
                exchange.getAttributes().put("userid",userId);
            }
        }
        return userId;
    }

}
