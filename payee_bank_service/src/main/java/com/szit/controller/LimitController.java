package com.szit.controller;

import com.szit.model.bean.Limit;
import com.szit.model.bean.User;
import com.szit.model.service.LimitService;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/limit")
@RefreshScope //开启动态刷新
public class LimitController {
    @Autowired
    private LimitService limitService;
    @Autowired
    private RedisTemplate redisTemplate;


//
//    @RequestMapping(value = "/find/{userId}" )
//    @ResponseBody
//    public Limit findLimit1(@PathVariable("userId")Integer userId){
//        //查询出交易限额
//        Limit limit=limitService.findOne(userId);
//        redisTemplate.opsForValue().set("limit",limit);
//        return limit;
//    }

    /**
     * 当前登录用户查询自己的交易限额
     * @param
     * @return
     */
    @RequestMapping(value = "/findLimit",method = RequestMethod.GET)
    public Limit findLimit(){
        //查询出交易限额
        DataTableJson dataTableJson = new DataTableJson();
        //userId =1;

        User user=(User) redisTemplate.opsForValue().get("user");
        Limit limit=limitService.findOne(user.getId());
        redisTemplate.opsForValue().set("limit",limit);
        List<Limit> limits=new ArrayList<>();
        limits.add(limit);
        dataTableJson.setData(limits);
        return limit;
    }

}
