package com.szit.controller;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.model.service.TransRecordsService;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/transRecords")
public class TransRecordsController {
    @Autowired
    private TransRecordsService transRecordsService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 点击某一条交易记录查看详情
     * @param id 交易记录id
     * @return 一条交易记录详情
     */
   @RequestMapping(value ="/tansdetailed/{id}" )
    public DataTableJson transDetail(@PathVariable("id")Integer id){
       DataTableJson dataTableJson=new DataTableJson();
       List<TransRecords> transRecords=new ArrayList<>();
       transRecords.add(transRecordsService.getTransRecordsById(id));
       dataTableJson.setData(transRecords);
       return dataTableJson;
   }
    /**
     *用户删除某一条交易记录   伪删除
     * 状态1 未删除 状态0 删除
     * @param
     * @return 伪删除成功与否
     */
    @RequestMapping(value = "/transdel")
    public Boolean transDel(@RequestParam Integer id){
       boolean flag=transRecordsService.deleteTrans(id);
       return flag;
    }

    /**
     * 添加交易记录
     * @param transRecords
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Boolean transAdd(@RequestBody TransRecords transRecords){
        return transRecordsService.addTransRecords(transRecords);
    }

    /**
     * 用户列出所有交易记录(删除状态为1的)
     * @return
     *  *
     *  @RequestParam(required = false) String name
     *
     */
    @RequestMapping(value = "/translist", method = RequestMethod.GET)
    public List<TransRecords> transList(@RequestParam Integer page,
                                   @RequestParam Integer limit){
        DataTableJson dataTableJson=new DataTableJson();
        //接受页面前端框架分页传过来的参数
//        Integer userId=1;
        User user= (User) redisTemplate.opsForValue().get("user");
        PageIndexer pag=new PageIndexer();
        pag.setPageIndex(page);
        pag.setPageSize(limit);
        List<TransRecords> lists=transRecordsService.listByPage(user.getId(),pag);
        redisTemplate.opsForValue().set("list",lists);
        dataTableJson.setData(lists);
        return lists;
    }

}
