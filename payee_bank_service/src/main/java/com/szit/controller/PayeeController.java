package com.szit.controller;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.Payee;
import com.szit.model.bean.User;
import com.szit.model.service.PayeeService;
import com.szit.model.service.UserPayeeService;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/payee")
@RefreshScope //开启动态刷新
public class PayeeController {
    @Autowired
    private PayeeService payeeService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private UserPayeeService userPayeeService;




    /**
     * 访问添加页面
     *
     * @return
     */
    @RequestMapping(value = "/payeeadd",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public boolean addUser(@RequestBody Payee payee) {
        User user=(User) redisTemplate.opsForValue().get("user");
        Integer newId = payeeService.queryMaxId()+1;
        payee.setId(newId);
        boolean flag = payeeService.add(payee);
        boolean flag1=false;
        //如果收款人添加成功 进行中间表添加操作
        if(flag){
            flag1 = userPayeeService.isAdd(user.getId(),newId);
            if(!flag1){
                throw new RuntimeException();
            }
//            flag1=true;
        }
        //如果成功
        return flag1;
    }

    /**
     * 访问收款人管理页面
     *列出所有收款人
     * @return
     */
    @RequestMapping(value = "/payeelist", method = RequestMethod.GET)
    public List<Payee> listByPage(@RequestParam Integer page,
                                  @RequestParam Integer limit,
                                  @RequestParam(required = false) String name) {
        DataTableJson dataTableJson = new DataTableJson();
        //存到集合
//        Integer userId=1;//测试数据

        User user= (User) redisTemplate.opsForValue().get("user");
       // String name= (String) request.getAttribute("name");
        PageIndexer pag=new PageIndexer();
        pag.setPageIndex(page);
        pag.setPageSize(limit);
//        System.out.println(name);
        List<Payee> lists=payeeService.listByPage(user.getId(),pag,name);
        // 保存状态
        redisTemplate.opsForValue().set("lists", lists);
        dataTableJson.setData(lists);
        dataTableJson.setCount(lists.size());
//        System.out.println("ctrl list: "+lists);
        return lists;
    }
//    @RequestMapping("/bb")
//    public String b(){
//        String a="";
//        String flag= restTemplate.getForObject("http://localhost:8084/payee/sao",String.class);
//        if(flag.equals("I found you!")){
//          a="信息洒下";
//        }
//        return a;
//    }

    /**
     * 根据id查看收款人详细信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/payeedetailed/{id}")
    public DataTableJson findOne(@PathVariable("id") Integer id){
        DataTableJson dataTableJson = new DataTableJson();
        Payee payee=payeeService.selectOne(id);
        redisTemplate.opsForValue().set("payee",payee);
        List<Payee> pay=new ArrayList<Payee>();
        pay.add(payee);
        dataTableJson.setCount(pay.size());
        dataTableJson.setData(pay);
        return dataTableJson;
    }




    /**
     * 修改收款人
     * @param payee
     * @return
     */
    @RequestMapping(value = "/payeeupdate")
    public Boolean updatePayee(@RequestBody Payee payee){
        boolean flag=payeeService.update(payee);
        return flag;
    }

    /**
     * 根据收款人id删除收款人信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/payeedel")
    public Boolean delPayee(@RequestParam Integer id){
        boolean flag=userPayeeService.isDel(id),
        flag1=false;
        if(flag){
            flag1=payeeService.delete(id);
        }
        return flag1;
    }

//    /**
//     * 模糊查询
//     * @param name
//     * @return
//     */
//    @RequestMapping(value = "/find",method = RequestMethod.POST)
//    public Payee findByname(String name){
//        DataTableJson dataTableJson=new DataTableJson();
//        List <Payee> lists=new ArrayList<>();
//        Payee p=payeeService.findPayeeByName(name);
//        lists.add(p);
//        dataTableJson.setCount(lists.size());
//        dataTableJson.setData(lists);
//        return p;
//    }




}
