package com.szit.controller;

import com.szit.feign.InsideFeignClient;
import com.szit.model.bean.User;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@RefreshScope //开启动态刷新
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired(required = false)
    private InsideFeignClient insideFeignClient;

    @RequestMapping(value = "/getusers",method = RequestMethod.GET)
    public List<User> getAllUser(){
        List<User> list = userService.findAll();
        RedisTemplate redisTemplate=new RedisTemplate();
        redisTemplate.opsForValue().set("list",list);
        System.out.println(list.get(0).getName());
        return list;
    }
    @RequestMapping(value = "/getusersby",method = RequestMethod.GET)
    public List<User> getAllUserByPayee(){
        List<User> users = insideFeignClient.getUsersByPayee();
        return users;
    }
}
