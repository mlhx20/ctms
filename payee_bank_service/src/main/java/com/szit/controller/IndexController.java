package com.szit.controller;

import com.szit.model.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description: 前端控制器
 * @author: zc
 * @create: 2019-06-14 10:36
 */
@Controller
@RequestMapping("/web")
public class IndexController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/index")
    public String index() {
        return "index.html";
    }
    @RequestMapping(value = "/limit")
    public String limit(){
        return "limit.html";
    }
    @RequestMapping(value = "/payeelist")
    public String payeeList(){
        User user=new User();
        user.setId(242);
        redisTemplate.opsForValue().set("user",user);
        return "payeelist.html";
    }
    @RequestMapping(value = "/translist")
    public String transList(){
        User user=new User();
        user.setId(242);
        redisTemplate.opsForValue().set("user",user);
        return "translist.html";
    }

}
