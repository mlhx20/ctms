package com.szit.service.impl;

import com.szit.model.dao.UserPayeeDao;
import com.szit.model.service.UserPayeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userPayeeService")
public class UserPayeeServiceImpl implements UserPayeeService {
    @Autowired(required = false)
    private UserPayeeDao userPayeeDao;


    @Override
    public Boolean isAdd(Integer userId, Integer payeeId) {
        boolean flag=false;
        try{
            int a=userPayeeDao.insertData(userId,payeeId);
            if(a>0){
                flag=true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public Integer getCounts(Integer userId) {
        return userPayeeDao.getCountsByPage(userId);
    }

    @Override
    public boolean isDel(Integer payeeId) {
        boolean flag=false;
        try{
            int a=userPayeeDao.delData(payeeId);
            if(a>0){
                flag=true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }
}
