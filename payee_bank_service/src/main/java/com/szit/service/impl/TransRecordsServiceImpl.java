package com.szit.service.impl;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;
import com.szit.model.dao.TransRecordsDao;
import com.szit.model.service.TransRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TransRecordsServiceImpl implements TransRecordsService {
    @Autowired(required = false)
    public TransRecordsDao transRecordsDao;
    @Override
    public List<TransRecords> listByPage(Integer userId, PageIndexer page) {
        //分页查询
        List <TransRecords> lists=transRecordsDao.listByPage(userId,(page.getPageIndex()-1)*
                page.getPageSize(),page.getPageSize());
        // 计算总页数
        int count = transRecordsDao.getCountsByPage(userId);

        page.setPageCount((int) Math.ceil(count * 1.0 / /*page.getPageSize()*/10));
        page.setCount(count);
        return lists;
    }

    /**
     * 根据交易记录id查询详情
     * @param id 交易记录id
     * @return
     */
    @Override
    public TransRecords getTransRecordsById(Integer id) {
        return transRecordsDao.getTransRecordsById(id);
    }

    /**
     * 伪删除交易记录
     * @param id
     * @return
     */
    @Override
    public boolean deleteTrans(Integer id) {
        boolean isdel=true;
        try{
            transRecordsDao.deleteTrans(id);
        } catch(Exception e){
            e.printStackTrace();
            isdel=false;
        }
        return isdel;
    }

    /**
     * 查询记录总数
     * @param userId
     * @return
     */
    @Override
    public int getCountsByPage(Integer userId) {
        return transRecordsDao.getCountsByPage(userId);
    }

    /**
     * 添加交易记录
     * @param transRecords
     * @return
     */
    @Override
    public boolean addTransRecords(TransRecords transRecords) {
        boolean isadd=true;
        try{
            transRecordsDao.insertTrans(transRecords);
        } catch(Exception e){
            e.printStackTrace();
            isadd=true;
        }
        return isadd;
    }
}
