package com.szit.service.impl;

import com.szit.model.bean.User;
import com.szit.model.dao.UserDao;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserDao userDao;
    @Override
    public List<User> findAll() {
        return userDao.queryAll();
    }

}
