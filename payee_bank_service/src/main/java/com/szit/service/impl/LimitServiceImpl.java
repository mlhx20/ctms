package com.szit.service.impl;

import com.szit.model.bean.Limit;
import com.szit.model.dao.LimitDao;
import com.szit.model.service.LimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LimitServiceImpl implements LimitService {

    @Autowired(required = false)
    private LimitDao limitDao;

    /**
     * 查询当前用户交易限额
     * @param userId
     * @return
     */
    @Override
    public Limit findOne(Integer userId) {
        return limitDao.selectLimitById(userId);
    }
}
