package com.szit.service.impl;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.Payee;
import com.szit.model.dao.PayeeDao;
import com.szit.model.dao.UserPayeeDao;
import com.szit.model.service.PayeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PayeeServiceImpl implements PayeeService {
    @Autowired(required = false)
    private PayeeDao payeeDao;
    @Autowired(required = false)
    private UserPayeeDao userPayeeDao;

    /**
     * 查询所有收款人
     * @return
     */
    @Override
    public List<Payee> findAll() {
        return payeeDao.queryAll();
    }

    /**
     * 添加收款人
     * @param payee
     * @return
     */
    @Override
    public boolean add(Payee payee) {
     boolean isAdd=false;
     try {
         int a=payeeDao.insertPayee(payee);
         if (a>0){
             isAdd=true;
         }
     }catch (Exception e){
         e.printStackTrace();
     }
        return isAdd;
    }

    /**
     * 删除收款人
     * @param id
     * @return
     */
    @Override
    public boolean delete(Integer id) {
        boolean isDelete=true;
        try {
            payeeDao.deletePayeeById(id);
        }catch (Exception e){
            e.printStackTrace();
            isDelete=false;
        }
        return isDelete;
    }

    /**
     * 修改收款人
     * @param payee
     * @return
     */
    @Override
    public boolean update(Payee payee) {
        boolean isUpdate=true;
        try {
            payeeDao.updatePayeeById(payee);
        }catch (Exception e){
            e.printStackTrace();
            isUpdate=false;
        }
        return isUpdate;
    }

    /**
     * 根据id 查询收款人详细信息
     * @param id
     * @return
     */
    @Override
    public Payee selectOne(Integer id) {
        return payeeDao.selectPayeeById(id);
    }

    /**
     * 分页列出所有收款人
     * @param userId
     * @return
     */
    @Override
    public List<Payee> listByPage(Integer userId, PageIndexer page,String name) {
        //分页查询
        List <Payee> lists=payeeDao.findPayeeByPage(userId,(page.getPageIndex() - 1) * page.getPageSize(), page.getPageSize(),name);
        // 计算总页数
        int counts=userPayeeDao.getCountsByPage(userId);
        page.setPageCount((int) Math.ceil(counts * 1.0 / /*page.getPageSize()*/10));
        page.setCount(counts);
        return lists;
    }

//    @Override
//    public Payee findPayeeByName(String name) {
//        return payeeDao.selectPayeeByName(name);
//    }

    @Override
    public Integer queryMaxId() {
        return payeeDao.queryMaxId();
    }
}
