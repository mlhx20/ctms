package com.szit.tool.result;

/**
 * @Author fushane-asura
 * @Description:
 * @Date: Created in 5:03 2018/4/10
 * @Modified By:
 */
public class PicUploadResult {

	private Integer error;
	private String url;
	private String width;
	private String height;

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeigth() {
		return height;
	}

	public void setHeigth(String heigth) {
		this.height = heigth;
	}

}
