package com.szit.tool.result;

import java.util.List;

/**
 * @Author fushane-asura
 * @Description:
 * @Date: Created in 12:51 2018/6/22
 * @Modified By:
 */
public class PostResult<T> {
	private Integer code; // 状态码 0找到且资格证没到期 1找到资格证临近到期 2未找到
	private String msg; // 消息
	private List<T> data; // 数据

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
