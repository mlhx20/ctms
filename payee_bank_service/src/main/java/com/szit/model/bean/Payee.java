package com.szit.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 收款人实体类
 */
public class Payee implements Serializable {
    private Integer id;
    private String name;
    private String code;
    private String phone;
    private BigDecimal money;
    private String country;
    private String address;
    private UserPayee userPayee;

    private String bankCard;
    private String bank;

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }


    public Payee() {
    }

    public Payee(Integer id, String name, String code, String phone, BigDecimal money, String country, String address, UserPayee userPayee) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.phone = phone;
        this.money = money;
        this.country = country;
        this.address = address;
        this.userPayee = userPayee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserPayee getUserPayee() {
        return userPayee;
    }

    public void setUserPayee(UserPayee userPayee) {
        this.userPayee = userPayee;
    }
}