package com.szit.model.dao;

import com.szit.model.bean.TransRecords;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TransRecordsDao {
    /**
     * 分页列出所有交易记录信息
     * @param from
     * @param userId
     * @param pageSize
     * @return
     */
    List<TransRecords> listByPage(@Param("userId") Integer userId,@Param("from")Integer from,
                                  @Param("pageSize")Integer pageSize);

    /**
     * 点击某一条交易记录查看详情
     * @param id 交易记录id
     * @return
     */
    TransRecords getTransRecordsById(@Param("id")Integer id);

    /**
     *用户删除某一条交易记录   伪删除
     * 状态1 未删除 状态0 删除
     * @param id
     * @return
     */
    int deleteTrans(@Param("id") Integer id);

    /**
     * 根据当前登陆用户查询出的所有交易记录总数，便于分页
     * @param userId
     * @return
     */
    int getCountsByPage(@Param("userId") Integer userId);

    /**
     * 添加交易记录
     * @param transRecords
     * @return
     */
    int insertTrans(TransRecords transRecords);



}
