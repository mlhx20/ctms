package com.szit.model.dao;

import com.szit.model.bean.Payee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PayeeDao {
    /**
     * 查询所有收款人
     * @return
     */
    List<Payee> queryAll();


    /**
     * 查询并分页列出收款人
     * @return
     */
    List<Payee> findPayeeByPage(@Param("userId")Integer userId,
                                @Param("from") Integer from,
                                @Param("pageSize") Integer pageSize,
                                @Param("name") String name);








    /**
     * 添加收款人信息
     * @param payee
     * @return
     */
    int insertPayee(Payee payee);

    /**
     * 根据收款人id删除收款人信息
     * @param id
     * @return
     */
    int deletePayeeById(@Param("id") Integer id);

    /**
     * 修改收款人信息
     * @param payee
     * @return 成功与否
     */
    int updatePayeeById(Payee payee);

    Integer queryMaxId();



    /**
     * 根据id查看收款人详细信息
     * @return payee
     */
    Payee selectPayeeById(@Param("id") Integer id);
//
//    /**
//     * 根据姓名模糊查询收款人
//     * @param name
//     * @return
//     */
//    List<Payee> selectPayeeByName(@Param("name")String name);


}
