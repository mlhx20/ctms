package com.szit.model.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserPayeeDao {
    /**
     * 插中间表
     * @param userId
     * @param payeeId
     * @return
     */
    int  insertData(@Param("userId") Integer userId, @Param("payeeId")Integer payeeId);
    /**
     * 查询总记录数
     * @return
     */
    int getCountsByPage(@Param("userId") Integer userId);
    /**
     * 删中间表
     * @param payeeId
     * @return
     */
    int  delData(@Param("payeeId")Integer payeeId);
}
