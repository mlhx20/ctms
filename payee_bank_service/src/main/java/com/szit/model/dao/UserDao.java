package com.szit.model.dao;

import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao {
    List<User> queryAll();
    /**
     * 通过userCode查询获取User
     *
     * @param userCode
     * @return 返回用户实体对象
     */
    User getUserByUserCode(@Param("userCode")String userCode);

}
