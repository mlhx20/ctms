package com.szit.model.dao;

import com.szit.model.bean.Limit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LimitDao {
    /**
     * 查询当前用户的交易限额
     * @param userId
     * @return
     */
    Limit selectLimitById(@Param("userId") Integer userId);
}
