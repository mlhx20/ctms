package com.szit.model.service;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.Payee;

import java.util.List;

/**
 * @author Administrator
 */
public interface PayeeService {
    /**
     * 查询所有收款人
     * @return
     */
    List <Payee> findAll();

    /**
     * 添加收款人信息
     * @param payee
     * @return
     */
    boolean add(Payee payee);

    /**
     * 根据收款人id删除收款人信息
     * @param id
     * @return
     */
    boolean delete(Integer id);

    /**
     * 修改收款人信息
     * @param payee
     * @return
     */
    boolean update(Payee payee);

    /**
     * 根据id查看收款人详细信息
     * @return payee
     */
    Payee selectOne(Integer id);


    /**
     * 分页列出所有收款人
     * @param userId
     * @return
     */
    List<Payee> listByPage(Integer userId, PageIndexer page,String name);

//    /**
//     * 根据姓名模糊查询收款人
//     * @param name
//     * @return
//     */
//    Payee findPayeeByName(String name);

    Integer queryMaxId();


}
