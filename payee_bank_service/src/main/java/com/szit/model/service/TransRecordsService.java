package com.szit.model.service;

import com.szit.model.bean.PageIndexer;
import com.szit.model.bean.TransRecords;

import java.util.List;

public interface TransRecordsService {
    /**
     * 分页交易记录
     * @param userId
     * @param page
     * @return
     */
    List<TransRecords> listByPage(Integer userId,PageIndexer page);


    /**
     * 点击某一条交易记录查看详情
     * @param id 交易记录id
     * @return
     */
    TransRecords getTransRecordsById(Integer id);

    /**
     *用户删除某一条交易记录   伪删除
     * 状态1 未删除 状态0 删除
     * @param id
     * @return
     */
    boolean deleteTrans(Integer id);

    /**
     * 根据当前登陆用户查询出的所有交易记录总数，便于分页
     * @param userId
     * @return
     */
    int getCountsByPage(Integer userId);

    /**
     * 添加交易记录
     * @param transRecords
     * @return
     */
    boolean addTransRecords(TransRecords transRecords);
}
