package com.szit.model.service;

public interface UserPayeeService {

    Boolean isAdd(Integer userId,Integer payeeId);

    Integer getCounts(Integer userId);
    boolean isDel(Integer payeeId);
}
