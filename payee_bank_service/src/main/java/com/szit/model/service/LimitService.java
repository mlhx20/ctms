package com.szit.model.service;

import com.szit.model.bean.Limit;

public interface LimitService {
    /**
     * 查询当前登录用户的交易限额
     * @param userId
     * @return
     */
    Limit findOne(Integer userId);
}
