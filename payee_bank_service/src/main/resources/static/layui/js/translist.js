layui.use(['layer', 'form', 'element', 'laypage', 'upload', 'tree', 'carousel', 'flow', 'table'], function () {
    var layer = layui.layer
        , form = layui.form
        , element = layui.element
        , $ = layui.jquery
        , laypage = layui.laypage
        , upload = layui.upload
        , tree = layui.tree
        , carousel = layui.carousel
        , table = layui.table
        , flow = layui.flow;
    //监听提交
    form.on('submit(searchSubmit)', function (data) {
        // TODO 校验
        //formSubmit(data);
        return false;
    });


    /*显示所有交易记录信息*/
    var tableIns = table.render({
        elem: '#translist'
        , url: '/transRecords/translist'
        // ,dataType: 'json'
        , toolbar: true //开启头部工具栏，并为其绑定左侧模板
        , title: '交易记录表'
        , page: true
        , parseData: function (res) {
            console.log(res);
            return {
                "code": 0,
                "msg": "",
                "count": 1000,
                data: res
            }
        }
        , cols:
            [[
                {
                    field: 'time', title: '交易日期', align: 'center',
                    templet: "<div>{{layui.util.toDateString(d.ordertime, 'yyyy-MM-dd HH:mm:ss')}}</div>"
                },
                {
                    field: 'typeId', title: '收款类型', align: 'center'
                    , templet: function (data) {
                        if (data.typeId === 1) {
                            return '转账';
                        } else if (data.typeId === 2) {
                            return '收款';
                        } else if (data.typeId === 3) {
                            return '资金归集';
                        }
                    }
                },
                , {field: 'amount', title: '交易金额', align: 'center'}

                , {
                    field: 'curId', title: '货币类型', align: 'center'
                    , templet: function (data) {
                        if (data.curId === 1) {
                            return '人民币';
                        } else if (data.curId === 2) {
                            return '美元';
                        } else if (data.curId === 3) {
                            return '泰铢';
                        }
                    }
                }
                , {
                    field: 'state', title: '订单状态', align: 'center', templet: function (data) {
                        if (data.state === 1) {
                            return '已完成';
                        } else if (data.state === 2) {
                            return '未完成';
                        } else if (data.state === 3) {
                            return '待处理';
                        } else if (data.state === 4) {
                            return '异常';
                        }
                    }
                }
                , {field: 'note', title: '备注', align: 'center'}
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', align: 'center'}

            ]]
    });


    table.on('tool(translist)', function (obj) {
        // 获取当前行数据和lay-event的值
        var data = obj.data;
        var event = obj.event;
        var type = null;
        if(data.typeId==1){
            type = "转账";
        }else if(data.typeId==2){
            type="收款";
        }else if(data.typeId==3){
            type="资金归集";
        }
        var cur = null;
        if(data.curId==1){
            type = "人民币";
        }else if(data.curId==2){
            type="美元";
        }else if(data.curId==3){
            type="泰铢";
        }
        var cur = null;
        if(data.curId==1){
            cur = "人民币";
        }else if(data.curId==2){
            cur="美元";
        }else if(data.curId==3){
            cur="泰铢";
        }
        var s = null;
        if(data.state==1){
             s= "已完成";
        }else if(data.state==2){
            s="未完成";
        }else if(data.state==3){
            s="待处理";
        }else if(data.state==4){
            s="异常";
        }

      // 更新用户事件
        if (event === 'viewTrans') {
            $("#time").attr('value', data.time)
            $("#typeId").attr('value', type)
            $("#amount").attr('value', data.amount)
            $("#curId").attr('value', cur)
            $("#state").attr('value', s)
            $("#note").attr('value', data.note)
            // form.val('view-payee-form', {
            //     "time": data.time,
            //     "typeId": data.typeId,
            //     "amount": data.amount,
            //     "curId": data.curId,
            //     "state": data.state,
            //     "note": data.note
            // });
            //查看收款人详细信息弹出层
            layer.open({
                type: 1,
                title: '查看详情',
                skin: 'layui-layer-molv',
                area: ['700px'],
                shade: 0,
                content: $('#view-trans-layer')
            });
        } else if (event === 'delTrans') {
            layer.confirm('真的要删除该条记录吗么', function (index) {
                // obj.del();
                deluser(data, index, obj);
                layer.close(index);
            });

            function deluser(data, index, obj) {
                $.ajax({
                    url: "/transRecords/transdel?id=" + data.id,    //这个是后台的路由地址
                    type: "GET",
                    data: JSON.stringify(data.field),//传给后台的值
                    dataType: "json",
                    success: function (data) {
                        if (data) {   //从前台取回的状态值
                            layer.close(index);
                            //同步更新表格和缓存对应的值
                            obj.del();
                            layer.msg("删除成功", {icon: 6});
                        } else {
                            layer.msg("删除失败", {icon: 5});
                        }
                    }
                });
            }
        }
    });


});