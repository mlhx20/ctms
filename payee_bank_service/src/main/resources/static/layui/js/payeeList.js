layui.use(['layer', 'form', 'element', 'laypage', 'upload', 'tree', 'carousel', 'flow', 'table'], function () {
    var layer = layui.layer
        , form = layui.form
        , element = layui.element
        , $ = layui.jquery
        , laypage = layui.laypage
        , upload = layui.upload
        , tree = layui.tree
        , carousel = layui.carousel
        , table = layui.table
        , flow = layui.flow;
    //监听提交
    form.on('submit(searchSubmit)', function (data) {
        // TODO 校验
        //formSubmit(data);
        return false;
    });
    //监听提交
    form.on('submit(payeeSubmit)', function (data) {
        // TODO 校验
        //formSubmit(data);
        return false;
    });

    /*显示所有收款人信息*/
    var tableIns = table.render({
        elem: '#payeelist'
        , url: '/payee/payeelist'
        //
        // ,dataType: 'json'
        , toolbar: true //开启头部工具栏，并为其绑定左侧模板
        , title: '收款人表'
        , page: true
        , parseData: function (res) {
            console.log(res);
            return {
                "code": 0,
                "msg": "",
                "count": 1000,
                data: res
            }
        }
        , cols:
            [[
                {field: 'id', title: '收款人Id', align: 'center'},
                {field: 'name', title: '收款人姓名', align: 'center'}

                , {field: 'phone', title: '电话', align: 'center'}

                , {field: 'country', title: '国籍', align: 'center'}
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', align: 'center'}

            ]]
    });


    //监听搜索框
    form.on('submit(searchSubmit)', function (data) {
        load(data);
        console.log("data1" + JSON.stringify(data))

        function load(obj) {
            console.log("data2:" + JSON.stringify(data))
            //重新加载table
            tableIns.reload({
                where: obj.field
            });
            console.log("data3:" + JSON.stringify(data))
        }

        return false;//false：阻止表单跳转  true：表单跳转

    });


    //显示添加用户弹出层
    $('#add-user-btn').click(function () {
        // 每次显示前重置表单
        $('#add-user-form')[0].reset();
        layer.open({
            type: 1,
            title: '添加收款人',
            skin: 'layui-layer-molv',
            area: ['700px'],
            shade: 0,
            content: $('#add-user-layer')
        });
    });
    // 添加用户表单提交
    form.on('submit(add-user-form-submit)', function (data) {
        // ajax方式添加用户
        $.ajax({
            url: "/payee/payeeadd",
            type: "POST",
            data: JSON.stringify(data.field),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data) {
                    layer.close(layer.index);
                    layer.msg('添加成功');
                    //table.reload('user-tbl');
                    window.location.reload()  //刷新页面
                } else {
                    layer.msg('添加失败');
                }
            },
            error: function () {
                layer.msg('添加失败 ');
                console.log("ajax error");
            }

        });
        // 阻止表单跳转
        return false;
    });
    //修改收款人信息弹出层
    table.on('tool(payeelist)', function (obj) {
        // 获取当前行数据和lay-event的值
        var data = obj.data;
        var event = obj.event;
        // 更新用户事件
        if (event === 'updatePayee') {
            // 每次显示更新用户的表单前自动为表单填写该行的数据
            $("#editId").attr('value', data.id)
            $("#editName").attr('value', data.name)
            $("#editPhone").attr('value', data.phone)
            $("#editCode").attr('value', data.code)
            $("#editCountry").attr('value', data.country)
            $("#editAddress").attr('value', data.address)
            // form.val('update-payee-form', {
            //     "id": data.id,
            //     "name": data.name,
            //     "phone": data.phone,
            //     "code": data.code,
            //     "country": data.country,
            //     "address": data.address
            // });
            // 显示更新用户表单的弹出层
            layer.open({
                type: 1,
                title: '更新',
                skin: 'layui-layer-molv',
                area: ['500px'],
                shade: 0,
                content: $('#update-payee-layer')
            });
            // 更新用户表单提交
            form.on('submit(payeeSubmit)', function (data) {
                // ajax方式更新用户
                $.ajax({
                    url: "/payee/payeeupdate",
                    type: "POST",
                    data: JSON.stringify(data.field),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            layer.close(layer.index);
                            layer.msg('更新成功');
                            //table.reload('user-tbl');
                            window.location.reload()  //刷新页面
                        } else {
                            layer.msg('更新失败');
                        }
                    },
                    error: function () {
                        console.log("ajax error");
                    }
                });
                // 阻止表单跳转
                return false;
            });
        } else if (event === 'viewPayee') {
            $("#viewId").attr('value', data.id)
            $("#viewName").attr('value', data.name)
            $("#viewPhone").attr('value', data.phone)
            $("#viewCode").attr('value', data.code)
            $("#viewCountry").attr('value', data.country)
            $("#viewAddress").attr('value', data.address)
            // form.val('view-payee-form', {
            //     "id": data.id,
            //     "name": data.name,
            //     "phone": data.phone,
            //     "code": data.code,
            //     "country": data.country,
            //     "address": data.address
            // });
            //查看收款人详细信息弹出层
            layer.open({
                type: 1,
                title: '查看详情',
                skin: 'layui-layer-molv',
                area: ['700px'],
                shade: 0,
                content: $('#view-payee-layer')
            });
        } else if(event='delPayee') {
            layer.confirm('真的删除该收款人吗么', function (index) {
                // obj.del();
                deluser(data, index, obj);
                layer.close(index);
            });

            function deluser(data, index, obj) {
                $.ajax({
                    url: "/payee/payeedel?id="+data.id,    //这个是后台的路由地址
                    type: "GET",
                    data: JSON.stringify(data.field),//传给后台的值
                    dataType: "json",
                    success: function (data) {
                        if (data) {   //从前台取回的状态值
                            layer.close(index);
                            //同步更新表格和缓存对应的值
                            obj.del();
                            layer.msg("删除成功", {icon: 6});
                        } else {
                            layer.msg("删除失败", {icon: 5});
                        }
                    }
                });
                // $.ajax({
                //     url: "/payee/payeedel",
                //     type: "POST",
                //     data: JSON.stringify(data.field),
                //     contentType: 'application/json',
                //     dataType: 'json',
                //     success: function (data){
                //         alert("删除成功！！")
                //     },
                //     error: function () {
                //         console.log("ajax error");
                //     }
                // });
                // layer.confirm('真的删除phone为['+ data.phone +']的记录么？', function(index){
                //     deluser(data,index,obj);
                // } );

                //


            }
        }
    });


});