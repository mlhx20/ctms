package com.szit.bean;

import java.io.Serializable;

/**
 * 执行方式实体类
 */
public class ExecutStyle implements Serializable {
    private Integer id;
    private String name;

    public ExecutStyle() {
    }

    public ExecutStyle(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
