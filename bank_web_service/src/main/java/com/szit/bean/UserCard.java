package com.szit.bean;

import java.io.Serializable;

/**
 * 用户银行卡中间表实体类
 */
public class UserCard implements Serializable {
    private Integer id;
    private Integer userId;
    private Integer cardId;
    private Card card;
    private User user;
    private Integer cardSign; //卡签约状态，0：未签约 1：已签约付款 2：已签约收款

    public UserCard() {
        this.user = new User();
        this.card = new Card();
    }

    public UserCard(Integer id, Integer userId, Integer cardId, Card card, User user, Integer cardSign) {
        this.id = id;
        this.userId = userId;
        this.cardId = cardId;
        this.card = card;
        this.user = user;
        this.cardSign = cardSign;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getCardSign() {
        return cardSign;
    }

    public void setCardSign(Integer cardSign) {
        this.cardSign = cardSign;
    }
}
