package com.szit.bean;

import java.io.Serializable;

/**
 * 用户收款人中间表实体类
 */
public class UserPayee implements Serializable {
    private Integer id;
    private Integer userId;
    private Integer payeeId;
    private User user;
    private Payee payee;

    public UserPayee() {
        this.user = new User();
        this.payee = new Payee();
    }

    public UserPayee(Integer id, Integer userId, Integer payeeId, User user, Payee payee) {
        this.id = id;
        this.userId = userId;
        this.payeeId = payeeId;
        this.user = user;
        this.payee = payee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(Integer payeeId) {
        this.payeeId = payeeId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }
}
