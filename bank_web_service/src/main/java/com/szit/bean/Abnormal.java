package com.szit.bean;

import java.io.Serializable;

/**
 * 异常实体类
 */
public class Abnormal implements Serializable {
    private Integer id;
    private Integer userId; //用户id
    private Integer code;
    private Integer transId; //交易记录id
    private Integer type;
    private Integer state;
    private User user; //用户
    private TransRecords transRecords; //交易记录

    public Abnormal() {
        this.user = new User();
        this.transRecords = new TransRecords();
    }

    public Abnormal(Integer id, Integer userId, Integer code, Integer transId, Integer type, Integer state, User user, TransRecords transRecords) {
        this.id = id;
        this.userId = userId;
        this.code = code;
        this.transId = transId;
        this.type = type;
        this.state = state;
        this.user = user;
        this.transRecords = transRecords;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TransRecords getTransRecords() {
        return transRecords;
    }

    public void setTransRecords(TransRecords transRecords) {
        this.transRecords = transRecords;
    }
}
