package com.szit.bean;

import java.io.Serializable;

/**
 * 管理员实体类
 */
public class Administrator implements Serializable {
    private Integer id;
    private String name;
    private String pwd;

    public Administrator() {
    }

    public Administrator(Integer id, String name, String pwd) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
