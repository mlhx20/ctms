package com.szit.bean;

import java.io.Serializable;

/**
 * 交易类型实体类
 */
public class TypeTrade implements Serializable {
    private Integer id;
    private String name;

    public TypeTrade() {
    }

    public TypeTrade(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
