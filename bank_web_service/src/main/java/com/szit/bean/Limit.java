package com.szit.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 交易限额实体类
 */
public class Limit implements Serializable {
    private Integer id;
    private Integer typeId; //交易类型id
    private BigDecimal daylmt; //当日限额
    private BigDecimal translimit; //单笔交易限额
    private Integer userId; //用户id
    private User user; //用户
    private TypeTrade typeTrade; //交易类型

    public Limit() {
        this.user = new User();
        this.typeTrade = new TypeTrade();
    }

    public Limit(Integer id, Integer typeId, BigDecimal daylmt, BigDecimal translimit, Integer userId, User user, TypeTrade typeTrade) {
        this.id = id;
        this.typeId = typeId;
        this.daylmt = daylmt;
        this.translimit = translimit;
        this.userId = userId;
        this.user = user;
        this.typeTrade = typeTrade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getDaylmt() {
        return daylmt;
    }

    public void setDaylmt(BigDecimal daylmt) {
        this.daylmt = daylmt;
    }

    public BigDecimal getTranslimit() {
        return translimit;
    }

    public void setTranslimit(BigDecimal translimit) {
        this.translimit = translimit;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TypeTrade getTypeTrade() {
        return typeTrade;
    }

    public void setTypeTrade(TypeTrade typeTrade) {
        this.typeTrade = typeTrade;
    }
}
