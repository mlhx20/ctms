package com.szit.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 交易记录实体类
 */
public class TransRecords implements Serializable {
    private Integer id;
    private String code;
    private Integer userId; //汇款用户
    private Integer remitCardId; //汇款银行卡id
    private Integer payeeId; //收款人id
    private Integer receivCardId; //收款银行卡id
    private Integer typeId; //交易类型id
    private BigDecimal amount; //交易金额
    private Integer curId; //货币类型id
    private Date time; //交易时间
    private Integer state; //交易状态 1：已完成 2：未完成 3：待处理 4 异常
    private String purpose; //汇款用途
    private Integer exeId; //执行方式id
    private String additi; //附言
    private String note; //备注说明
    private User user;
    private Card remitCard;
    private Payee payee;
    private Card receivCard;
    private TypeTrade typeTrade;
    private Currency currency;

    public TransRecords() {
        this.user = new User();
        this.remitCard = new Card();
        this.payee = new Payee();
        this.receivCard = new Card();
        this.typeTrade = new TypeTrade();
        this.currency = new Currency();
    }

    public TransRecords(Integer id, String code, Integer userId, Integer remitCardId, Integer payeeId, Integer receivCardId, Integer typeId, BigDecimal amount, Integer curId, Date time, Integer state, String purpose, Integer exeId, String additi, String note) {
        this.id = id;
        this.code = code;
        this.userId = userId;
        this.remitCardId = remitCardId;
        this.payeeId = payeeId;
        this.receivCardId = receivCardId;
        this.typeId = typeId;
        this.amount = amount;
        this.curId = curId;
        this.time = time;
        this.state = state;
        this.purpose = purpose;
        this.exeId = exeId;
        this.additi = additi;
        this.note = note;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Card getRemitCard() {
        return remitCard;
    }

    public void setRemitCard(Card remitCard) {
        this.remitCard = remitCard;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    public Card getReceivCard() {
        return receivCard;
    }

    public void setReceivCard(Card receivCard) {
        this.receivCard = receivCard;
    }

    public TypeTrade getTypeTrade() {
        return typeTrade;
    }

    public void setTypeTrade(TypeTrade typeTrade) {
        this.typeTrade = typeTrade;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRemitCardId() {
        return remitCardId;
    }

    public void setRemitCardId(Integer remitCardId) {
        this.remitCardId = remitCardId;
    }

    public Integer getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(Integer payeeId) {
        this.payeeId = payeeId;
    }

    public Integer getReceivCardId() {
        return receivCardId;
    }

    public void setReceivCardId(Integer receivCardId) {
        this.receivCardId = receivCardId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getCurId() {
        return curId;
    }

    public void setCurId(Integer curId) {
        this.curId = curId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Integer getExeId() {
        return exeId;
    }

    public void setExeId(Integer exeId) {
        this.exeId = exeId;
    }

    public String getAdditi() {
        return additi;
    }

    public void setAdditi(String additi) {
        this.additi = additi;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
