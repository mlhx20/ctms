package com.szit.bean;

import java.io.Serializable;

/**
 * 收款人银行卡中间表实体类
 */
public class PayeeCard implements Serializable {
    private Integer id;
    private Integer payeeId;
    private Integer cardId;
    private Payee payee;
    private Card card;

    public PayeeCard() {
        this.card = new Card();
        this.payee = new Payee();
    }

    public PayeeCard(Integer id, Integer payeeId, Integer cardId, Payee payee, Card card) {
        this.id = id;
        this.payeeId = payeeId;
        this.cardId = cardId;
        this.payee = payee;
        this.card = card;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(Integer payeeId) {
        this.payeeId = payeeId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
