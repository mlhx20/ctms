package com.szit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EntityScan("com.szit.model.bean, com.szit.tool.result, com.szit.tool")
public class BankWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(BankWebApplication.class,args);
    }
}
