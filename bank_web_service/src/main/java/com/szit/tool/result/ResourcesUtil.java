package com.szit.tool.result;

import com.szit.tool.Config;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 * Desc:读取资源文件工具 Created by Charles on 2017/3/21.
 */
public class ResourcesUtil implements Serializable {

	private static final long serialVersionUID = 3228876809474248267L;

	/**
	 * 设置国家与语言
	 * 
	 * @return 本地信息
	 */
	private static Locale getLocal() {
		return new Locale(Config.LANGUAGE, Config.COUNTRY);
	}

	/**
	 * 根据资源文件和key获取文件值
	 * 
	 * @param baseName 资源文件
	 * @param key      key值
	 * @return 查到的值
	 */
	private static String getProperties(String baseName, String key) {
		return (String) ResourceBundle.getBundle(baseName, getLocal()).getObject(key);
	}

	/**
	 * 根据资源文件获取所有key
	 * 
	 * @param baseName 资源文件
	 * @return 没有返回NULL, 有则key数组
	 */
	private static List<String> getKeyList(String baseName) {
		Set<String> keySet = ResourceBundle.getBundle(baseName).keySet();
		if (keySet.size() > 0) {
			List<String> keyList = new ArrayList<String>();
			for (String key : keySet) {
				keyList.add(key);
			}
			return keyList;
		}
		return null;
	}

	/**
	 * 根据默认资源文件和key获取文件值
	 * 
	 * @param key key值
	 * @return 查到的值
	 */
	public static String getValue(String key) {
		return getProperties(Config.MESSAGEFILE, key);
	}

	/**
	 * 根据资源文件和key获取文件值
	 * 
	 * @param fileName 资源文件名
	 * @param key      key值
	 * @return 查到的值
	 */
	public static String getValue(String fileName, String key) {
		return getProperties(fileName, key);
	}

	/**
	 * 根据资源文件和key并通过传参获取文件值
	 * 
	 * @param fileName 资源文件
	 * @param key      key值
	 * @param objs     参数数组
	 * @return 查到的值
	 */
	public static String getValue(String fileName, String key, Object[] objs) {
		return MessageFormat.format(getValue(fileName, key), objs);
	}

}
