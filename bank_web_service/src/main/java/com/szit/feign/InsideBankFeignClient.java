package com.szit.feign;

import com.szit.model.bean.Card;
import com.szit.model.bean.Payee;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 调用行内服务
 */

@FeignClient(name = "inside-bank-service",fallback = InsideBankFeignClientFallBack.class)
public interface InsideBankFeignClient {


    @GetMapping("/card/getCardByUserIdAndCardSign")
    @ResponseBody
    public  List<Card> getCardByUserIdAndCardSign(@RequestParam Integer userId,@RequestParam Integer cardSign);

    @GetMapping("/card/updateCardSign")
    @ResponseBody
    public  boolean updateCardSign(@RequestParam Integer cardSign,@RequestParam Integer userId,@RequestParam Integer cardId);

    @GetMapping("/card/getCardByPayId")
    @ResponseBody
    public  List<Card> getCardByPayId(@RequestParam Integer payeeId);

    @GetMapping("/card/getCardByCardNo")
    @ResponseBody
    public  Card getCardByCardNo(@RequestParam String cardNo);

    @RequestMapping(value = "/card/getCardsByUserId",method = RequestMethod.GET)
    @ResponseBody
    public  List<Card> getCardsByUserId(@RequestParam Integer userId);
    /**
     * 转账处理
     * @param transRecords
     * @return
     */
    @RequestMapping(value = "/transRecords/insideTrans",method = RequestMethod.POST)
    public int insideTrans(TransRecords transRecords);

    /**
     * 获取收款人
     * @param userId
     * @return
     */
    @RequestMapping(value = "/payee/getpayee",method = RequestMethod.GET)
    @ResponseBody
    public List<Payee> getPayee(@RequestParam Integer userId);

    @GetMapping("/user/queryUserByUserName")
    @ResponseBody
    public User queryUserByUserName(@RequestParam String userName);

    @GetMapping("/user/queryUserByCardId")
    @ResponseBody
    public User queryUserByCardId(@RequestParam Integer cardId);

    @RequestMapping(value = "/transRecords/setTransRecords",method = RequestMethod.POST)
    @ResponseBody
    public boolean setTransRecords(@RequestBody  TransRecords transRecords);

    @RequestMapping(value = "/transRecords/selectTransRecords",method = RequestMethod.GET)
    @ResponseBody
    public List<TransRecords> selectTransRecords(@RequestParam  Integer userId,@RequestParam Integer state);

    @RequestMapping(value = "/transRecords/getTransRecordsByTransId",method = RequestMethod.GET)
    @ResponseBody
    public TransRecords getTransRecordsByTransId(@RequestParam Integer transId);

    @RequestMapping(value = "/transRecords/updateTransRecordsState",method = RequestMethod.GET)
    @ResponseBody
    public boolean updateTransRecordsState(@RequestParam Integer transId,@RequestParam Integer state);

}
