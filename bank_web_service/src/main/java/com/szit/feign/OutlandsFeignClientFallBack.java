package com.szit.feign;

import com.szit.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.TokenBean;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OutlandsFeignClientFallBack implements OutlandsFeignClient {

    @Override
    public boolean login(TokenBean tokenBean) {
        return false;
    }

    @Override
    public boolean reg(User user, String pwd2) {
//        ResultInfo resultInfo = ResultUtil.createFail(900201);
        return false;
    }

    @Override
    public boolean existUser(User user) {
        System.out.println("调用降级方法！");
//        throw new RuntimeException();
        return true;
    }

    @Override
    public List<User> findUsers(Map paraMap) {
        System.out.println("进入降级");
        return new ArrayList<>();
    }

    @Override
    public List<User> getUsersByOutlands(User user) {
        System.out.println("进入降级");
        return null;
    }

    @Override
    public DataTableJson getPayeeByPage(Integer pageNo, Integer pageSize) {
        System.out.println("进入降级");
        return null;
    }

    @Override
    public DataTableJson getPayeeCard(Integer payeeId, Integer pageNo, Integer pageSize) {
        return null;
    }

    @Override
    public DataTableJson getPayeeByUser(Integer userId,Integer payeeId, Integer pageNo, Integer pageSize) {
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setMsg("请求失败！");
        return dataTableJson;
    }

    @Override
    public DataTableJson getCardByUserId(Integer userId, Integer pageNo, Integer pageSize) {
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setMsg("请求失败！");
        return dataTableJson;
    }

    @Override
    public String trans(TransRecords transRecords) {
        return "网络异常！";
    }

    @Override
    public String batchTrans(List<TransRecords> transRecordsList) {
        return "网络异常";
    }

    @Override
    public boolean loginOut(String access) {
        return false;
    }

    @Override
    public User getUserByToken(String accessToken) {
        return new User();
    }

    @Override
    public boolean existUserIdCardId(Integer userId, Integer cardId) {
        return false;
    }

}
