package com.szit.feign;

import com.szit.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.TokenBean;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name = "outlands-bank-service",fallback = OutlandsFeignClientFallBack.class)
public interface OutlandsFeignClient {


    @RequestMapping(value = "/user/login",method = RequestMethod.POST)
    @ResponseBody
    public boolean login(@RequestBody TokenBean tokenBean);
    @RequestMapping(value = "/user/reg",method = RequestMethod.POST)
    @ResponseBody
    public boolean reg(@RequestBody User user, @RequestParam("pwd2")String pwd2);
    @RequestMapping(value = "/user/exist",method = RequestMethod.POST)
    @ResponseBody
    public boolean existUser(@RequestBody User user);
    @RequestMapping(value = "/user/findusers",method = RequestMethod.POST)
    @ResponseBody
    public List<User> findUsers(@RequestBody Map paraMap);

    @RequestMapping(value = "/user/getusers",method = RequestMethod.POST)
    @ResponseBody
    public List<User> getUsersByOutlands(@RequestBody User user);
//收款人
    @RequestMapping(value = "/payee/getpage",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeByPage(@RequestParam(value = "page",required = false) Integer pageNo,
                                        @RequestParam(value = "limit",required = false) Integer pageSize);

    @RequestMapping(value = "/payee/card/getpayees",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeCard(@RequestParam(value = "id",required = false) Integer payeeId,
                                        @RequestParam(value = "page",required = false) Integer pageNo,
                                        @RequestParam(value = "limit",required = false) Integer pageSize);
    @RequestMapping(value = "/user/payee/getpayees",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeByUser(@RequestParam(value = "id",required = false) Integer userId,
                                        @RequestParam(value = "payee",required = false)Integer payeeId,
                                       @RequestParam(value = "page",required = false) Integer pageNo,
                                       @RequestParam(value = "limit",required = false) Integer pageSize);
    @RequestMapping(value = "/user/card/getcards",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getCardByUserId(@RequestParam(value = "userid",required = false) Integer userId,
                                         @RequestParam(value = "page",required = false) Integer pageNo,
                                         @RequestParam(value = "limit",required = false) Integer pageSize);
    @RequestMapping(value = "/trans/trans",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String trans(@RequestBody TransRecords transRecords);

    @RequestMapping(value = "/trans/batch",method = RequestMethod.POST)
    @ResponseBody
    public String batchTrans(@RequestBody List<TransRecords> transRecordsList);
    @RequestMapping(value = "/user/loginout",method = RequestMethod.POST)
    @ResponseBody
    public boolean loginOut(@RequestParam String access);
    @RequestMapping(value = "/user/getuserbytoken",method = RequestMethod.GET)
    @ResponseBody
    public User getUserByToken(@RequestParam String accessToken);
    @RequestMapping(value = "/user/card/existcard",method = RequestMethod.GET)
    @ResponseBody
    public boolean existUserIdCardId(@RequestParam(value = "userid",required = true)Integer userId,
                                     @RequestParam(value = "cardid",required = true)Integer cardId);
}
