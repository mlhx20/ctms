package com.szit.feign;

import com.szit.model.bean.Card;
import com.szit.model.bean.Payee;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InsideBankFeignClientFallBack implements InsideBankFeignClient {

    @Override
    public List<Card> getCardByUserIdAndCardSign(Integer userId, Integer cardSign) {
        System.out.println("获取失败！");
        return null;
    }

    @Override
    public boolean updateCardSign(Integer cardSign, Integer userId, Integer cardId) {
        return false;
    }

    @Override
    public List<Card> getCardByPayId(Integer payeeId) {
        System.out.println("fallback------");
        return null;
    }

    @Override
    public Card getCardByCardNo(String cardNo) {
        return null;
    }

    @Override
    public List<Card> getCardsByUserId(Integer userId) {
        return null;
    }

    @Override
    public int insideTrans(TransRecords transRecords) {
        return 0;
    }



    @Override
    public List<Payee> getPayee(Integer userId) {
        return null;
    }

    @Override
    public User queryUserByUserName(String userName) {
        return null;
    }

    @Override
    public User queryUserByCardId(Integer cardId) {
        System.out.println("查真实姓名降级");
        return null;
    }

    @Override
    public boolean setTransRecords(TransRecords transRecords) {
        System.out.println("主动收款降级");
        return false;
    }

    @Override
    public List<TransRecords> selectTransRecords(Integer userId, Integer state) {
        System.out.println("查询交易记录降级");
        return null;
    }

    @Override
    public TransRecords getTransRecordsByTransId(Integer transId) {
        System.out.println("主动收款查询交易记录降级");
        return null;
    }

    @Override
    public boolean updateTransRecordsState(Integer transId, Integer state) {
        System.out.println("删除降级");
        return false;
    }


}
