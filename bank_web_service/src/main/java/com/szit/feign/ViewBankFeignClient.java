package com.szit.feign;

import com.szit.model.bean.Card;
import com.szit.model.bean.Transbank;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FeignClient(name="outside-bank-service",fallback= ViewBankFeignClientFallBack.class)
public interface ViewBankFeignClient {

    //调用outside-bank-service 服务进行跨行转账
    @RequestMapping(value = "/enja/transfer",method = RequestMethod.POST)
    @ResponseBody
    public Integer bankTransfer(@RequestParam("payerCardNo") String payerCardNo
            ,@RequestParam("payeeCardNo")String payeeCardNo
            ,@RequestParam("amount")double amount
            ,@RequestParam("id")Integer id);

    //调用outside-bank-service 服务进行资金归集
    @RequestMapping(value="/enja/fundCollect",method=RequestMethod.POST)
    @ResponseBody
    public Integer fundCollect(@RequestParam("mastercardno") String mastercardno,
                               @RequestParam("collectcardno") String collectcardno,
                               @RequestParam("gruancardMoney") double gruancardMoney,
                               @RequestParam("id")Integer id);

    //查询当前用户的主卡号
    @RequestMapping(value = "/enja/queryMasterCard",method = RequestMethod.POST)
    public String selectMasterCardBySign(@RequestParam("id") Integer id);

    //根据用户ID获取当前用户的所有未归集的银行卡号
    @RequestMapping(value="/enja/allCardNoByUserId",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectAllCardNoById(@RequestParam("id") Integer id);

    //获取当前用户非主卡的其他卡
    @RequestMapping("/enja/queryCardNoMasterCard")
    @ResponseBody
    public List<String> selectCardNoMasterCardNoById(@RequestParam("id") Integer id);

    //获取当前登录用户的所有银行卡号
    @RequestMapping(value="/enja/selectCardNoByUserId",method=RequestMethod.POST)
    @ResponseBody
    public List<String> selectCardNoByUserId(@RequestParam("id") Integer id);

    //获取当前登录用户的所有收款人姓名
    @RequestMapping(value = "/enja/selectAllPayeeName",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectAllPayeeNameById(@RequestParam("id") Integer id);

    //自动筛选出该收款人不同于付款人的他行账号
    @RequestMapping(value="/enja/selectPayeeCardNo",method=RequestMethod.POST)
    @ResponseBody
    public List<String> selectPayeeCardNo(@RequestParam("payeename")String payeename,@RequestParam("cardno")String cardno);

    //根据卡号查询支付密码
    @RequestMapping(value="/enja/selectPwd",method=RequestMethod.POST)
    @ResponseBody
    public String selectPwdByCardNo(@RequestParam("cardno") String cardno);

    //修改主卡为未归集状态
    @RequestMapping(value = "/enja/searchMasterCard",method = RequestMethod.POST)
    @ResponseBody
    public Integer searchMasterCardByCardNo();

    //修改归集卡为主卡
    @RequestMapping(value = "/enja/searchCollectCard",method = RequestMethod.POST)
    public Integer searchCollectCardByCardNo(@RequestParam("collectCardNo") String collectCardNo);

    //根据卡号查询在哪家银行
    @RequestMapping(value = "/enja/selectCollectCard",method = RequestMethod.POST)
    public Integer selectBankTypeByCardNo(@RequestParam("cardno")String cardno);

    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    @RequestMapping(value = "/enja/selectBankNameAndCardNo" , method = RequestMethod.POST)
    @ResponseBody
    public List<Card> selectBankNameAndCardNo(@RequestParam("cardno")String cardno, @RequestParam("id")Integer id);

    //修改归集卡的状态
    @RequestMapping(value = "/enja/searchCollectSignByCardNo",method = RequestMethod.POST)
    @ResponseBody
    public Integer searchCollectSignByCardNo(@RequestParam("cardno") String cardno);

}
