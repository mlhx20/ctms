package com.szit.feign;

import com.szit.model.bean.Card;
import com.szit.model.bean.Transbank;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
public class ViewBankFeignClientFallBack implements ViewBankFeignClient {

    @Override
    public Integer bankTransfer(String payerCardNo, String payeeCardNo, double amount, Integer id) {
        return null;
    }

    @Override
    public Integer fundCollect(String mastercardno, String collectcardno, double gruancardMoney, Integer id) {
        return 1;
    }

    @Override
    public String selectMasterCardBySign(Integer id) {
        return null;
    }

    @Override
    public List<String> selectAllCardNoById(Integer id) {
        return null;
    }

    @Override
    public List<String> selectCardNoMasterCardNoById(Integer id) {
        return null;
    }

    @Override
    public List<String> selectCardNoByUserId(Integer id) {
        return null;
    }

    @Override
    public List<String> selectAllPayeeNameById(Integer id) {
        return null;
    }

    @Override
    public List<String> selectPayeeCardNo(String payeename, String cardno) {
        return null;
    }

    @Override
    public String selectPwdByCardNo(String cardno) {
        return null;
    }

    @Override
    public Integer searchMasterCardByCardNo() {
        return null;
    }

    @Override
    public Integer searchCollectCardByCardNo(String collectCardNo) {
        return null;
    }

    @Override
    public Integer selectBankTypeByCardNo(String cardno) {
        return null;
    }

    @Override
    public List<Card> selectBankNameAndCardNo(String cardno, Integer id) {
        return null;
    }

    @Override
    public Integer searchCollectSignByCardNo(String cardno) {
        return null;
    }


}
