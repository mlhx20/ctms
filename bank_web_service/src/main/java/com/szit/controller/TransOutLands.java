package com.szit.controller;

import com.szit.bean.Payee;
import com.szit.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.bean.UserPayee;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/outlands")
public class TransOutLands extends BaseController {


    @RequestMapping(value = "/getloginuser",method = RequestMethod.GET)
    @ResponseBody
    public User getLoginUser(@RequestParam String access_token){
        User loginUser = new User();
        User userToken = outlandsFeignClient.getUserByToken(access_token);
        loginUser.setName(userToken.getName());
        loginUser.setId(userToken.getId());
        return loginUser;
    }

    @RequestMapping(value = "/trans",method = RequestMethod.POST)
    @ResponseBody
    public String trans(@RequestParam Integer receivCardId,
                                  @RequestParam Integer payeeId,
                                  @RequestParam Integer remitCardId,
                                  @RequestParam Integer userId,
                                  @RequestParam BigDecimal amount,
                                  @RequestParam String password){
        TransRecords transRecords = new TransRecords();
        transRecords.setUserId(userId);
        transRecords.setPayeeId(payeeId);
        transRecords.setReceivCardId(receivCardId);
        transRecords.setRemitCardId(remitCardId);
        transRecords.setAmount(amount);
        transRecords.getRemitCard().setPwd(password);
        String msg = outlandsFeignClient.trans(transRecords);
        return msg;
    }
    List<Payee> defaultPayeeList;

    @RequestMapping(value = "/getbatchpayees",method = RequestMethod.POST)
    @ResponseBody
    public Integer getBatchPayees(@RequestBody List<Payee> payeeList){
        defaultPayeeList = new ArrayList<>();
        Integer res = 0;
        if(payeeList.size()>0){
            defaultPayeeList = payeeList;
            res = 1;
        }
        return res;
    }
    @RequestMapping(value = "/batchTrans",method = RequestMethod.POST)
    @ResponseBody
    public SubmitResultInfo batchTrans(@RequestParam Integer userId,
                             @RequestParam Integer remitCardId,
                             @RequestParam String password){
        ResultInfo resultInfo = new ResultInfo();
        int type =0;
        resultInfo.setMessage("付款卡不存在！");
        if(outlandsFeignClient.existUserIdCardId(userId,remitCardId)){
            List<TransRecords> recordsList = new ArrayList<>();
            for(Payee payee:defaultPayeeList){
                TransRecords transRecords = new TransRecords();
                transRecords.setPayeeId(payee.getId());
                transRecords.setPayee(payee);
                transRecords.setReceivCardId(payee.getCardId());
                transRecords.setAmount(payee.getMoney());
                transRecords.setRemitCardId(remitCardId);
                transRecords.setUserId(userId);
                transRecords.getRemitCard().setPwd(password);
                recordsList.add(transRecords);
            }
            defaultPayeeList.clear();
            String res = outlandsFeignClient.batchTrans(recordsList);
            resultInfo.setMessage(res);
            if(res.equals("转账成功")){
                type=1;
            }
        }
        resultInfo.setType(type);
        return ResultUtil.createSubmitResult(resultInfo);
    }

}
