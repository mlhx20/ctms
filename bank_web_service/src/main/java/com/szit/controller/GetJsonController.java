package com.szit.controller;

import com.szit.feign.ViewBankFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GetJsonController {

    @Autowired
    private ViewBankFeignClient viewBankFeignClient;

    @RequestMapping(value = "/getCollect",method = RequestMethod.POST)
    public List<String> getCollect(@RequestParam("collectId")String collectId,@RequestParam("depositType") String depositType,@RequestParam("depositMethod")String depositMethod){
        List<String> collects = new ArrayList<>();

        collects.add(collectId);
        collects.add(depositType);
        collects.add(depositMethod);
        System.out.println(collectId+depositType+depositMethod);
        return collects;
    }
}
