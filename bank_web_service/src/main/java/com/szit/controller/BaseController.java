package com.szit.controller;

import com.szit.feign.OutlandsFeignClient;
import com.szit.feign.ViewBankFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController {
    @Autowired
    protected OutlandsFeignClient outlandsFeignClient;
    @Autowired
    protected ViewBankFeignClient viewBankFeignClient;
    @Autowired
    protected RedisTemplate redisTemplate;

}
