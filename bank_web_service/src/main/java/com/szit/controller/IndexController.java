package com.szit.controller;

import com.szit.model.bean.User;
import com.szit.feign.ViewBankFeignClient;
import com.szit.model.bean.Card;
import com.szit.tool.TokenBean;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @description: 前端控制器
 * @author: xu zhihao
 * @create: 2019-06-14 10:36
 */
@Controller
public class IndexController extends BaseController {

    private final String PRE_PATH="/outlands"; //前置路径

    //从redis获取用户登录信息

//    User user = (User)redisTemplate.opsForValue().get("");

    Integer id = 242;

    @GetMapping("/models/index.html")
    public String getIndex() {
        return "models/index.html";
    }

    @GetMapping("/trans.html")
    public String getTransView(){return "/models/trans.html";}


    //获取当前登录用户的所有银行卡号
    @RequestMapping(value="/selectCardNoByUserId",method=RequestMethod.POST)
    @ResponseBody
    public List<String> selectCardNoByUserId(){

        return viewBankFeignClient.selectCardNoByUserId(id);
    }

    //获取当前登录用户的所有收款人姓名
    @RequestMapping(value = "/selectAllPayeeName",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectAllPayeeNameById(){
        List<String> stringList = viewBankFeignClient.selectAllPayeeNameById(id);
        System.out.println("stringList:"+stringList.size());
        return stringList;
    }

    //自动筛选出该收款人不同于付款人的他行账号
    @RequestMapping(value="/selectPayeeCardNo",method=RequestMethod.GET)
    @ResponseBody
    public List<String> selectPayeeCardNo(@RequestParam("payeename")String payeename,@RequestParam("cardno")String cardno){
        return viewBankFeignClient.selectPayeeCardNo(payeename,cardno);
    }

    //跨行转账
    @RequestMapping(value = "/transfer" ,method = RequestMethod.POST)
    @ResponseBody
    public Integer getCardAndName(@RequestParam("payerCardNo") String payerCardNo
                                ,@RequestParam("payeeCardNo")String payeeCardNo
                                ,@RequestParam("amount")double amount
                                ,@RequestParam("paypwd")String paypwd){



        Integer model = -1;

        String payerPwd = viewBankFeignClient.selectPwdByCardNo(payerCardNo);
        if(payerPwd.equals(paypwd)){
            //支付密码匹配成功
            Integer transferflag = viewBankFeignClient.bankTransfer(payerCardNo,payeeCardNo,amount,id);

            if(transferflag==1){
                //转账成功
                model = 1;
            }else if(transferflag==-1){
                //余额不足
                model=0;
            }
        }else{
            //支付密码匹配失败
            model=-2;
        }

        return model;
    }

    //判断是否有主卡
    @RequestMapping(value = "/isMasterCard",method = RequestMethod.GET)
    @ResponseBody
    public boolean isMasterCard(){
        //判断用户是否有主卡
        String masterCard = viewBankFeignClient.selectMasterCardBySign(id);

        if(masterCard!=null){
            return true;
        }

        return false;
    }


    //获取当前用户的是否有主卡
    @RequestMapping(value = "/findMasterCard",method = RequestMethod.GET)
    @ResponseBody
    public List<String> selectAllCadNoBySign(){

        List<String> listCard = new ArrayList<>();
        //判断用户是否有主卡
        String masterCard = viewBankFeignClient.selectMasterCardBySign(id);
        if(masterCard!=null){
            //有主卡，只需返回一张卡号
           listCard.add(masterCard);
        }else{

            listCard = viewBankFeignClient.selectAllCardNoById(id);
        }

        return listCard;
    }

    //获取非主卡的其他卡
    //获取当前用户获取非主卡的其他卡
    //修改主卡状态
    @RequestMapping(value = "/queryCardNoMasterCard",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectCardNoMasterCardNoById(){

        List list = new ArrayList();

        //修改归集卡状态
        //获取非主卡的其他卡
        list =  viewBankFeignClient.selectCardNoMasterCardNoById(id);

        //修改主卡状态
        Integer flag = viewBankFeignClient.searchMasterCardByCardNo();
        if(flag>0){
            //修改主卡成功
        }else if(flag<1){
            System.out.println("修改主卡失败");
            return null;
        }else if(flag==null){
            System.out.println("调用服务失败");
        }

        return list;
    }

    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    @RequestMapping(value = "/selectBankNameAndCardNo" , method = RequestMethod.GET)
    @ResponseBody
    public List<Card> selectBankNameAndCardNo(@RequestParam("cardno")String cardno){

        System.out.println(cardno);
        System.out.println(viewBankFeignClient.selectBankNameAndCardNo(cardno,id));

        return viewBankFeignClient.selectBankNameAndCardNo(cardno,id);
    }

    //获取当前用户的所有未被归集的卡
    @RequestMapping(value = "/findNoTransCardNo",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectNoTransCardNoBySign(){
        return viewBankFeignClient.selectAllCardNoById(id);
    }

    @RequestMapping("/fundCollect.html")
    public String getJindu(){
        return "/fundCollect.html";
    }

    //资金归集
    @RequestMapping(value = "/fundCollect",method = RequestMethod.POST)
    @ResponseBody
//    @Scheduled(cron = "0 */1 * * * ?")
    public Integer fundCollect(@RequestParam("mastercardno") String mastercardno,
                               @RequestParam("collectcardno") String collectcardno,
                               @RequestParam("gruanMoney") double gruanMoney){

        Integer model = null;

        Integer transflag = viewBankFeignClient.fundCollect(mastercardno,collectcardno,gruanMoney,id);

        System.out.println("transflag:"+transflag);

        if(transflag==1){
            //归集成功

            //将修改后的主卡状态改变
            Integer flag = viewBankFeignClient.searchCollectCardByCardNo(mastercardno);
            if(flag>0){
                //修改主卡成功
                //修改归集后的归集卡状态
                Integer collflag = viewBankFeignClient.searchCollectSignByCardNo(collectcardno);
                if(collflag>0){
                    model=1;
                }else if(collflag<1){
                    System.out.println("修改归集卡失败");
                    return null;
                }else if(collflag==null){
                    System.out.println("调用服务失败");
                }
            }else if(flag<1){
                System.out.println("修改主卡失败");
                return null;
            }else if(flag==null){
                System.out.println("调用服务失败");
            }
        }else if(transflag==0){
            //归集失败
            model=0;
        }else if(transflag==-1){
            //余额不足，归集失败
            model=-1;
        }else if(transflag==null){
            //调用服务失败

        }

        return model;
    }
    @GetMapping("/login.html")
    public String login() {
        return PRE_PATH+"/login.html";
    }
    @PostMapping("/dologin.html")
    @ResponseBody
    public SubmitResultInfo dologin( TokenBean tokenBean,HttpSession session) {
        ResultInfo resultInfo = new ResultInfo();
        session.setAttribute("infoUser", tokenBean);
        String message ="用户名不存在！";
//        int message = 0;
        String res = "/login.html";
        resultInfo.setIndex(ResultInfo.TYPE_RESULT_FAIL);
        if(tokenBean != null && tokenBean.getUsername() != null){
            if(existUserName(tokenBean.getUsername())){
                String access= UUID.randomUUID().toString();
                tokenBean.setClient_id(access);
                boolean boo = outlandsFeignClient.login(tokenBean);
                if(boo){
                    resultInfo.setIndex(ResultInfo.TYPE_RESULT_SUCCESS);
                    Map sysData = new HashMap();
                    sysData.put("access_token", access);
                    resultInfo.setSysdata(sysData);
                    resultInfo.setType(1);
                    message = "登录成功！";
//                    message = 1;
                }else {
                    message = "登录失败！";
                }
            }
        }
        resultInfo.setMessage(message);
        SubmitResultInfo submitResultInfo = ResultUtil.createSubmitResult(resultInfo);
//        if(message==1){
//            res = "/index.html";
//        }
        return submitResultInfo;
    }
    @RequestMapping(value = "/loginout",method = RequestMethod.GET)
    @ResponseBody
    public String loginOut(@RequestParam String access){
        boolean boo = outlandsFeignClient.loginOut(access);
        String res = "";
        if (boo){
            res= "/login.html";
        }
        return res;
    }
    @RequestMapping("/reg")
    public String register() {
        return PRE_PATH+"/register.html";
    }
    @PostMapping("/doreg")
    @ResponseBody
    public SubmitResultInfo doRegister(User user, String pwd2, HttpSession session) {
        ResultInfo resultInfo = new ResultInfo();
        String message ="用户已注册，请直接登录！";
        resultInfo.setIndex(ResultInfo.TYPE_RESULT_FAIL);
        if(!outlandsFeignClient.existUser(user)){
            message = "用户名已存在！";
            if(!existUserName(user.getName())){
                message = "两次密码不一致！";
                if(user.getPwd().equals(pwd2)){
                    boolean boo = outlandsFeignClient.reg(user,pwd2);
                    if(boo){
                        session.removeAttribute("regUser");
                        message = "注册成功！";
                        resultInfo.setIndex(ResultInfo.TYPE_RESULT_SUCCESS);
                    }else {
                        message = "注册失败！";
                    }
                }
            }

        }
        resultInfo.setMessage(message);
        return ResultUtil.createSubmitResult(resultInfo);
    }
//    @RequestMapping("/a")
//    public String a() {
//        return "/a.html";
//    }

    @RequestMapping(value = "/getexist",method = RequestMethod.GET)
    @ResponseBody
    public boolean getExistUser(User user) {
        boolean boo = outlandsFeignClient.existUser(user);
        return boo;
    }
    @RequestMapping(value = "/existname",method = RequestMethod.GET)
    @ResponseBody
    public boolean existUserName(String userName) {
        Map param = new HashMap();
        param.put("name",userName);
        boolean boo = outlandsFeignClient.findUsers(param).size()>0?true:false;
        return boo;
    }
//    @RequestMapping(value = "/getexist",method = RequestMethod.GET)
//    @ResponseBody
//    public boolean getusersby1(User user) {
//        boolean boo = outlandsFeignClient.existUser(user);
//        return boo;
//    }
    /**
     * 跨境转账
     */
    @RequestMapping(value = "/outlands.html",method = RequestMethod.GET)
    public String toTrans(){
        return PRE_PATH+"/outlands.html";
    }
    @RequestMapping("/batch.html")
    public String toBatchTrans(){
        return PRE_PATH+"/batch.html";
    }
    @RequestMapping("/batch2.html")
    public String toBatchTrans2() {
        return PRE_PATH + "/batch2.html";
    }
    @RequestMapping(value = "/web/index")
    public String index() {
        return "index.html";
    }
    @RequestMapping(value = "/web/limit")
    public String limit(){
        return "limit.html";
    }
    @RequestMapping(value = "/web/payeelist")
    public String payeeList(){
        return "payeelist.html";
    }
    @RequestMapping(value = "/web/translist")
    public String transList(){
        return "translist.html";
    }

}
