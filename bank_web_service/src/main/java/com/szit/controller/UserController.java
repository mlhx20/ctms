package com.szit.controller;

import com.szit.model.bean.User;
import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Random;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController{

    @RequestMapping(value = "/getcards",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getCardByUserId(@RequestParam(value = "id",required = false) Integer userId,
                                         @RequestParam(value = "page",required = false) Integer pageNo,
                                         @RequestParam(value = "limit",required = false) Integer pageSize){
        return outlandsFeignClient.getCardByUserId(userId,pageNo,pageSize);
    }
    @RequestMapping(value = "/getuserbytoken",method = RequestMethod.GET)
    @ResponseBody
    public User getUserByToken(@RequestParam String accessToken){
        User userToken = outlandsFeignClient.getUserByToken(accessToken);
        return userToken;
    }
    @RequestMapping("/test")
    @ResponseBody
    public String getCard(){
        String val = "6621";
        Random random = new Random();
        for (int i = 0; i < 15; i++) {
            val += String.valueOf(random.nextInt(10));
        }
        return val;
    }
    @RequestMapping("/uuid")
    @ResponseBody
    public String getUUid(){
        return UUID.randomUUID().toString();
    }
    @RequestMapping("/phone")
    @ResponseBody
    public String getPhone(){
        String val = "137";
        Random random = new Random();
        for (int i = 0; i < 8; i++) {
            val += String.valueOf(random.nextInt(10));
        }
        return val;
    }
}
