package com.szit.controller;

import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PayeeController extends BaseController {

    //    @RequestMapping(value = "/payee/getpayees",method = RequestMethod.GET)
//    @ResponseBody
//    public DataTableJson getPayeeByPage(@RequestParam(value = "page",required = false) Integer pageNo,
//                                        @RequestParam(value = "limit",required = false) Integer pageSize){
//        return outlandsFeignClient.getPayeeByPage(pageNo,pageSize);
//    }
    @RequestMapping(value = "/card/getpayees", method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeByPayeeId(@RequestParam(value = "id",required = false) Integer payeeId,
                                      @RequestParam(value = "page", required = false) Integer pageNo,
                                      @RequestParam(value = "limit", required = false) Integer pageSize) {
        return outlandsFeignClient.getPayeeCard(payeeId, pageNo, pageSize);
    }
    @RequestMapping(value = "/user/getpayees", method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeByUserId(@RequestParam(value = "id",required = false) Integer userId,
                                          @RequestParam(value = "payee",required = false)Integer payeeId,
                                      @RequestParam(value = "page",required = false) Integer pageNo,
                                      @RequestParam(value = "limit",required = false) Integer pageSize) {
        if(pageSize==null){
            pageSize=10;
        }
        return outlandsFeignClient.getPayeeByUser(userId,payeeId, pageNo, pageSize);
    }
}
