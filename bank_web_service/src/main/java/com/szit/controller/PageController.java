package com.szit.controller;


import com.szit.feign.InsideBankFeignClient;
import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;


@Controller
@RequestMapping("/web")
public class PageController {
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/insideTrans")
    public String insideTran(HttpSession session){
        User user =new User();
        user.setId(242);
        redisTemplate.opsForValue().set("user",user);
        return "insideTrans.html";
    }

    @GetMapping("/insideSweep")
    public String insideSweep(){
        return "insideSweep.html";
    }

    @GetMapping("/SelectCard")
    public String SelectCard() {
        return "SelectCard.html";
    }

    @GetMapping("/activeCollection")
    public String activeCollection() {
        return "activeCollection.html";
    }

    @GetMapping("/beCollection")
    public String beCollection() {
        return "beCollection.html";
    }

}
