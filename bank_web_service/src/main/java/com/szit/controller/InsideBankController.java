package com.szit.controller;

import com.szit.feign.InsideBankFeignClient;
import com.szit.model.bean.Card;
import com.szit.model.bean.Payee;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.User;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.SubmitResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/inside")
public class InsideBankController {
    @Autowired
    private InsideBankFeignClient insideBankFeignClient;
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/getPayee")
    public List<Payee> getPayee(HttpSession session){
        //返回收款人
        User user= (User)redisTemplate.opsForValue().get("user");
        user.setId(242);
        List<Payee> payees=new ArrayList<Payee>();
        if(user!=null){
           payees=  insideBankFeignClient.getPayee(user.getId());
        }
         return payees;
    }

    @GetMapping("/getUserByName")
    public User getUserByName(@RequestParam  String userName){
        if(userName==null){
            return null;
        }
        return  insideBankFeignClient.queryUserByUserName(userName);
    }

    @GetMapping("/getCardByPayeeId")
    public  List<Card> getCardByPayeeId(Integer payeeId){
        if(payeeId==null){
            return null;
        }
        List<Card> cards=new ArrayList<Card>();
        List<Card> returnCards = insideBankFeignClient.getCardByPayId(payeeId);
        //降级
        for (Card card : returnCards) {
            Card c = new Card();
            c.setId(card.getId());
            c.setNo(card.getNo());
            cards.add(c);
        }
        return  cards;
    }

    @PostMapping("/insertActiveCollection")
    public boolean insertActiveCollection(@RequestParam String userName,@RequestParam String remitCardNo,@RequestParam Integer amount){
        if(userName==null || remitCardNo==null || amount==null){
            return false;
        }
        boolean flag=false;
        User user=insideBankFeignClient.queryUserByUserName(userName);
        Card card=insideBankFeignClient.getCardByCardNo(remitCardNo);
        if(user!=null && card!=null){
            TransRecords transRecords=new TransRecords();
            transRecords.setUserId(user.getId());
            transRecords.setReceivCardId(card.getId());
            transRecords.setAmount(new BigDecimal(amount));
            transRecords.setState(2);
            flag=insideBankFeignClient.setTransRecords(transRecords);
        }
        return  flag;
    }
    @GetMapping("/getCardByUserIdAndCardSign")
    public  List<Card> getCardByUserIdAndCardSign(@RequestParam Integer cardSign){
        if(cardSign==null ){
            return null;
        }
        //返回收款人
        User user= (User)redisTemplate.opsForValue().get("user");
        List<Card> cards=new ArrayList<Card>();
        if(user!=null) {
            List<Card> returnCards = insideBankFeignClient.getCardByUserIdAndCardSign(user.getId(), cardSign);
            //降级
            for (Card card : returnCards) {
                Card c = new Card();
                c.setId(card.getId());
                c.setNo(card.getNo());
                cards.add(c);
            }
        }
        return cards;
    }

    @GetMapping("/queryTransrecords")
    public DataTableJson queryTransrecords(){
        User user= (User)redisTemplate.opsForValue().get("user");
        List<TransRecords> transRecords=insideBankFeignClient.selectTransRecords(user.getId(),2);
        for (TransRecords transRecords1:transRecords){
            User user1=insideBankFeignClient.queryUserByCardId(transRecords1.getReceivCardId());
            transRecords1.setCode(user1.getRealName());
        }
        DataTableJson dataTableJson=new DataTableJson();
        dataTableJson.setData(transRecords);
        dataTableJson.setCount(transRecords.size());
        return dataTableJson;
    }
    @GetMapping("/updateFirstCardSign")
    public int updateFirstCardSign(@RequestParam("card1") String card1){
        if(card1==null ){
            return 0;
        }
        int flag=0;
        Card firstCard=insideBankFeignClient.getCardByCardNo(card1);
        User user= (User)redisTemplate.opsForValue().get("user");

        if(firstCard!=null) {
            List<Card> cards = insideBankFeignClient.getCardByUserIdAndCardSign(user.getId(), 1);
            System.out.println(cards.size()+"==");
            if (cards.size() > 0) {
                if (firstCard.getBankid() == cards.get(0).getBankid()) {
                    if (insideBankFeignClient.updateCardSign(1, user.getId(), firstCard.getId())) {
                        if (insideBankFeignClient.updateCardSign(0, user.getId(), cards.get(0).getId())) {
                            flag = 1;
                        }
                    }
                }
            } else {
                if (insideBankFeignClient.updateCardSign(1, user.getId(), firstCard.getId())) {
                    flag = 1;
                }
            }
        }
        return  flag;
        }
    @GetMapping("/updateCardSign")
    public  int updateCardSign(@RequestParam("card3") String card3 ){
        if(card3==null ){
            return 0;
        }
        Card card1=insideBankFeignClient.getCardByCardNo(card3);
        int flag=0;
        User user= (User)redisTemplate.opsForValue().get("user");
        List<Card> cards=insideBankFeignClient.getCardByUserIdAndCardSign(user.getId(),1);
        if(card1.getBankid()==cards.get(0).getBankid()) {
            if (user != null) {
             if(insideBankFeignClient.updateCardSign(2, user.getId(), card1.getId())){
                    flag=1;
                }
            }
        }else{
            flag=-1;
        }
        return  flag;
    }


   @GetMapping("/getCardByUserId")
   public  List<Card> getCardByUserId(){
       User user= (User)redisTemplate.opsForValue().get("user");
       List<Card> cards=new ArrayList<Card>();
       if(user!=null) {
           List<Card> returnCards = insideBankFeignClient.getCardsByUserId(user.getId());
           //降级
           for (Card card : returnCards) {
               Card c = new Card();
               c.setId(card.getId());
               c.setNo(card.getNo());
               cards.add(c);
           }
       }
       return  cards;
   }

    @GetMapping("/delCollection")
    public void delCollection(@RequestParam  Object transId, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        if(transId==null){
            out.print(-1);
            return;
        }
        int resultInfo=-1;
       if(insideBankFeignClient.updateTransRecordsState(Integer.parseInt(transId.toString()),3)){
           resultInfo=1;
       }else {
           resultInfo=0;
       }
        out.print(resultInfo);
    }

    @GetMapping("/doCollection")
    public void doCollection(@RequestParam  Object transId,@RequestParam String password,@RequestParam String remitCardNo, HttpServletResponse response) throws IOException {

        PrintWriter out = response.getWriter();
        if(transId==null ||password==null||remitCardNo==null){
            out.print(-1);
            return ;
        }
        TransRecords transRecords=insideBankFeignClient.getTransRecordsByTransId(Integer.parseInt(transId.toString()));
       Card remitCard=insideBankFeignClient.getCardByCardNo(remitCardNo);
        User user= (User)redisTemplate.opsForValue().get("user");
        int resultInfo=-1;
        if(password.equals(remitCard.getPwd()) && user.getId().equals(transRecords.getUserId())) {
            //交易
            transRecords.setRemitCard(remitCard);
            transRecords.setRemitCardId(remitCard.getId());
            //1转账成功 2系统异常 3余额不足 4单笔转账金额超过当前限额 5当天转账金额超过当前限额 6非行内转账
            resultInfo=insideBankFeignClient.insideTrans(transRecords);
        }
        out.print(resultInfo);
    }

    @PostMapping("/sumitTrans")
    public void sumitTrans(HttpServletRequest request, HttpServletResponse response) throws IOException {


        PrintWriter out = response.getWriter();
        //调用转账接口需要的参数userId、amount、remitCard、receivCard
        String remitCardNo=request.getParameter("remitCardNo");
        BigDecimal amount=new BigDecimal(request.getParameter("amount"));
        String receivCardNo=request.getParameter("receivCardNo");
        String password=request.getParameter("password");
        Integer payeeId=Integer.valueOf(request.getParameter("payeeId"));
        User user= (User)redisTemplate.opsForValue().get("user");
        user.setId(242);
        if(remitCardNo==null ||password==null||remitCardNo==null||amount==null||receivCardNo==null||payeeId==null){
           out.print(-1);
            return ;
        }

       Card remitCard= (Card) insideBankFeignClient.getCardByCardNo(remitCardNo);
       Card receivCard= (Card) insideBankFeignClient.getCardByCardNo(receivCardNo);
        int resultInfo=-1;
       if(password.equals(remitCard.getPwd())) {
           //添加到交易记录
           TransRecords transRecords = new TransRecords();
           transRecords.setReceivCard(receivCard);
           transRecords.setReceivCardId(receivCard.getId());
           transRecords.setRemitCard(remitCard);
           transRecords.setRemitCardId(remitCard.getId());
           transRecords.setAmount(amount);
           transRecords.setUserId(user.getId());
           transRecords.setPayeeId(payeeId);
            transRecords.setState(1);
           //1转账成功 2系统异常 3余额不足 4单笔转账金额超过当前限额 5当天转账金额超过当前限额 6非行内转账
           resultInfo=insideBankFeignClient.insideTrans(transRecords);
           //System.out.println(submitResultInfo.getResultInfo().getMessage());
       }
            out.print(resultInfo);
       System.out.println(resultInfo);
    }
}
