package com.szit.feign;

import com.szit.model.bean.TransRecords;
import org.springframework.stereotype.Component;

@Component
public class InsideFeignClientFallBack implements InsideFeignClient {
    @Override
    public boolean insertTransRecords(TransRecords transRecords) {
        return true;
    }

    @Override
    public int insideTrans(TransRecords transRecords) {
        return 0;
    }
}
