package com.szit.feign;

import com.szit.model.bean.TransRecords;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "inside-bank-service",fallback = InsideFeignClientFallBack.class)
public interface InsideFeignClient {

    /**
     * 生成交易记录
     * @param transRecords
     * @return
     */
    @RequestMapping(value = "/transRecords/insertrecords",method = RequestMethod.POST)
    @ResponseBody
    public boolean insertTransRecords(@RequestBody TransRecords transRecords);

    /**
     * 行内转账
     * @param transRecords
     * @return
     */
    @RequestMapping(value = "/transRecords/insideTrans",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public int insideTrans(@RequestBody  TransRecords transRecords);
}
