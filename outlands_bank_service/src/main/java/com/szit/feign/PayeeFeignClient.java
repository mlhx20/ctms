package com.szit.feign;

import com.szit.model.bean.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "payee-bank-service",fallback = PayeeFeignClientFallBack.class)
public interface PayeeFeignClient {

    @RequestMapping(value = "/getusers",method = RequestMethod.GET)
    public List<User> getUsersByPayee();
}
