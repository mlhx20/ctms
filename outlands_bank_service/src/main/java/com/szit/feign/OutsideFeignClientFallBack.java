package com.szit.feign;

import org.springframework.stereotype.Component;

@Component
public class OutsideFeignClientFallBack implements OutsideFeignClient {
    @Override
    public Integer bankTransfer(String payerCardNo, String payeeCardNo, double amount, Integer id) {
        return 0;
    }
}
