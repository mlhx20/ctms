package com.szit.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "outside-bank-service",fallback = OutsideFeignClientFallBack.class)
public interface OutsideFeignClient {

    @RequestMapping(value = "/enja/transfer",method = RequestMethod.POST)
    @ResponseBody
    public Integer bankTransfer(@RequestParam("payerCardNo") String payerCardNo
            ,@RequestParam("payeeCardNo")String payeeCardNo
            ,@RequestParam("amount")double amount
            ,@RequestParam("id")Integer id);
}
