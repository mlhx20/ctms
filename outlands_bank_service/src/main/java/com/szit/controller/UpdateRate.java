package com.szit.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.time.LocalDate;
import java.time.LocalDateTime;

//@Configuration
//@EnableScheduling
public class UpdateRate implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addCronTask(()-> System.out.println(LocalDateTime.now()),
                "0/1 * * * * ?");
    }
}
