package com.szit.controller;

import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user/payee")
public class UserPayeeController extends BaseController {

    @RequestMapping(value = "/getpayees",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeByUser(@RequestParam(value = "id",required = false) Integer userId,
                                        @RequestParam(value = "payee",required = false)Integer payeeId,
                                        @RequestParam(value = "page",required = false) Integer pageNo,
                                        @RequestParam(value = "limit",required = false) Integer pageSize){
        return userPayeeService.findByUserId(userId,payeeId, pageNo, pageSize);
    }
}
