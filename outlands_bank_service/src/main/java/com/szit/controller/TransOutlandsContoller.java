package com.szit.controller;

import com.szit.model.bean.*;
import com.szit.model.bean.Currency;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import io.micrometer.core.instrument.noop.NoopDistributionSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * 跨境转账
 * 用户登录->查询所有银行卡->选择收款人->输入转账金额->提交转账
 */
@RestController
@RequestMapping("/trans")
public class TransOutlandsContoller extends BaseController {

    @RequestMapping(value = "/trans",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public String trans(@RequestBody TransRecords transRecords){
        String msg = "不是跨境转账！";
        Card remitCard = cardService.findObjById(transRecords.getRemitCardId());
        Card receivCard = cardService.findObjById(transRecords.getReceivCardId());
        transRecords.setCurId(remitCard.getCurrencyid());
        transRecords.setCode(Long.toString(new Date().getTime()));
        if(isCrossBorder(remitCard,receivCard)){
            try{
                //获取转换币种后金额
                BigDecimal otherCurrency = getOtherCurrency(transRecords.getAmount(),remitCard.getCurrency(),receivCard.getCurrency());
                //转换为人民币
                BigDecimal cny = transRecords.getAmount().multiply(remitCard.getCurrency().getRate()).divide(new BigDecimal(100));
                //获取转账方 判断转账方 是否超过限额、 判断转账账户是否存在异常转账
                Integer remitId = transRecords.getUserId();
                Map limitMap = new HashMap();
                limitMap.put("userId", remitId);
                List<Limit> userLimit = limitService.findObjs(limitMap);
                msg = "交易额度不足！";
                if (userLimit.size()>0 && transRecords.getAmount().compareTo(userLimit.get(0).getTranslimit()) == -1){  //-1小于  0等于  1大于  转账金额小于单笔限额
                    //获取日转账金额 人民币
                    BigDecimal dayAmount = transRecordsService.queryDayAmount(remitId);
                    if(userLimit.get(0).getDaylmt().compareTo(cny.add(dayAmount))==1){  //当日限额 大于 转账金额 + 当日已交易金额
                        //判断转账卡号 和姓名是否匹配
                        if(cardService.cardMatch(transRecords.getRemitCardId(),transRecords.getUserId())){
                            //判断密码是否正确
                            if(transRecords.getRemitCard().getPwd().equals(remitCard.getPwd())){
                                //减少转账方卡余额
                                boolean remitYes = cardService.tansCard(transRecords.getRemitCardId(),transRecords.getAmount(),false);
                                if(remitYes){
                                    //判断收款卡号 与收款人id是否匹配
                                    if(cardService.cardMatchPayee(transRecords.getReceivCardId(),transRecords.getPayeeId())){
                                        //增加收款方卡余额
                                        boolean receiveStatue = cardService.tansCard(transRecords.getReceivCardId(),otherCurrency,true);
                                        if(receiveStatue){
                                            transRecords.setState(1);
                                            boolean boo = insideFeignClient.insertTransRecords(transRecords);
                                            if(!boo){
                                                throw new RuntimeException();
                                            }
                                            msg = "交易成功";
                                        }
                                    }
                                }else {
                                    msg = "余额不足！";
                                }
                            }else {
                                msg = "密码错误！";
                            }
                        }else {
                            msg = "卡不存在！";
                        }
                    }
                }
            }catch (Exception e){
                transRecords.setState(2);
                msg = "交易失败！";
            }
        }
        return msg;
    }

    /**
     * 判断是否是跨境转账
     * @param remitCard
     * @param receivCard
     * @return
     */
    public boolean isCrossBorder(Card remitCard,Card receivCard){
        boolean result = false;
//        //根据付款卡id，获取转账银行国家 地址
//        Card remitCard = cardService.findObjById(remitCardId);
//        //根据收款卡id，获取转账银行国家 地址
//        Card receivCard = cardService.findObjById(receivCardId);
        //判断不是同一个国家 或者 不是同一个地区的银行（例： 中国 和俄罗斯，中国大陆 和中国香港）
        if(!remitCard.getBank().getCountry().equals(receivCard.getBank().getCountry()) || !remitCard.getBank().getAddress().equals(receivCard.getBank().getAddress())){
            result = true;
        }
        return result;
    }

    /**
     * 汇率转换
     * @param amount 转换前金额
     * @param remitCurrency 转出币种
     * @param receivCurrency 接收币种
     * @return
     */
    public BigDecimal getOtherCurrency(BigDecimal amount,Currency remitCurrency,Currency receivCurrency){
        //转换后的币额 = (转换前的币额 * 目标币种的汇率）/原币种汇率
        BigDecimal v = amount.multiply(remitCurrency.getRate());
        BigDecimal r = v.divide(receivCurrency.getRate(),6,BigDecimal.ROUND_HALF_DOWN);
        return r;
    }
    /**
     * 判断是否是行内转账
     * @param remitCard
     * @param receivCard
     * @return
     */
    public boolean isInside(Card remitCard,Card receivCard){
        boolean result = false;
//        //根据付款卡id，获取转账银行国家 地址
//        Card remitCard = cardService.findObjById(remitCardId);
//        //根据收款卡id，获取转账银行国家 地址
//        Card receivCard = cardService.findObjById(receivCardId);
        //判断是同一个银行转账，不包括跨境的银行
        if(remitCard.getBankid().equals(receivCard.getBankid())){
            result = true;
        }
        return result;
    }
    /**
     * 判断是否是跨行转账
     * @param remitCard
     * @param receivCard
     * @return
     */
    public boolean isOutside(Card remitCard,Card receivCard){
        boolean result = false;
//        //根据付款卡id，获取转账银行国家 地址
//        Card remitCard = cardService.findObjById(remitCardId);
//        //根据收款卡id，获取转账银行国家 地址
//        Card receivCard = cardService.findObjById(receivCardId);
        //判断不是同一个国家 或者 不是同一个地区的银行（例： 中国 和俄罗斯，中国大陆 和中国香港）
        if(!remitCard.getBankid().equals(receivCard.getBankid()) &&
                remitCard.getBank().getCountry().equals(receivCard.getBank().getCountry()) &&
        remitCard.getBank().getAddress().equals(receivCard.getBank().getAddress())){
            result = true;
        }
        return result;
    }
    /**
     * 判断批量转账是否是同类型的
     */
    public Integer isSameBatch(List<TransRecords> transRecordsList){
        Integer num = transRecordsList.size();
        Integer sum = num;
        Integer result = 0;
        if(num<=BATCH_TRANS_MAX_COUNT){ //批量转账 一次最多50笔
            for (TransRecords transRecords:transRecordsList){
                Card remitCard = cardService.findObjById(transRecords.getRemitCardId());
                Card receivCard = cardService.findObjById(transRecords.getReceivCardId());
                if(isInside(remitCard,receivCard)){  //是行内转账
                    num = num+1;
                }else if(isOutside(remitCard,receivCard)){  //是跨行转账
                    num = num+2;
                }else if(isCrossBorder(remitCard,receivCard)){  //是跨境转账
                    num = num+3;
                }
            }
            if(num==(sum*2)){
                //都是行内转账
                result =1;
            }else if(num==(sum*3)){
                //都是跨行转账
                result =2;
            }else if(num==(sum*4)){
                //都是跨境转账
                result = 3;
            }else {
                result =4;
            }
        }
        return result;
    }
    /**
     * 批量转账
     */
    @RequestMapping(value = "/batch",method = RequestMethod.POST)
    @ResponseBody
    public String batchTrans(@RequestBody List<TransRecords> transRecordsList){
        ResultInfo resultInfo = new ResultInfo();
        Integer batchType = isSameBatch(transRecordsList);
        String msg = "成功";
        switch (batchType){
            case 0:
                msg="转账次数不能超过50次！";
                break;
            case 1:
                //调用批量行内转账
                boolean b1 = batchInsideLands(transRecordsList);
                if(!b1){
                    msg = "转账失败！";
                }
                break;
            case 2:
                //调用批量跨行转账
                boolean b2 = batchOutsideLands(transRecordsList);
                if(!b2){
                    msg = "转账失败！";
                }
                break;
            case 3:
                //调用批量跨境转账
                 boolean boo = batchOutLands(transRecordsList);
                 if(!boo){
                     msg = "转账失败！";
                 }
                break;
            case 4:
                msg = "批量转账一次只能选择一种转账方式！";
                break;
            default:
                msg = "转账失败！";
        }
        resultInfo.setMessage(msg);
        System.out.println("msg:"+msg);
        return msg;
    }
    /**
     * 跨境批量转账
     */
    public boolean batchOutLands(List<TransRecords> transRecordsList){
        boolean result = true;
        try{
            for(TransRecords transRecords:transRecordsList){
                String res = trans(transRecords);
                if(!res.equals("交易成功")){
                    throw new RuntimeException();
                }
            }
        }catch (Exception e){
            result = false;
//            throw new RuntimeException();
        }
        return result;
    }
    /**
     * 行内 批量转账
     */
    public boolean batchInsideLands(List<TransRecords> transRecordsList){
        boolean result = false;
        try{
            for(TransRecords transRecords:transRecordsList){
                int flag = insideFeignClient.insideTrans(transRecords);
                if (flag==1){
                    result = true;
                }
            }
        }catch (Exception e){
            result = false;
//            throw new RuntimeException();
        }
        return result;
    }
    /**
     * 跨行 批量转账
     */
    public boolean batchOutsideLands(List<TransRecords> transRecordsList){
        boolean result = false;
        try{
            for(TransRecords transRecords:transRecordsList){
                Card remitCard = cardService.findObjById(transRecords.getRemitCardId());
                Card receivCard = cardService.findObjById(transRecords.getReceivCardId());
                Integer flag = outsideFeignClient.bankTransfer(remitCard.getNo(),
                        receivCard.getNo(),
                        transRecords.getAmount().doubleValue(), transRecords.getUserId());
                if(flag==1){
                    result = true;
                }
            }
        }catch (Exception e){
            result = false;
//            throw new RuntimeException();
        }
        return result;
    }

}
