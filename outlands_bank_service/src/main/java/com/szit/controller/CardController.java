package com.szit.controller;

import com.szit.model.service.CardService;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/card")
public class CardController extends BaseController {


    @RequestMapping(value = "/getcards",method = RequestMethod.GET)
    public DataTableJson getCardsByUserId(Integer userId, @RequestParam(value = "page",required = false) Integer pageNo,
                                          @RequestParam(value = "limit",required = false) Integer pageSize){
        return cardService.findByUserId(userId,pageNo,pageSize);
    }

}
