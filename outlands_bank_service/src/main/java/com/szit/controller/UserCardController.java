package com.szit.controller;

import com.szit.model.bean.User;
import com.szit.model.bean.UserCard;
import com.szit.model.service.UserCardService;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user/card")
public class UserCardController extends BaseController {

    @RequestMapping(value = "/getcards",method = RequestMethod.GET)
    public DataTableJson getCardByUserId(@RequestParam(value = "userid",required = false) Integer userId,
                                         @RequestParam(value = "page",required = false) Integer pageNo,
                                         @RequestParam(value = "limit",required = false) Integer pageSize){
        return userCardService.findByUserId(userId, pageNo, pageSize);
    }
    @RequestMapping(value = "/existcard",method = RequestMethod.GET)
    public boolean existUserIdCardId(@RequestParam(value = "userid",required = true)Integer userId,
                                     @RequestParam(value = "cardid",required = true)Integer cardId){
        return userCardService.existUserCardId(userId,cardId);
    }
}
