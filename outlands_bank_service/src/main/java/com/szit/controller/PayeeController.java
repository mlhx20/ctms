package com.szit.controller;

import com.szit.model.service.PayeeService;
import com.szit.service.impl.PayeeServiceImpl;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/payee")
public class PayeeController extends BaseController {

    @RequestMapping(value = "/getpage",method = RequestMethod.GET)
    public DataTableJson findByPage(@RequestParam(value = "page",required = false) Integer pageNo,@RequestParam(value = "limit",required = false) Integer pageSize){
        return payeeService.getPayeeByPage(pageNo,pageSize);
    }
}
