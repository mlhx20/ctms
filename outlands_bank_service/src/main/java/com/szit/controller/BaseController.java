package com.szit.controller;

import com.szit.feign.InsideFeignClient;
import com.szit.feign.OutsideFeignClient;
import com.szit.model.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

public class BaseController {
    @Autowired
    protected CardService cardService;
    @Autowired
    protected PayeeService payeeService;
    @Autowired
    protected PayeeCardService payeeCardService;
    @Autowired
    protected UserCardService userCardService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected UserPayeeService userPayeeService;
    @Autowired
    protected LimitService limitService;
    @Autowired
    protected TransRecordsService transRecordsService;
    //    @Autowired
//    private PayeeFeignClient payeeFeignClient;
    @Autowired
    protected InsideFeignClient insideFeignClient;
    @Autowired
    protected OutsideFeignClient outsideFeignClient;
    @Autowired
    protected RedisTemplate<String,Object> redisTemplate;

    //批量转账 一次最多50笔
    public static final Integer BATCH_TRANS_MAX_COUNT = 50;
}
