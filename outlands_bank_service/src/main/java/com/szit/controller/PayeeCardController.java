package com.szit.controller;

import com.szit.model.bean.PayeeCard;
import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/payee/card")
public class PayeeCardController extends BaseController{

    @RequestMapping(value = "/getpayees",method = RequestMethod.GET)
    @ResponseBody
    public DataTableJson getPayeeCard(@RequestParam(value = "id",required = false) Integer payeeId,
                                      @RequestParam(value = "page",required = false) Integer pageNo,
                                      @RequestParam(value = "limit",required = false) Integer pageSize){
        return payeeCardService.findByPayeeId(payeeId, pageNo, pageSize);
    }

}
