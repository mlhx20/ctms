package com.szit.controller;

import com.alibaba.fastjson.JSON;
import com.szit.feign.PayeeFeignClient;
import com.szit.model.bean.User;
import com.szit.model.service.UserService;
import com.szit.tool.RSAKeyPair;
import com.szit.tool.TokenBean;
import com.szit.tool.result.DataTableJson;
import com.szit.tool.result.ResultInfo;
import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpSession;
import java.security.KeyPair;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController{


    @RequestMapping(value = "/getusers",method = RequestMethod.POST)
    public List<User> getAllUser(@RequestBody User user){
        System.out.println("进入getusers");
        return userService.findAll();
    }
//    @RequestMapping(value = "/getusersby",method = RequestMethod.GET)
//    public List<User> getAllUserByPayee(){
//        List<User> users = payeeFeignClient.getUsersByPayee();
//        return users;
//    }
//
//    @RequestMapping("/getOrderData")
//    public DataTableJson getData() {
//        return userService.getData();
//    }

    /**
     * 登录
     * @param tokenBean 登录实体对象 用户名，密码
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public boolean login(@RequestBody TokenBean tokenBean) throws Exception {
        boolean result = false;
        if(tokenBean != null && tokenBean.getUsername() != null){
            Map paraMap = new HashMap();
            paraMap.put("name",tokenBean.getUsername());
            List<User> users = userService.findObjs(paraMap);
            if(users.size() !=0 ){
                for (User u:users) {
                    //解密后明文
                    String decryptData = RSAKeyPair.decrypt(u.getPwd(), RSAKeyPair.getPrivateKey(u.getPrivateKey()));
                    if(tokenBean.getPassword().equals(decryptData)){
//                        System.out.println("Client_id:"+tokenBean.getClient_id());
                        //删除该用户的所有token
                        Set<String> keys = redisTemplate.keys("token_"+u.getId()+"_*");
                        redisTemplate.delete(keys);
                        //token保存到redis 失效时间为30分钟
                        redisTemplate.opsForValue().set("token_"+u.getId()+"_"+tokenBean.getClient_id(), tokenBean,30, TimeUnit.MINUTES);
                        result = true;
                    }
                }
            }
        }
        return result;
    }
    @RequestMapping(value = "/loginout",method = RequestMethod.POST)
    @ResponseBody
    public boolean loginOut(@RequestParam String access){
        //删除该用户的所有token
        Set<String> keys = redisTemplate.keys("*"+access);
        redisTemplate.delete(keys);
        return true;
    }
    @RequestMapping(value = "/getuserbytoken",method = RequestMethod.GET)
    @ResponseBody
    public User getUserByToken(@RequestParam String accessToken){
//        System.out.println(accessToken);
        Set<String> keys = redisTemplate.keys("*"+accessToken);
        User userToken = new User();
        for(String k:keys){
            Integer userId = Integer.parseInt(k.substring(6,k.lastIndexOf("_")));
//            System.out.println("userId:"+userId);
            userToken = userService.findObjById(userId);
        }
        return userToken;
    }
    /**
     * 用户注册
     * @param user 用户对象  需要身份证号
     * @param pwd2 确认密码
     * @return
     */
    @RequestMapping(value = "/reg",method = RequestMethod.POST)
    @Transactional(rollbackFor=Exception.class)
    public boolean register(@RequestBody User user, @RequestParam("pwd2")String pwd2) {
        boolean result = false;
        try{
            if(user.getIdCard()!=null && !userService.existUser(user) ){
                if(user.getPwd().equals(pwd2)){
                    //生成私钥和公钥，密码加密
                    KeyPair keyPair = RSAKeyPair.getKeyPair();
                    String privateKey = new String(Base64.encodeBase64(keyPair.getPrivate().getEncoded()));
                    String publicKey = new String(Base64.encodeBase64(keyPair.getPublic().getEncoded()));
                    String encryptData = RSAKeyPair.encrypt(user.getPwd(), RSAKeyPair.getPublicKey(publicKey));
                    user.setPwd(encryptData);
                    user.setCode(UUID.randomUUID().toString());
                    user.setPrivateKey(privateKey);
                    user.setPublicKey(publicKey);
                    user.setOpendate(new Date());
                    userService.insert(user);
                    result = true;
                }
            }
        }catch (Exception e){
            throw new RuntimeException();
        }
        return result;
    }
    /**
     * 判断用户是否已存在
     */
    @RequestMapping(value = "/exist",method = RequestMethod.POST)
    public boolean existUser(@RequestBody User user){
        boolean result = true;
        if(user.getIdCard()!=null && !userService.existUser(user)){
            result = false;
        }
        return result;
    }
    /**
     * 判断用户信息是否已存在
     */
    @RequestMapping(value = "/findusers",method = RequestMethod.POST)
    public List<User> findObjs(@RequestBody Map paraMap){
        List<User> objs = userService.findObjs(paraMap);
        return objs;
    }

}
