package com.szit.service.impl;

import com.szit.model.bean.PayeeCard;
import com.szit.model.service.PayeeCardService;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PayeeCardServiceImpl extends BaseServiceImpl<PayeeCard> implements PayeeCardService {
    @Override
    public DataTableJson findByPayeeId(Integer payeeId, Integer pageNo, Integer pageSize) {
        Map paraMap = new HashMap();
        paraMap.put("payeeId",payeeId);
        paraMap.put("pageNo",pageNo);
        paraMap.put("pageSize",pageSize);
        PageIndexer pageIndexer = getPageIndexer(paraMap);
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setCount(pageIndexer.getTotalCount());
        dataTableJson.setData(pageIndexer.getItems());
        return dataTableJson;
    }
}
