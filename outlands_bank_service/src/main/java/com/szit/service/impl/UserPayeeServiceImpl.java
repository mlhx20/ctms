package com.szit.service.impl;

import com.szit.model.bean.User;
import com.szit.model.bean.UserPayee;
import com.szit.model.dao.UserPayeeDao;
import com.szit.model.service.UserPayeeService;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserPayeeServiceImpl extends BaseServiceImpl<UserPayee> implements UserPayeeService {

    @Autowired
    protected UserPayeeDao userPayeeDao;
    @Override
    public DataTableJson findByUserId(Integer userId,Integer payeeId, Integer pageNo, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageNo", pageNo);
        map.put("pageSize", pageSize);
        map.put("userId",userId);
        map.put("payeeId",payeeId);
        PageIndexer pageIndexer = getPageIndexer(map);
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setCount(pageIndexer.getTotalCount());
        dataTableJson.setData(pageIndexer.getItems());
        return dataTableJson;
    }
    public PageIndexer getPageIndexer(Map paraMap) {
        int pageNo; // 当前页号
        int pageSize; // 每页大小
        int currLine; // 当前行号
        int pageLine; // 查询行号
        long totalCount; // 总记录数

        if (paraMap == null) {
            paraMap = new HashMap<String, Object>();
        }

        // 如果没有传入当前页号，默认为1
        if ((paraMap.get("pageNo") == null) || ((Integer) paraMap.get("pageNo") < 1)) {
            pageNo = 1;
        } else {
            pageNo = (Integer) paraMap.get("pageNo");
        }

        // 如果没有传入每页大小，默认为3
        if (paraMap.get("pageSize") == null || (Integer) paraMap.get("pageSize") < 1) {
            pageSize = 3;
        } else {
            pageSize = (Integer) paraMap.get("pageSize");
        }
        pageLine = pageSize;

        // 行号
        currLine = (pageNo - 1) * pageSize;

        paraMap.put("currLine", currLine);
        paraMap.put("pageLine", pageLine);
        totalCount = userPayeeDao.findPayeeCardCount(paraMap);

        // 查询列表(无条件/有条件)
        List<UserPayee> items = userPayeeDao.findPayeeCard(paraMap);
        // 本页列表删除，返回上一页
        if (totalCount > 0 && items.size() == 0 && pageNo > 1) {
            pageNo = pageNo - 1;
            currLine = (pageNo - 1) * pageSize;
            paraMap.put("currLine", currLine);
            items = userPayeeDao.findPayeeCard(paraMap);
        }

        return new PageIndexer(totalCount, pageNo, items, pageSize);
    }
}
