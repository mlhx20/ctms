package com.szit.service.impl;

import com.szit.model.bean.User;
import com.szit.model.dao.UserDao;
import com.szit.model.service.UserService;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.util.calendar.BaseCalendar;
import sun.util.calendar.LocalGregorianCalendar;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Override
    public List<User> findAll() {
        return userDao.queryAll();
    }



    @Override
    public DataTableJson getData() {
        List<User> list = new ArrayList<>();
        list.add(new User(1, "123", "张三", "123456", "张三丰", "425123198712345678", new Date(), "东方",
                452358, "13856457895", new Date(), 1, 1, 1,"",""));
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setCount(2);
        dataTableJson.setData(list);
        return dataTableJson;
    }

    @Override
    public boolean existUser(User user) {
        Map paraMap = new HashMap();
        paraMap.put("idCard", user.getIdCard());
        return userDao.findObjs(paraMap).size() >0 ?true:false;
    }
}
