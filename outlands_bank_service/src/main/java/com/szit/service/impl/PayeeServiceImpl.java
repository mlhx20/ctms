package com.szit.service.impl;

import com.szit.model.bean.Payee;
import com.szit.model.service.PayeeService;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PayeeServiceImpl extends BaseServiceImpl<Payee> implements PayeeService {
    @Override
    public DataTableJson getPayeeByPage(Integer pageNo, Integer pageSize) {
        Map paraMap = new HashMap();
        paraMap.put("pageNo",pageNo);
        paraMap.put("pageSize",pageSize);
        PageIndexer pageIndexer = getPageIndexer(paraMap);
        DataTableJson dataTableJson = DataTableJson.success();
        dataTableJson.setData(pageIndexer.getItems());
        dataTableJson.setCount(pageIndexer.getTotalCount());
        return dataTableJson;
    }

}
