package com.szit.service.impl;

import com.szit.model.bean.Limit;
import com.szit.model.service.LimitService;
import org.springframework.stereotype.Service;

@Service
public class LimitServiceImpl extends BaseServiceImpl<Limit> implements LimitService {
}
