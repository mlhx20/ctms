package com.szit.service.impl;

import com.szit.model.bean.UserCard;
import com.szit.model.dao.UserCardDao;
import com.szit.model.service.UserCardService;
import com.szit.tool.PageIndexer;
import com.szit.tool.result.DataTableJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserCardServiceImpl extends BaseServiceImpl<UserCard> implements UserCardService {

    @Override
    public DataTableJson findByUserId(Integer userId,Integer pageNo, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageNo", pageNo);
        map.put("pageSize", pageSize);
        map.put("userId",userId);
        PageIndexer pageIndexer = getPageIndexer(map);
        DataTableJson dataTableJson = new DataTableJson();
        dataTableJson.setCount(pageIndexer.getTotalCount());
        dataTableJson.setData(pageIndexer.getItems());
        return dataTableJson;
    }

    @Override
    public boolean existUserCardId(Integer userId, Integer cardId) {
        Map paraMap = new HashMap();
        paraMap.put("userId",userId);
        paraMap.put("cardId",cardId);
        List<UserCard> userCardList = userCardDao.findObjs(paraMap);
        return userCardList.size()>0?true:false;
    }
}
