package com.szit.service.impl;

import com.szit.model.bean.*;
import com.szit.model.service.CardService;
import com.szit.tool.exception.ExceptionResultInfo;
import com.szit.tool.result.DataTableJson;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.cert.CertPathBuilderResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CardServiceImpl extends BaseServiceImpl<Card> implements CardService {
    @Override
    public DataTableJson findByUserId(Integer userId,Integer pageNo, Integer pageSize) {
        Map paraMap = new HashMap();
        paraMap.put("pageNo",pageNo);
        paraMap.put("pageSize",pageSize);
        paraMap.put("userId",userId);
//        int pageNo; // 当前页号
//        int pageSize; // 每页大小
        int currLine; // 当前行号
        int pageLine; // 查询行号
        long totalCount; // 总记录数
        // 如果没有传入当前页号，默认为1
        if ((pageNo == null) || ( pageNo < 1)) {
            pageNo = 1;
        }
        // 如果没有传入每页大小，默认为3
        if (pageSize == null || pageSize < 1) {
            pageSize = 3;
        }
        pageLine = pageSize;

        // 行号
        currLine = (pageNo - 1) * pageSize;

        paraMap.put("currLine", currLine);
        paraMap.put("pageLine", pageLine);
        totalCount = cardDao.queryPageCount(paraMap);

        // 查询列表(无条件/有条件)
        List<Card> items = cardDao.queryPageList(paraMap);
        // 本页列表删除，返回上一页
        if (totalCount > 0 && items.size() == 0 && pageNo > 1) {
            pageNo = pageNo - 1;
            currLine = (pageNo - 1) * pageSize;
            paraMap.put("currLine", currLine);
            items = cardDao.queryPageList(paraMap);
        }
        DataTableJson dataTableJson = DataTableJson.success();
        dataTableJson.setData(items);
        dataTableJson.setCount(totalCount);
        return dataTableJson;
    }

    @Override
    public boolean tansCard(Integer cardId, BigDecimal amount,boolean isReceive) {
        boolean result = false;
        try{
            Card transCard = cardDao.findObjById(cardId);
            if(isReceive){
                transCard.setBalance(transCard.getBalance().add(amount));//转入方银行卡 增加余额
                cardDao.update(transCard);
                result = true;
            }else {
                if(transCard.getBalance().compareTo(amount)==1 || transCard.getBalance().compareTo(amount)==0){
                    transCard.setBalance(transCard.getBalance().subtract(amount));//转出方银行卡 减少余额
                    cardDao.update(transCard);
                    result = true;
                }
            }
        }catch (Exception e){
            throw new RuntimeException();
        }
        return result;
    }

    @Override
    public boolean cardMatch(String cardNumber, String name){
        boolean result = false;
        try{
            Map paraMap = new HashMap();
            paraMap.put("no", cardNumber);
            Card transCard = cardDao.findObjs(paraMap).get(0);
            paraMap.put("cardId",transCard.getId());
            UserCard userCard = userCardDao.findObjs(paraMap).get(0);
            Integer userId = userCard.getUserId();
            User user = userDao.queryOne(userId);
            if(user.getName().equals(name)){
                result =true;
            }
        }catch (Exception e){
            throw new RuntimeException();
        }
        return result;
    }

    @Override
    public boolean cardMatch(Integer cardId, Integer userId) {
        boolean result = false;
        Card card = cardDao.findObjById(cardId);
        User user = userDao.findObjById(userId);
        Map paraMap = new HashMap();
        paraMap.put("userId",userId);
        paraMap.put("cardId",cardId);
        UserCard userCard = userCardDao.findObjs(paraMap).get(0);
        if(card!=null && user!=null && userCard.getCardId().equals(cardId) && userCard.getUserId().equals(userId)){
            result = true;
        }
        return result;
    }

    @Override
    public boolean cardMatchPayee(Integer cardId, Integer payeeId) {
        boolean result = false;
        Card card = cardDao.findObjById(cardId);
        Payee payee = payeeDao.findObjById(payeeId);
        Map paraMap = new HashMap();
        paraMap.put("payeeId",payeeId);
        paraMap.put("cardId",cardId);
        PayeeCard payeeCard = payeeCardDao.findObjs(paraMap).get(0);
        if(card!=null && payee!=null && payeeCard.getCardId().equals(cardId) && payeeCard.getPayeeId().equals(payeeId)){
            result = true;
        }
        return result;
    }

}
