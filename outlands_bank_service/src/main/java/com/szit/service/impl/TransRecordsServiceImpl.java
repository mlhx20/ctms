package com.szit.service.impl;

import com.szit.model.bean.TransRecords;
import com.szit.model.service.TransRecordsService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class TransRecordsServiceImpl extends BaseServiceImpl<TransRecords> implements TransRecordsService {
    @Override
    public BigDecimal queryDayAmount(Integer userId) {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Date fromTime = new Date();
//        Date endTime = new Date();
//        Calendar calendar = new GregorianCalendar();
//        calendar.setTime(endTime);
//        calendar.add(calendar.DATE,1); //把日期往后增加一天
//        endTime  =calendar.getTime();
//        System.out.println(fromTime);
//        System.out.println(endTime);
        //获取转换汇率后，日转账金额，结果是人民币
        double v = transRecordsDao.queryDayAmount(userId);
        return new BigDecimal(v);
    }

    @Override
    public long queryDayCount(Integer userId) {
        Date fromTime = new Date();
        Date endTime = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(endTime);
        calendar.add(calendar.DATE,1); //把日期往后增加一天
        endTime  =calendar.getTime();
        return transRecordsDao.queryDayCount(userId);
    }
}
