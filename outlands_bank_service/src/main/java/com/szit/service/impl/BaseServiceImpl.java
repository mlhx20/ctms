package com.szit.service.impl;

import com.szit.model.dao.*;
import com.szit.model.service.BaseService;
import com.szit.tool.PageIndexer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zenz.
 */
public class BaseServiceImpl<T> implements BaseService<T> {

	@Autowired
	protected UserDao userDao;
	@Autowired
	protected UserCardDao userCardDao;
	@Autowired
	protected CardDao cardDao;
	@Autowired
	protected PayeeDao payeeDao;
	@Autowired
	protected PayeeCardDao payeeCardDao;
	@Autowired
	protected UserPayeeDao userPayeeDao;
	@Autowired
	protected LimitDao limitDao;
	@Autowired
	protected TransRecordsDao transRecordsDao;

	protected BaseDao<T> baseDao;

	@PostConstruct // 在构造方法后，初化前执行
	private void initBaseMapper() throws Exception {
		// 完成以下逻辑，需要对研发本身进行命名与使用规范
		// this关键字指对象本身，这里指的是调用此方法的实现类（子类）
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		// 获取第一个参数的class
		@SuppressWarnings("rawtypes")
		Class clazz = (Class) type.getActualTypeArguments()[0];

		// 转化为属性名（相关的Mapper子类的引用名）Supplier supplierMapper
		String localField = clazz.getSimpleName().substring(0, 1).toLowerCase() + clazz.getSimpleName().substring(1)
				+ "Dao";

		// getDeclaredField:可以使用于包括私有、默认、受保护、公共字段，但不包括继承的字段
		Field field = this.getClass().getSuperclass().getDeclaredField(localField);
		Field baseField = this.getClass().getSuperclass().getDeclaredField("baseDao");

		// field.get(this)获取当前this的field字段的值。例如：Supplier对象中，baseMapper所指向的对象为其子类型SupplierMapper对象，子类型对象已被spring实例化于容器中
		baseField.set(this, field.get(this));
	}

	@Override
	public void insert(T entity) {
		baseDao.insert(entity);
	}

	@Override
	public void update(T entity) {
		baseDao.update(entity);
	}

	@Override
	public void delete(Serializable id) {
		baseDao.delete(id);
	}

	@Override
	public void deletes(Serializable[] ids) {
		baseDao.deletes(ids);
	}

	@Override
	public List<T> findObjs(Map paraMap) {
		return baseDao.findObjs(paraMap);
	}

	@Override
	public PageIndexer getPageIndexer(Map paraMap) {
		int pageNo; // 当前页号
		int pageSize; // 每页大小
		int currLine; // 当前行号
		int pageLine; // 查询行号
		long totalCount; // 总记录数

		if (paraMap == null) {
			paraMap = new HashMap<String, Object>();
		}

		// 如果没有传入当前页号，默认为1
		if ((paraMap.get("pageNo") == null) || ((Integer) paraMap.get("pageNo") < 1)) {
			pageNo = 1;
		} else {
			pageNo = (Integer) paraMap.get("pageNo");
		}

		// 如果没有传入每页大小，默认为10
		if (paraMap.get("pageSize") == null || (Integer) paraMap.get("pageSize") < 1) {
			pageSize = 10;
		} else {
			pageSize = (Integer) paraMap.get("pageSize");
		}
		pageLine = pageSize;

		// 行号
		currLine = (pageNo - 1) * pageSize;

		paraMap.put("currLine", currLine);
		paraMap.put("pageLine", pageLine);
		totalCount = baseDao.findPageCount(paraMap);

		// 查询列表(无条件/有条件)
		List<T> items = baseDao.findPageList(paraMap);
		// 本页列表删除，返回上一页
		if (totalCount > 0 && items.size() == 0 && pageNo > 1) {
			pageNo = pageNo - 1;
			currLine = (pageNo - 1) * pageSize;
			paraMap.put("currLine", currLine);
			items = baseDao.findPageList(paraMap);
		}

		return new PageIndexer(totalCount, pageNo, items, pageSize);
	}

	@Override
	public T findObjById(Serializable id) {
		return baseDao.findObjById(id);
	}

}
