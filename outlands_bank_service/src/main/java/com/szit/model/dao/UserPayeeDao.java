package com.szit.model.dao;

import com.szit.model.bean.UserPayee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserPayeeDao extends BaseDao<UserPayee>{
    List<UserPayee> findPayeeCard(Map paraMap);
    long findPayeeCardCount(Map paraMap);
}
