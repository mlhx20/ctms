package com.szit.model.dao;

import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Charles.
 */
public interface BaseDao<T> {
    /**
     * 新增对象
     *
     * @param entity 新增的对象
     */
    void insert(T entity);

    /**
     * 更新对象
     *
     * @param entity 更新的对象
     */
    void update(T entity);

    /**
     * 根据主键删除对象
     *
     * @param id 对象主键
     */
    void delete(@Param("id") Serializable id);

    /**
     * 批量删除对象
     *
     * @param ids 对象主键
     */
    void deletes(Serializable[] ids);

    /**
     * 根据主键查询对象
     *
     * @param id 对象主键
     * @return 查找到的对象
     */
    T findObjById(@Param("id") Serializable id);

    /**
     * 查询对象列表
     *
     * @return 查到对象列表
     */
    List<T> findObjs(Map paraMap);

    /**
     * 分页查询总记录数
     *
     * @param paraMap 查询参数
     * @return 总记录数
     */
    long findPageCount(Map paraMap);

    /**
     * 分页查询列表
     *
     * @param paraMap 查询参数
     * @return 对象列表
     */
    List<T> findPageList(Map paraMap);
}
