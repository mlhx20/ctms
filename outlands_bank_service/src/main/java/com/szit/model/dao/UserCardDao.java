package com.szit.model.dao;

import com.szit.model.bean.Card;
import com.szit.model.bean.UserCard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserCardDao extends BaseDao<UserCard> {
    List<UserCard> findByUserId(@Param("userId") Integer userId);
}
