package com.szit.model.dao;

import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao extends BaseDao<User> {
    List<User> queryAll();
    User queryOne(@Param("id") Integer id);
    User queryByName(@Param("name") String name);
}
