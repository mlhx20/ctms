package com.szit.model.dao;

import com.szit.model.bean.PayeeCard;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PayeeCardDao extends BaseDao<PayeeCard> {
}
