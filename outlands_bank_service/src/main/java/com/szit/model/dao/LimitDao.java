package com.szit.model.dao;

import com.szit.model.bean.Limit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LimitDao extends BaseDao<Limit> {
}
