package com.szit.model.dao;

import com.szit.model.bean.Card;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CardDao extends BaseDao<Card> {
    List<Card> findByUserId(Map paraMap);
    Long queryPageCount(Map paraMap);
    List<Card> queryPageList(Map paraMap);
}
