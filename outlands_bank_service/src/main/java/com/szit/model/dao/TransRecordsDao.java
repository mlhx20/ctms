package com.szit.model.dao;

import com.szit.model.bean.TransRecords;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Mapper
public interface TransRecordsDao extends BaseDao<TransRecords> {

    double queryDayAmount(@Param("userId")  Integer userId);
    long queryDayCount(@Param("userId")  Integer userId);
}
