package com.szit.model.dao;

import com.szit.model.bean.Payee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PayeeDao extends BaseDao<Payee> {
    List<Payee> queryAll();

    /**
     * 获取收款人、银行卡信息
     * @param paraMap
     * @return
     */
    List<Payee> queryPageList(Map paraMap);
}
