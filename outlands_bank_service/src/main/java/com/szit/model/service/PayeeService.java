package com.szit.model.service;

import com.szit.tool.result.DataTableJson;

public interface PayeeService {
    DataTableJson getPayeeByPage(Integer pageNo, Integer pageSize);
}
