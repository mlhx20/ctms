package com.szit.model.service;

import com.szit.model.bean.TransRecords;

import java.math.BigDecimal;

public interface TransRecordsService extends BaseService<TransRecords> {
    BigDecimal queryDayAmount(Integer userId);
    long queryDayCount(Integer userId);
}
