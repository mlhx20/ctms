package com.szit.model.service;

import com.szit.model.bean.User;
import com.szit.tool.result.DataTableJson;

import java.util.List;

public interface UserService extends BaseService<User> {
    List<User> findAll();
//    boolean login(User user);
    DataTableJson getData();
    boolean existUser(User user);
}
