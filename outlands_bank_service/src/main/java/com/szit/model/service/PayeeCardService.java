package com.szit.model.service;

import com.szit.tool.result.DataTableJson;

import java.util.Map;

public interface PayeeCardService {
    DataTableJson findByPayeeId(Integer payeeId,Integer pageNo, Integer pageSize);
}
