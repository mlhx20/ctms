package com.szit.model.service;

import com.szit.tool.result.DataTableJson;

public interface UserPayeeService {

    DataTableJson findByUserId(Integer userId,Integer payeeId,Integer pageNo, Integer pageSize);

}
