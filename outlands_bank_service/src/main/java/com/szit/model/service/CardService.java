package com.szit.model.service;

import com.szit.model.bean.Card;
import com.szit.tool.result.DataTableJson;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface CardService extends BaseService<Card> {
    DataTableJson findByUserId(Integer userId,Integer pageNo, Integer pageSize);

    /**
     * 修改 卡余额
     * @param cardId
     * @param amount
     * @param isReceive
     * @return
     */
    boolean tansCard(Integer cardId, BigDecimal amount,boolean isReceive);
    boolean cardMatch(String cardNumber,String name);
    boolean cardMatch(Integer cardId,Integer userId);
    boolean cardMatchPayee(Integer cardId,Integer payeeId);
}
