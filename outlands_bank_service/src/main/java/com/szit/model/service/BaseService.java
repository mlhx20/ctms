package com.szit.model.service;


import com.szit.tool.PageIndexer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Zenz.
 */
public interface BaseService<T> {
	/**
	 * 新增对象
	 * 
	 * @param entity 新增的对象
	 */
	void insert(T entity);

	/**
	 * 更新对象
	 * 
	 * @param entity 更新的对象
	 */
	void update(T entity);

	/**
	 * 根据主键删除对象
	 * 
	 * @param id 对象主键
	 */
	void delete(Serializable id);

	/**
	 * 批量删除对象
	 * 
	 * @param ids 对象主键
	 */
	void deletes(Serializable[] ids);

	/**
	 * 根据主键查询对象
	 * 
	 * @param id 对象主键
	 * @return 查找到的对象
	 */
	T findObjById(Serializable id);

	/**
	 * 查询对象列表
	 * 
	 * @return 查到对象列表
	 */
	List<T> findObjs(Map paraMap);

	/**
	 * 分页查询
	 * 
	 * @param paraMap 查询参数
	 * @return 分页对象
	 */
	PageIndexer getPageIndexer(Map paraMap);

}
