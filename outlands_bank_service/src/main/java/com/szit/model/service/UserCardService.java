package com.szit.model.service;


import com.szit.model.bean.User;
import com.szit.model.bean.UserCard;
import com.szit.tool.result.DataTableJson;

import java.util.List;

public interface UserCardService {
    DataTableJson findByUserId(Integer userId,Integer pageNo, Integer pageSize);
    boolean existUserCardId(Integer userId,Integer cardId);
}
