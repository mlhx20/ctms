package com.szit.tool.result;


import java.util.ArrayList;
import java.util.List;

/**
 * @Author fushane-asura
 * @Description: layui列表查询结果
 * @Date: Created in 20:16 2017/11/13
 *
 */
public class DataTableJson {
	// 状态码
	private int code;
	// 提示信息
	private String msg;
	// 总记录数
	private long count;
	// 数据集
	private List data = new ArrayList();

	public static DataTableJson success() {
		DataTableJson resultInfo = new DataTableJson();
		resultInfo.setCode(0);
		resultInfo.setMsg(ResourcesUtil.getValue("900200"));
		return resultInfo;
	}

	public static DataTableJson fail() {
		DataTableJson resultInfo = new DataTableJson();
		resultInfo.setCode(412);
		resultInfo.setMsg(ResourcesUtil.getValue("900201"));
		return resultInfo;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}
}
