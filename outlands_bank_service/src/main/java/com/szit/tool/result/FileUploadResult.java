package com.szit.tool.result;

/**
 * @Author fushane-asura
 * @Description:
 * @Date: Created in 2:17 2018/5/18
 * @Modified By:
 */
public class FileUploadResult {
	private int code; // 上传状态码
	private String msg; // 返回提示信息
	private String url; // 返回路径

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
