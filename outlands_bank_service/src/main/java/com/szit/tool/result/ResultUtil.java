package com.szit.tool.result;


import com.szit.tool.Config;
import com.szit.tool.exception.ExceptionResultInfo;

import java.util.List;

/**
 * 系统结果工具类
 *
 */
public class ResultUtil {

	/**
	 * 创建错误结果
	 */
	public static ResultInfo createFail(int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_FAIL, messageCode,
				ResourcesUtil.getValue(Config.MESSAGEFILE, messageCode + ""));
	}

	public static ResultInfo createFail(String fileName, int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_FAIL, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + ""));
	}

	public static ResultInfo createFail(String fileName, int messageCode, Object[] objs) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_FAIL, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + "", objs));
	}

	/**
	 * 创建敬告提示结果
	 */
	public static ResultInfo createWarning(int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_WARN, messageCode,
				ResourcesUtil.getValue(Config.MESSAGEFILE, messageCode + ""));
	}

	public static ResultInfo createWarning(String fileName, int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_WARN, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + ""));
	}

	public static ResultInfo createWarning(String fileName, int messageCode, Object[] objs) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_WARN, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + "", objs));
	}

	/**
	 * 创建成功提示结果
	 */
	public static ResultInfo createSuccess(int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_SUCCESS, messageCode,
				ResourcesUtil.getValue(Config.MESSAGEFILE, messageCode + ""));
	}

	public static ResultInfo createSuccess(String fileName, int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_SUCCESS, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + ""));
	}

	public static ResultInfo createSuccess(String fileName, int messageCode, Object[] objs) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_SUCCESS, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + "", objs));
	}

	/**
	 * 创建普通信息提示结果
	 */
	public static ResultInfo createInfo(int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_INFO, messageCode,
				ResourcesUtil.getValue(Config.MESSAGEFILE, messageCode + ""));
	}

	public static ResultInfo createInfo(String fileName, int messageCode) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_INFO, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + ""));
	}

	public static ResultInfo createInfo(String fileName, int messageCode, Object[] objs) {
		return new ResultInfo(ResultInfo.TYPE_RESULT_INFO, messageCode,
				ResourcesUtil.getValue(fileName, messageCode + "", objs));
	}

	/**
	 * 抛出异常
	 */
	public static void throwExcepion(ResultInfo resultInfo) throws ExceptionResultInfo {
		throw new ExceptionResultInfo(resultInfo);
	}

	public static void throwExcepion(ResultInfo resultInfo, List<ResultInfo> details) throws ExceptionResultInfo {
		if (resultInfo != null) {
			resultInfo.setDetails(details);
		}
		throw new ExceptionResultInfo(resultInfo);
	}

	/**
	 * 创建提交结果信息
	 */
	public static SubmitResultInfo createSubmitResult(ResultInfo resultInfo) {
		return new SubmitResultInfo(resultInfo);
	}

	/**
	 * 创建提交结果信息，包括明细信息
	 */
	public static SubmitResultInfo createSubmitResult(ResultInfo resultInfo, List<ResultInfo> details) {
		if (resultInfo != null) {
			resultInfo.setDetails(details);
		}
		return new SubmitResultInfo(resultInfo);
	}

}
