package com.szit.tool.bank;

import com.szit.tool.result.ResultUtil;
import com.szit.tool.result.SubmitResultInfo;
import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 第三方银行工具类
 */
public class BankTool {

    /**
     * 验证银行卡号 和姓名是否匹配
     * @param cardNumber 银行卡号
     * @param name 姓名
     */
    public static HttpResponse cardMatches(String cardNumber, String name) {
        String host = "https://dfbank2.market.alicloudapi.com";
        String path = "/verify_name_bankcard";
        String method = "POST";
        String appcode = "ae48aedc0c384d7687aac94181baa048";  //自己的AppCode ae48aedc0c384d7687aac94181baa048
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
        bodys.put("card_number", cardNumber);
        bodys.put("name", name);
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            return response;
            //获取response的body
            //System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
