package com.szit.tool.bank;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.szit.tool.result.DataTableJson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class RateTool {

    @RequestMapping("/getrate")
    @ResponseBody
    public JSONObject getrateC() throws IOException {
        HttpEntity entity = getRate();
        if(entity!=null){
            System.out.println(EntityUtils.toString(entity));
            System.out.println(JSONObject.parseObject(entity.toString()));
            System.out.println(JSONObject.parseObject(entity.toString()).get("msg").toString());
            System.out.println(JSONObject.parseObject(entity.toString()).get("updatetime").toString());
        }
        return JSONObject.parseObject(entity.toString());
    }

    public static HttpEntity getRate() {
        String host = "https://jisuhuilv.market.alicloudapi.com";
        String path = "/exchange/convert";
        String method = "GET";
        String appcode = "07d501791a5b4709be2e5177e731c010";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("amount", "100");
        querys.put("from", "USD");
        querys.put("to", "CNY");
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
//            System.out.println(response.toString());
//            Map map = new HashMap();
//            String s = EntityUtils.toString(response.getEntity());

//            map.put("entity",JSONObject.fromObject(s));
//            获取response的body
//            System.out.println(s);
            return response.getEntity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
