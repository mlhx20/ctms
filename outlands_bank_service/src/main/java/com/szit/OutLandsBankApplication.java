package com.szit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EntityScan("com.szit.model.bean, com.szit.tool.result, com.szit.tool")
@EnableFeignClients
public class OutLandsBankApplication {
    public static void main(String[] args) {
        SpringApplication.run(OutLandsBankApplication.class,args);
    }
}
