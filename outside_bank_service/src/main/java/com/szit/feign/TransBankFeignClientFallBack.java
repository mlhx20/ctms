package com.szit.feign;

import com.szit.model.bean.TransRecords;
import com.szit.model.bean.Transbank;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class TransBankFeignClientFallBack implements TransBankFeignClient{

    //调用springcloud-transbank 服务查询收款方银行卡号与用户名是否匹配失败
    @Override
    public int getCardAndName(Transbank transbank) {
        return -1;
    }

    //调用springcloud-transbank 服务进行修改余额操作失败
    @Override
    public int searchBanlance(Transbank transbank) {
        return -1;
    }

    @Override
    public int fundCollectBanlanceByCard(String usercard, double gruaMoney) {
        return -1;
    }

    @Override
    public boolean insertTransRecords(TransRecords transRecords) {
        System.out.println("hhh");
        return false;
    }

}
