package com.szit.feign;

import com.szit.model.bean.TransRecords;
import com.szit.model.bean.Transbank;
import com.szit.model.bean.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="inside-bank-service",fallback=TransBankFeignClientFallBack.class)
public interface TransBankFeignClient {

    //调用springcloud-transbank 服务查询收款方银行卡号与用户名是否匹配
    @RequestMapping(value="/receivables/getCardAndName",method=RequestMethod.POST)
    public int getCardAndName(@RequestBody Transbank transbank);

    //调用springcloud-transbank 服务进行修改余额操作
    @RequestMapping(value="/receivables/addBalance",method=RequestMethod.POST)
    public int searchBanlance(@RequestBody Transbank transbank);

    //调用springcloud-transbank 服务进行修改资金归集操作
    @RequestMapping(value="/receivables/fundCollect",method=RequestMethod.POST)
    public int fundCollectBanlanceByCard(@RequestParam("usercard") String usercard,@RequestParam("gruanMoney")double gruaMoney);

    //调用服务添加交易记录
    @RequestMapping(value = "/transRecords/insertrecords",method = RequestMethod.POST)
    @ResponseBody
    public boolean insertTransRecords(@RequestBody TransRecords transRecords);
}
