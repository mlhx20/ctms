package com.szit.controller;

import com.szit.feign.TransBankFeignClient;
import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import com.szit.model.bean.Transbank;
import com.szit.model.bean.UserCard;
import com.szit.model.dao.UserCardDao;
import com.szit.service.impl.EnjaTransServiceImpl;
import com.szit.service.impl.UserBankCardNoServiceImpl;
import com.szit.service.impl.UserCardServiceImpl;
import com.szit.service.impl.UserServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/enja")
public class EnjaTransController {

    @Autowired
    private TransBankFeignClient transBankFeignClient;
    @Autowired
    private EnjaTransServiceImpl enjaTransService;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserCardServiceImpl userCardService;
    @Autowired
    private UserBankCardNoServiceImpl userBankCardNoService;
    @Autowired
    private UserCardDao userCardDao;

    @Autowired
    private RedisTemplate redisTemplate;


    //查询所有卡号
    @RequestMapping(value="/allCardNo",method=RequestMethod.POST)
    public List<String> getAllCardNo(){
        return userBankCardNoService.selectCardNo();
    }

    //根据用户ID获取当前用户的所有未归集的银行卡号
    @RequestMapping(value="/allCardNoByUserId",method = RequestMethod.POST)
    public List<String> selectAllCardNoById(@RequestParam("id") Integer id){

        return userCardService.selectCarNoByUserId(id);
    }

    //获取当前用户获取非主卡的其他卡
    @RequestMapping("/queryCardNoMasterCard")
    public List<String> selectCardNoMasterCardNoById(@RequestParam("id") Integer id){
        return userCardService.selectCardNoMasterCardNoById(id);
    }

    //查询当前用户的主卡号
    @RequestMapping(value = "/queryMasterCard",method = RequestMethod.POST)
    @ResponseBody
    public String selectMasterCardBySign(@RequestParam("id") Integer id){
        return userCardService.selectCandNoBySign(id);
    }

    //获取当前登录用户的所有银行卡号
    @RequestMapping(value="/selectCardNoByUserId",method=RequestMethod.POST)
    @ResponseBody
    public List<String> selectCardNoByUserId(@RequestParam("id") Integer id){
        return userCardService.selectCardNoByUserId(id);
    }

    //获取当前登录用户的所有收款人姓名
    @RequestMapping(value = "/selectAllPayeeName",method = RequestMethod.POST)
    @ResponseBody
    public List<String> selectAllPayeeNameById(@RequestParam("id") Integer id){
        return userCardService.selectAllPayeeNameById(id);
    }

    //自动筛选出该收款人不同于付款人的他行账号
    @RequestMapping(value="/selectPayeeCardNo",method=RequestMethod.POST)
    @ResponseBody
    public List<String> selectPayeeCardNo(@RequestParam("payeename")String payeename,@RequestParam("cardno")String cardno){
        Map map = new HashMap();
        map.put("payeename",payeename);
        map.put("cardno",cardno);
        return userCardService.selectPayeeCardNo(map);
    }

    //根据卡号查询支付密码
    @RequestMapping(value="/selectPwd",method=RequestMethod.POST)
    @ResponseBody
    public String selectPwdByCardNo(@RequestParam("cardno") String cardno){
        return userCardService.selectPwdByCardNo(cardno);
    }

    //修改主卡为未归集状态
    @RequestMapping(value = "/searchMasterCard",method = RequestMethod.POST)
    public Integer searchMasterCardByCardNo(){
        return userCardService.searchMasterCardByCardNo();
    }

    //修改归集卡为主卡
    @RequestMapping(value = "/searchCollectCard",method = RequestMethod.POST)
    public Integer searchCollectCardByCardNo(@RequestParam("collectCardNo") String collectCardNo){
        return userCardService.searchCollectCardByCardNo(collectCardNo);
    }

    //根据卡号判查询在哪家银行
    @RequestMapping(value = "/selectCollectCard",method = RequestMethod.POST)
    public Integer selectBankTypeByCardNo(@RequestParam("cardno")String cardno){

        return userCardService.selectBankTypeByCardNo(cardno);
    }

    //跨行转账
    @RequestMapping(value = "/transfer",method = RequestMethod.POST)
    @ResponseBody
    public Integer bankTransfer(@RequestParam("payerCardNo") String payerCardNo
            ,@RequestParam("payeeCardNo")String payeeCardNo
            ,@RequestParam("amount")double amount
            ,@RequestParam("id")Integer id){

        Integer flag = enjaTransService.bankTransfer(payerCardNo,payeeCardNo,amount,id);
        if(flag==1){
            //添加交易记录
            TransRecords transRecords = new TransRecords();
            //交易单号
            transRecords.setCode(Long.toString(new Date().getTime()));
            //当前登录用户ID
            transRecords.setUserId(id);
            //汇款银行卡ID
            transRecords.setRemitCardId(userCardDao.queryBankIdByCardNo(payerCardNo));
            //收款人ID
            transRecords.setPayeeId(userCardDao.queryUserIdByCardNo(payeeCardNo));
            //收款人银行卡ID
            transRecords.setReceivCardId(userCardDao.queryBankIdByCardNo(payeeCardNo));
            //交易类型
            transRecords.setTypeId(1);
            //货币类型
            transRecords.setCurId(1);
            //交易状态
            transRecords.setState(1);
            System.out.println("添加交易记录：xxxx");
            boolean bflag = transBankFeignClient.insertTransRecords(transRecords);
            System.out.println("添加交易记录：" +bflag);
        }

        return flag;
    }

    //开始资金归集
    //调用outside-bank-service 服务进行资金归集
    @RequestMapping(value="/fundCollect",method=RequestMethod.POST)
    @ResponseBody
    public Integer fundCollect(@RequestParam("mastercardno") String mastercardno,
                               @RequestParam("collectcardno") String collectcardno,
                               @RequestParam("gruancardMoney") double gruancardMoney,
                               @RequestParam("id")Integer id){
        Integer flag = enjaTransService.searchFundConllectBalanceByNo(mastercardno,collectcardno,gruancardMoney,id);
        System.out.println("flag:"+flag);
        if(flag==1){
                    //添加交易记录
                    TransRecords transRecords = new TransRecords();
                    //交易单号
                    transRecords.setCode(Long.toString(new Date().getTime()));
                    //当前登录用户ID
                    transRecords.setUserId(id);
                    //汇款银行卡ID
                    transRecords.setRemitCardId(userCardDao.queryBankIdByCardNo(collectcardno));
                    //收款人ID
                    transRecords.setPayeeId(userCardDao.queryUserIdByCardNo(mastercardno));
                    //收款人银行卡ID
                    transRecords.setReceivCardId(userCardDao.queryBankIdByCardNo(mastercardno));
                    //交易类型
                    transRecords.setTypeId(2);
                    //货币类型
                    transRecords.setCurId(1);
                    //交易状态
                    transRecords.setState(1);
                    System.out.println("添加交易记录：xxxx");
                    boolean bflag = transBankFeignClient.insertTransRecords(transRecords);
                    System.out.println("添加交易记录：" +bflag);
                }
        return flag;
}

    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    @RequestMapping(value = "/selectBankNameAndCardNo" , method = RequestMethod.POST)
    @ResponseBody
    public List<Card> selectBankNameAndCardNo(@RequestParam("cardno")String cardno,@RequestParam("id")Integer id){
        Map map = new HashMap();
        map.put("cardno",cardno);
        map.put("id",id);
        return enjaTransService.selectBankNameAndCardNo(map);
    }

    //修改归集卡的状态
    @RequestMapping(value = "/searchCollectSignByCardNo",method = RequestMethod.POST)
    @ResponseBody
    public Integer searchCollectSignByCardNo(@RequestParam("cardno") String cardno){
        return userCardService.searchCollectSignByCardNo(cardno);
    }

}
