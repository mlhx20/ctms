package com.szit.model.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserBankCardNoDao {

    //查询所有卡号
    List<String> queryCardNo();
}
