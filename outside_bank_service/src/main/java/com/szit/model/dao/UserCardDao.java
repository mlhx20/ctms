package com.szit.model.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserCardDao {

    //判断用户归集账号是否是该账户名下的
    Integer queryCardIdByUserIdAndUserCard(Map map);

    //根据用户ID获取当前用户的所有未归集的银行卡号
    List<String> queryCarNoByUserId(@Param("id") Integer id);

    //查询当前用户ID是否设置了主卡
    String queryCandNoBySign(@Param("id") Integer id);

    //获取当前用户获取非主卡的其他卡
   List<String> queryCardNoMasterCardNoById(@Param("id") Integer id);

   //获取当前登录用户的所有银行卡号
   List<String> queryCardNoByUserId(@Param("id") Integer id);

   //获取当前登录用户的所有收款人姓名
    List<String> queryAllPayeeNameById(@Param("id") Integer id);

    //自动筛选出该收款人不同于付款人的他行账号
    List<String> queryPayeeCardNo(Map map);

    //根据卡号查询支付密码
    String queryPwdByCardNo(@Param("cardno")String cardno);

   //修改主卡为未归集状态
    int updateMasterCardByCardNo();

    //修改归集卡为主卡
    int updateCollectCardByCardNo(@Param("collectCardNo") String collectCardNo);

    //根据卡号判查询在哪家银行
    Integer queryBankTypeByCardNo(@Param("cardno")String cardno);

    //修改归集卡的状态
    Integer updateCollectSignByCardNo(@Param("cardno") String cardno);

    //根据银行卡号获取银行卡号ID
    Integer queryBankIdByCardNo(@Param("cardno") String cardno);

    //根据银行卡号获取用户ID
    Integer queryUserIdByCardNo(@Param("cardno") String cardno);
}
