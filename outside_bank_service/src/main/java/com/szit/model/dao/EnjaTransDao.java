package com.szit.model.dao;

import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EnjaTransDao {

//    查询转账方卡里余额
    public double queryBalanceByCardNo(@Param("cardNo")  String cardNo);

//修改转账方卡里余额
    public int updateBalanceByNo(Map map);

    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    public List<Card> queryBankNameAndCardNo(Map map);
}
