package com.szit.model.dao;

import com.szit.model.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao {
    //查询所有用户
    List<User> queryAll();

    //根据ID查询身份证
    String queryIdCardById(int id);
}
