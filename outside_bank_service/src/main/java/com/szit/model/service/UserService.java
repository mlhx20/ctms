package com.szit.model.service;

import com.szit.model.bean.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    //根据ID查询用户身份证
    String getIdCardById(int id);
}
