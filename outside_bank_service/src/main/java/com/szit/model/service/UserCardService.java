package com.szit.model.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface UserCardService {

    //判断用户归集账号是否是该账户名下的
    boolean IsCardIdByUserIdAndUserCard(Map map);

    //根据用户ID获取当前用户的所有未归集的银行卡号
    List<String> selectCarNoByUserId(Integer id);

    //查询用户主卡
    String selectCandNoBySign(Integer id);

    //获取当前用户获取非主卡的其他卡
    List<String> selectCardNoMasterCardNoById(Integer id);

    //获取当前登录用户的所有银行卡号
    List<String> selectCardNoByUserId(Integer id);

    //获取当前登录用户的所有收款人姓名
    List<String> selectAllPayeeNameById(Integer id);

    //自动筛选出该收款人不同于付款人的他行账号
    List<String> selectPayeeCardNo(Map map);

    //根据卡号查询支付密码
    String selectPwdByCardNo(String cardno);

    //修改主卡为未归集状态
    Integer searchMasterCardByCardNo();

    //修改归集卡为主卡
    Integer searchCollectCardByCardNo(String collectCardNo);

    //根据卡号判查询在哪家银行
    Integer selectBankTypeByCardNo(String cardno);

    //修改归集卡的状态
    Integer searchCollectSignByCardNo(String cardno);

}
