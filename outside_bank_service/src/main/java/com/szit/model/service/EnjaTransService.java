package com.szit.model.service;

import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;

import java.util.List;
import java.util.Map;

public interface EnjaTransService {


    //查询转账方卡里余额
    public double getBalance(String cardno);

    //跨行转账
    public int bankTransfer(String payerCardNo,String payeeCardNo,double amount,Integer id);

    //修改归集后的用户余额
    public int searchFundConllectBalanceByNo(String mastercardno,String collectcardno,double gruancardMoney,Integer id);

    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    public List<Card> selectBankNameAndCardNo(Map map);

}
