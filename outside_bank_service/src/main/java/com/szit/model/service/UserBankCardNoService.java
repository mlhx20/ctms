package com.szit.model.service;

import java.util.List;

public interface UserBankCardNoService {

    //查询所有卡号
    List<String> selectCardNo();
}
