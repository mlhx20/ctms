package com.szit.model.bean;

import java.io.Serializable;

public class Transbank implements Serializable {

    private int id;
    private String username;
    private String usercard;

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUsercard(String usercard) {
        this.usercard = usercard;
    }

    public void setUserbalance(double userbalance) {
        this.userbalance = userbalance;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getUsercard() {
        return usercard;
    }

    public double getUserbalance() {
        return userbalance;
    }

    private double userbalance;
    private String useridcard;

    public void setUseridcard(String useridcard) {
        this.useridcard = useridcard;
    }

    public String getUseridcard() {
        return useridcard;
    }

    public Transbank() {
    }

    public Transbank(int id, String username, String usercard, double userbalance, String useridcard) {
        this.id = id;
        this.username = username;
        this.usercard = usercard;
        this.userbalance = userbalance;
        this.useridcard = useridcard;
    }
}
