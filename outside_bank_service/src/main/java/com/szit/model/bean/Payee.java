package com.szit.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 收款人实体类
 */
public class Payee implements Serializable {
    private Integer id;
    private String name;
    private String code;
    private String phone; //
    private BigDecimal money; //
    private String country;
    private String address;

    public Payee() {
    }

    public Payee(Integer id, String name, String code, String phone, BigDecimal money, String country, String address) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.phone = phone;
        this.money = money;
        this.country = country;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
