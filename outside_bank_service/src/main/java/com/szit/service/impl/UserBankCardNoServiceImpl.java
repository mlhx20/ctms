package com.szit.service.impl;

import com.szit.model.dao.UserBankCardNoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserBankCardNoServiceImpl implements com.szit.model.service.UserBankCardNoService {

    @Autowired
    UserBankCardNoDao userBankCardNoDao;

    //查询所有卡号
    public List<String> selectCardNo(){

        return userBankCardNoDao.queryCardNo();
    }
}
