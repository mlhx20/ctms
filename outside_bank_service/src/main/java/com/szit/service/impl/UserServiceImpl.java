package com.szit.service.impl;

import com.szit.model.bean.User;
import com.szit.model.dao.UserDao;
import com.szit.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public List<User> findAll() {
        return userDao.queryAll();
    }

    //根据ID查询用户身份证
    @Override
    public String getIdCardById(int id) {
        return userDao.queryIdCardById(id);
    }
}
