package com.szit.service.impl;

import com.szit.model.dao.UserCardDao;
import com.szit.model.service.UserCardService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class UserCardServiceImpl implements UserCardService {

    @Autowired
    private UserCardDao userCardDao;

    //判断用户归集账号是否是该账户名下的
    @Override
    public boolean IsCardIdByUserIdAndUserCard(Map map){
        boolean flag = true;
        Integer index = userCardDao.queryCardIdByUserIdAndUserCard(map);

        if(index==null){
            flag=false;
        }

        return flag;
    }

    //根据用户ID获取当前用户的所有未归集的银行卡号
    public List<String> selectCarNoByUserId(Integer id){

        return userCardDao.queryCarNoByUserId(id);
    }

    //获取当前用户获取非主卡的其他卡
    public List<String> selectCardNoMasterCardNoById(Integer id){

        return userCardDao.queryCardNoMasterCardNoById(id);
    }

    //查询用户主卡
    public String selectCandNoBySign(Integer id){

        return userCardDao.queryCandNoBySign(id);
    }

    //获取当前登录用户的所有银行卡号
    public List<String> selectCardNoByUserId(Integer id){
        return userCardDao.queryCardNoByUserId(id);
    }

    //获取当前登录用户的所有收款人姓名
    public List<String> selectAllPayeeNameById(Integer id){
        return userCardDao.queryAllPayeeNameById(id);
    }

    //自动筛选出该收款人不同于付款人的他行账号
    public List<String> selectPayeeCardNo(Map map){
        return userCardDao.queryPayeeCardNo(map);
    }

    //根据卡号查询支付密码
    public String selectPwdByCardNo(String cardno){
        return userCardDao.queryPwdByCardNo(cardno);
    }

    //修改主卡为未归集状态
    public Integer searchMasterCardByCardNo(){
        return userCardDao.updateMasterCardByCardNo();
    }

    //修改归集卡为主卡
    public Integer searchCollectCardByCardNo(String collectCardNo){
        return userCardDao.updateCollectCardByCardNo(collectCardNo);
    }

    //根据卡号判查询在哪家银行
    public Integer selectBankTypeByCardNo(String cardno){

        return userCardDao.queryBankTypeByCardNo(cardno);
    }

    //修改归集卡的状态
    public Integer searchCollectSignByCardNo( String cardno){
        return userCardDao.updateCollectSignByCardNo(cardno);
    }
}
