package com.szit.service.impl;

import com.szit.model.bean.Card;
import com.szit.model.bean.TransRecords;
import com.szit.model.dao.EnjaTransDao;
import com.szit.model.dao.UserCardDao;
import com.szit.model.service.EnjaTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EnjaTransServiceImpl implements EnjaTransService {

    @Autowired
    private EnjaTransDao enjaTransDao;

    @Autowired
    private UserCardDao userCardDao;

    //查询转账方卡里余额
    public double getBalance(String cardno){
        return enjaTransDao.queryBalanceByCardNo(cardno);
    }

    //跨行转账 1:转账成功 -1：余额不足 0：系统繁忙
    @Transactional(rollbackFor = Exception.class)
    public int bankTransfer(String payerCardNo,String payeeCardNo,double amount,Integer id){
        int flag = 0;
        Map map = new HashMap();
        //获取付款人余额
        double payerBanlance = getBalance(payerCardNo);
        if(payerBanlance-amount<0){
            //余额不足
            flag=-1;
        }else{
            try {
                //余额充足
                //修改付款方余额
                map.put("banlance", payerBanlance - amount);
                map.put("cardno", payerCardNo);

                int payerflag = enjaTransDao.updateBalanceByNo(map);
                if (payerflag == 1) {
                    //修改成功
                    //获取付款方余额
                    double payeeBanlance = getBalance(payeeCardNo);
                    //修改收款方余额
                    map.put("banlance", payeeBanlance + amount);
                    map.put("cardno", payeeCardNo);

                    int payeeflag = enjaTransDao.updateBalanceByNo(map);
                    if (payeeflag == 1) {
                        //修改成功
                        //转账成功

                        flag=1;
                    } else {
                        //修改失败
                        throw new RuntimeException();
                    }
                } else {
                    //修改失败
                    throw new RuntimeException();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return flag;

    }

    //修改资金归集 归集卡的用户余额 1:归集成功  0：归集失败  -1：余额不足，归集失败
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int searchFundConllectBalanceByNo(String mastercardno, String collectcardno, double gruancardMoney,Integer id) {
        int flag = 0;

        System.out.println("mastercardno" + mastercardno + "collectcardno" + collectcardno + "gruancardno" + gruancardMoney);

        //获取被归集卡当前余额
        double collBanlance = getBalance(collectcardno);

        System.out.println("collBanlance:" + collBanlance);

        try{
        if (collBanlance - gruancardMoney <= 0) {
            //余额不足
            flag = -1;
        } else {
            Map map1 = new HashMap();
            //余额充足
            //进行归集操作
            map1.put("cardno", collectcardno);
            map1.put("banlance", gruancardMoney);
            int colflag = enjaTransDao.updateBalanceByNo(map1);

            if (colflag == 1) {
                //归集卡归集成功

                //主卡进行归集
                //获取主卡当前余额
                double mastBanlance = getBalance(mastercardno);
                System.out.println("mastBanlance:" + mastBanlance);
                Map map2 = new HashMap();
                //归集后的资金
                double afterMastBanlance = mastBanlance + collBanlance - gruancardMoney;
                System.out.println("afterMastBanlance" + afterMastBanlance);
                map2.put("cardno", mastercardno);
                map2.put("banlance", afterMastBanlance);
                int mastflag = enjaTransDao.updateBalanceByNo(map2);
                if (mastflag == 1) {
                    //归集成功

                    flag = 1;
                }else{
                    throw new RuntimeException();
                }
            }else{
                throw new RuntimeException();
            }
        }
    }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }



    //根据用户选择的下拉列表卡号将非本行的其他银行卡号渲染在归集下拉列表
    public List<Card> selectBankNameAndCardNo(Map map){
        return enjaTransDao.queryBankNameAndCardNo(map);
    }


}
